package com.a2a.fans.SecurityHelper;



public class EncryptionDecryptionHelper {


    private static EncryptionDecryptionHelper instance=null;

    public static EncryptionDecryptionHelper getInstance() {
        if(instance == null)
        {
            instance = new EncryptionDecryptionHelper();
        }

        return instance;
    }

    private EncryptionDecryptionHelper() {
    }


    public String encryptData(String data, String key) throws Exception {
        if (key != null && !key.isEmpty()) {
            key = key.replaceAll("Bearer", "");
            key = key.trim();
            String[] arraAuth = key.split("\\.");
            key = arraAuth[2];
            DataEncryption dataEncryption = new DataEncryption();
            String secrectKey = key.substring(0, 32);

            String dataEnc = "";

            try {
                String iv = "1234567890123456";
                CryptLib _crypt = new CryptLib();
                dataEnc = _crypt.encrypt(data, secrectKey, iv); //encrypt
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return dataEnc;

        } else throw new Exception();
    }

    public String decryptData(String data, String key) throws Exception {
        if (key != null && !key.isEmpty()) {
            key = key.replaceAll("Bearer", "");
            key = key.trim();
            String[] arraAuth = key.split("\\.");
            key = arraAuth[2];
            DataEncryption dataEncryption = new DataEncryption();
            String secrectKey = key.substring(0, 32);
            String dataDec = "";

            try {
                String iv = "1234567890123456";
                CryptLib _crypt = new CryptLib();
                dataDec = _crypt.decrypt(data, secrectKey, iv); //encrypt
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return dataDec;

        } else throw new Exception();
    }

}
