package com.a2a.fans.views.activities;

import android.app.NotificationManager;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;


import com.a2a.fans.databinding.ChattingActivityBinding;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.ChatRoomModel;
import com.a2a.fans.utls.ClipboardManager;
import com.a2a.fans.viewmodels.ChattingRoomVM;
import com.abangfadli.shotwatch.ShotWatch;
import com.a2a.fans.views.Adapters.ChattingRoomAdapter;
import com.a2a.fans.R;

import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;


public class ChattingActivity extends AppCompatActivity implements View.OnClickListener, ChattingRoomAdapter.AdapterCallback {

    ChattingActivityBinding binding;
    ChattingRoomVM viewModel;
    Animation anim;

     MutableLiveData<List<ChatRoomModel>> arrayList=new MutableLiveData<>();
     ChattingRoomAdapter adapter;

     long bankID;
     String bankName="";

    ShotWatch mShotWatch;
    boolean multiSelectMode=false;

    BroadcastReceiver receiver;
    int SEARCH_ACTIVITY=1111;
    int numberOfSelectedItem=0;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SEARCH_ACTIVITY && data!=null)
        {
            String accNum=data.getExtras().getString("accNum");
           // String cardNum=data.getExtras().getString("cardNum");
            String amntFrom=data.getExtras().getString("amntFrom");
            String amntTo=data.getExtras().getString("amntTo");
            String startDate=data.getExtras().getString("startDate");
            String endDate=data.getExtras().getString("endDate");
            String flagged=data.getExtras().getString("flagged");


            viewModel.searchMessageList(bankID, "", amntFrom, amntTo, startDate, endDate, accNum, flagged).observe(ChattingActivity.this, new Observer<List<ChatRoomModel>>() {
                @Override
                public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                    arrayList.setValue(chatRoomModels);
                    initRecycleView();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(ChattingActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.chatting_activity);
        binding.setLifecycleOwner(ChattingActivity.this);
        viewModel = ViewModelProviders.of(this).get(ChattingRoomVM.class);
        binding.setViewModel(viewModel);
        viewModel.bankID=bankID;


//        ShotWatch.Listener screenshotListner=new ShotWatch.Listener(){
//            @Override
//            public void onScreenShotTaken(ScreenshotData screenshotData)
//            {
//            //    Toast.makeText(getApplicationContext(), "Screen Captured !!!! " ,  Toast.LENGTH_LONG).show();
//            }
//        };
//        mShotWatch = new ShotWatch(getContentResolver(), screenshotListner);



        customType(ChattingActivity.this,getResources().getString(R.string.animation_bottom_to_up));


//        android.support.v7.widget.Toolbar toolbar=findViewById ( R.id.my_toolbar );
//        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.filter_icon);
//        toolbar.setOverflowIcon(drawable);
//        setSupportActionBar ( toolbar );

        anim = AnimationUtils.loadAnimation(this, R.anim.shake);

        binding.deleteBTN.setVisibility(View.INVISIBLE);
        binding.exportBTN.setVisibility(View.INVISIBLE);
        binding.sendBTN.setOnClickListener(this);
        binding.attachGalleryBTN.setOnClickListener(this);
        binding.attachCameraBTN.setOnClickListener(this);
        binding.allFilterBTN.setOnClickListener(this);
        binding.accountFilterBTN.setOnClickListener(this);
        binding.cardFilterBTN.setOnClickListener(this);
        binding.otherFilterBTN.setOnClickListener(this);
        binding.adsFilterBTN.setOnClickListener(this);
        binding.advanceFilterBTN.setOnClickListener(this);
        binding.deleteBTN.setOnClickListener(this);
        binding.exportBTN.setOnClickListener(this);
        binding.noMsgImg.setVisibility(View.GONE);


        try
        {
            bankID = getIntent().getLongExtra("bankID",0);
            bankName = getIntent().getStringExtra("bankName");
            binding.contactTXT.setText(bankName);
            if(getIntent().getAction().equalsIgnoreCase("REPORT_ACTION"))
            {
                String msgID = getIntent().getStringExtra("msgID");
                viewModel.reportMessage(bankID,msgID).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(@Nullable Boolean aBoolean) {

                        if(aBoolean==true)
                        {
                            fillObserver(bankID,"");
                        }
                    }
                });
            }

        }
        catch (Exception xx)
        {
            xx.getMessage();
        }


        viewModel.fillMessageList(bankID,"");
        viewModel.chatList.observe(this, new Observer<List<ChatRoomModel>>()
        {
            @Override
            public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                arrayList.setValue(chatRoomModels);
                initRecycleView();
                binding.allFilterBTN.setSelected(true);
            }
        });

        receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancelAll();

                viewModel.fillMessageList(bankID,"").observe(ChattingActivity.this, new Observer<List<ChatRoomModel>>()
                {
                    @Override
                    public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                        arrayList.setValue(chatRoomModels);
                        initRecycleView();
                        binding.allFilterBTN.performClick();
                        viewModel.updateMsgsAsReadToServer(bankID);


                    }
                });
            }
        };


        binding.searchTXT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(count>0)
                {

                    viewModel.searchMessageList(bankID, binding.searchTXT.getText().toString().trim(), "", "", "", "", "", "").observe(ChattingActivity.this, new Observer<List<ChatRoomModel>>() {
                        @Override
                        public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                            arrayList.setValue(chatRoomModels);
                            initRecycleView();
                        }
                    });
                }
                else
                {
                    binding.allFilterBTN.performClick();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.searchTXT.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    if(!binding.searchTXT.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        viewModel.searchMessageList(bankID, binding.searchTXT.getText().toString().trim(), "", "", "", "", "", "").observe(ChattingActivity.this, new Observer<List<ChatRoomModel>>() {
                            @Override
                            public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                                arrayList.setValue(chatRoomModels);
                                initRecycleView();
                            }
                        });

                    }
                    return true;
                }
                return false;
            }
        });

        viewModel.updateMsgsAsReadToServer(bankID);

    }

    private void fillObserver(long bankID,String groubBy)
    {
        viewModel.fillMessageList(bankID,groubBy);
        viewModel.chatList.observe(this, new Observer<List<ChatRoomModel>>()
        {
            @Override
            public void onChanged(@Nullable List<ChatRoomModel> chatRoomModels) {

                arrayList.setValue(chatRoomModels);
                initRecycleView();
                if(groubBy.equalsIgnoreCase(""))
                {
                    binding.allFilterBTN.setSelected(true);
                    binding.accountFilterBTN.setSelected(false);
                    binding.cardFilterBTN.setSelected(false);
                    binding.adsFilterBTN.setSelected(false);
                    binding.otherFilterBTN.setSelected(false);

                }
            }
        });


    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mShotWatch.register();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        mShotWatch.unregister();
//    }



    @Override
    public void onClick(View v) {

//        if(v.getId()==R.id.sendBTN)
//        {
//           sendMessage("1","","");
//        }

//        if(v.getId()==R.id.attachGalleryBTN)
//        {
//            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//            startActivityForResult(i, 11);
//        }
//        if(v.getId()==R.id.attachCameraBTN)
//        {
//            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(intent, 12);
//        }

        if(v.getId()==R.id.allFilterBTN)
        {
            binding.allFilterBTN.setSelected(true);
            binding.accountFilterBTN.setSelected(false);
            binding.cardFilterBTN.setSelected(false);
            binding.adsFilterBTN.setSelected(false);
            binding.otherFilterBTN.setSelected(false);
            fillObserver(bankID,"");

        }
        if(v.getId()==R.id.accountFilterBTN)
        {
            binding.allFilterBTN.setSelected(false);
            binding.accountFilterBTN.setSelected(true);
            binding.cardFilterBTN.setSelected(false);
            binding.adsFilterBTN.setSelected(false);
            binding.otherFilterBTN.setSelected(false);
            fillObserver(bankID,"2");

        }
        if(v.getId()==R.id.cardFilterBTN)
        {
            binding.allFilterBTN.setSelected(false);
            binding.accountFilterBTN.setSelected(false);
            binding.cardFilterBTN.setSelected(true);
            binding.adsFilterBTN.setSelected(false);
            binding.otherFilterBTN.setSelected(false);
            fillObserver(bankID,"1");

        }
        if(v.getId()==R.id.adsFilterBTN)
        {
            binding.allFilterBTN.setSelected(false);
            binding.accountFilterBTN.setSelected(false);
            binding.cardFilterBTN.setSelected(false);
            binding.adsFilterBTN.setSelected(true);
            binding.otherFilterBTN.setSelected(false);
            fillObserver(bankID,"3");
        }
        if(v.getId()==R.id.otherFilterBTN)
        {
            binding.allFilterBTN.setSelected(false);
            binding.accountFilterBTN.setSelected(false);
            binding.cardFilterBTN.setSelected(false);
            binding.adsFilterBTN.setSelected(false);
            binding.otherFilterBTN.setSelected(true);
            fillObserver(bankID,"4");

        }
        if(v.getId()==R.id.advanceFilterBTN)
        {
            startActivityForResult(new Intent(getApplicationContext(),FilterActivity.class).putExtra("bankID",bankID),SEARCH_ACTIVITY);

        }
        if(v.getId()==R.id.deleteBTN)
        {
            new AlertDialog.Builder(ChattingActivity.this)
                    .setTitle("Hint")
                    .setCancelable(false)
                    .setMessage(R.string.sure_to_delete)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(viewModel.deleteMsgs(arrayList,bankID))
                            {
                                binding.deleteBTN.setVisibility(View.INVISIBLE);
                                binding.exportBTN.setVisibility(View.INVISIBLE);
                                fillObserver(bankID,"");
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();


        }
        if(v.getId()==R.id.exportBTN)
        {
            boolean result=viewModel.exportMessages(arrayList);
           if(result)
           {
               binding.deleteBTN.setVisibility(View.INVISIBLE);
               binding.exportBTN.setVisibility(View.INVISIBLE);
               fillObserver(bankID,"");
           }
        }



    }

    private void initRecycleView()
    {
        try
        {
            if(arrayList.getValue().size()>0)
            {
                binding.noMsgImg.setVisibility(View.GONE);
            }
            else
            {
                binding.noMsgImg.setVisibility(View.VISIBLE);
            }

            adapter = new ChattingRoomAdapter(ChattingActivity.this, arrayList, bankID, ChattingActivity.this);
            binding.chattingLV.setAdapter(adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            binding.chattingLV.setLayoutManager(layoutManager);
            int siz=arrayList.getValue().size();
            binding.chattingLV.scrollToPosition(siz - 1);
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }


//        if(goToLastMsg)
//        {
//            chattingLV.scrollToPosition(arrayList.size() - 1);
//        }
//        else
//        {
//            chattingLV.scrollToPosition(clickedPos- 1);
//        }
    }


    @Override
    public void onRecyclItemClicked(int itemID,int position, ChatRoomModel itemModel) {

        if(itemID==R.id.addFlagBTN)
        {
            if (itemModel.getIsFlagged().equalsIgnoreCase("false"))
            {
                String msgID=itemModel.getMsgId();
                viewModel.flag_unFlagMsg(msgID,bankID,"true");
                arrayList.getValue().get(position).setIsFlagged("true");

            } else if (itemModel.getIsFlagged().equalsIgnoreCase("true"))
            {
                String msgID=itemModel.getMsgId();
                viewModel.flag_unFlagMsg(msgID,bankID,"false");
                arrayList.getValue().get(position).setIsFlagged("false");

            }



        }
        else if(itemID==R.id.reportBTN)
        {
            String msgID=itemModel.getMsgId();
            viewModel.reportMessage(bankID,msgID).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean aBoolean) {

                    if(aBoolean==true)
                    {
                        fillObserver(bankID,"");
                    }
                }
            });


        }
        else if(itemID==R.id.shareBTN)
        {
            String msgID=itemModel.getMsgId();
            String date=itemModel.getMsgDate();
            String body=itemModel.getMsgBody();
            String name="";

            viewModel.openShareDialog(msgID,bankID,date,body,name);

        }
        else if(itemID==R.id.copyBTN)
        {
            try
            {
                String otpTXT = this.arrayList.getValue().get(position).getMsgBody().toString();
                String[] otpArr = otpTXT.split(":");
                ClipboardManager.getInstance().setClipboard(getApplicationContext(), otpArr[1]);
            }
            catch (Exception xx)
            {
                xx.toString();
            }

        }
        else if(itemID==R.id.msgContentIMG)
        {
            String img64=itemModel.getBase64();
            viewModel.openImageZoom(img64);

        }
        else if(itemID==R.id.msgLO)
        {

            if(arrayList.getValue().get(position).getMessageType().equalsIgnoreCase("3")&& multiSelectMode==false )
            {
                if(!arrayList.getValue().get(position).getBase64().equalsIgnoreCase("")) {
                    String url = arrayList.getValue().get(position).getBase64();
                    viewModel.openPDFMsg(url);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.pdf_url_not_available), Toast.LENGTH_SHORT).show();

                }
            }
            else if(arrayList.getValue().get(position).getMessageType().equalsIgnoreCase("2")&& multiSelectMode==false )
            {
                if(!arrayList.getValue().get(position).getBase64().equalsIgnoreCase(""))
                {
                    String img64 = arrayList.getValue().get(position).getBase64();
                    viewModel.openImageZoom(img64);
                }
            }
            else if(multiSelectMode)
            {

                if(arrayList.getValue().get(position).getChecked().equalsIgnoreCase("true"))
                {
                    numberOfSelectedItem-=1;
                    binding.deleteBTN.setVisibility(View.VISIBLE);
                    binding.exportBTN.setVisibility(View.VISIBLE);
                    arrayList.getValue().get(position).setChecked("false");
                }
                else
                {
                    numberOfSelectedItem+=1;
                    binding.deleteBTN.setVisibility(View.VISIBLE);
                    binding.exportBTN.setVisibility(View.VISIBLE);
                    arrayList.getValue().get(position).setChecked("true");
                }

                if(numberOfSelectedItem<=0)
                {
                    binding.deleteBTN.setVisibility(View.INVISIBLE);
                    binding.exportBTN.setVisibility(View.INVISIBLE);
                    multiSelectMode=false;
                }

            }
        }


        adapter.notifyDataSetChanged();

        }



    @Override
    public void onRecyclItemLongClicked(int itemID, int position, ChatRoomModel itemModel) {

        if(itemID==R.id.msgLO)
        {
            if(multiSelectMode==false)
            {
                multiSelectMode = true;
                ChattingRoomAdapter.multiSelectMode = multiSelectMode;
                binding.deleteBTN.setVisibility(View.VISIBLE);
                binding.exportBTN.setVisibility(View.VISIBLE);
                arrayList.getValue().get(position).setChecked("true");
                numberOfSelectedItem += 1;
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        viewModel.updateMsgsAsRead(bankID);

        multiSelectMode=false;
        ChattingRoomAdapter.multiSelectMode=multiSelectMode;

    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("receiveAction"));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
}
