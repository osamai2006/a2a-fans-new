package com.a2a.fans.views.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.a2a.fans.databinding.OtpActivityBinding;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.viewmodels.OtpVM;
import com.airbnb.lottie.LottieAnimationView;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.R;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class otpActivity extends AppCompatActivity {

     OtpActivityBinding binding;
     OtpVM viewModel;


    CountDownTimer SMSTimer;
    String utr="",mobileNumber="";
    int timeOut=60000*3;
    LottieAnimationView lottieAnimationView;

    SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(otpActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.otp_activity);
        binding.setLifecycleOwner(otpActivity.this);
        viewModel = ViewModelProviders.of(this).get(OtpVM.class);
        binding.setViewModel(viewModel);

       // customType(otpActivity.this,getString(R.string.animation_bottom_to_up));

        try
        {
            timeOut=Integer.parseInt(getIntent().getExtras().getString("timeout"))*1000*60;
            //otp=getIntent().getExtras().getString("otp");
            utr=getIntent().getExtras().getString("utr");
            mobileNumber=getIntent().getExtras().getString("MobileNo");

        }
        catch (Exception xx)
        {
            xx.toString();
        }

        binding.hintTXT1.setText(getString(R.string.activation_hint1)+" "+mobileNumber);
        binding.hintTXT2.setText(getString(R.string.activation_hint2)+" "+mobileNumber);
        binding.lottieAnimationView.playAnimation();


        binding.otpCodeTXT.addTextChangedListener(new TextWatcher()
        {
               @Override
               public void beforeTextChanged(CharSequence s, int start, int count, int after) {

               }

               @Override
               public void onTextChanged(CharSequence s, int start, int before, int count) {

                   if( binding.otpCodeTXT.getText().length()>=4)
                   {
                       viewModel.mobileNumber=mobileNumber;
                       viewModel.countryCode=appConstants.countryCode;
                         boolean result= viewModel.requestValidation(binding.otpCodeTXT.getText().toString().trim(),utr);
                         if(result==false)
                         {
                             binding.otpCodeTXT.setText("");
                             Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.otp_faild_3_times),  Toast.LENGTH_LONG).show();

                         }

//                       if( binding.otpCodeTXT.getText().toString().equalsIgnoreCase(otp))
//                       {
//                           viewModel.mobileNumber=mobileNumber;
//                           viewModel.countryCode=appConstants.countryCode;
//                           viewModel.requestValidation(otp,utr);
//
//                       }
//                       else
//                       {
//                           binding.otpCodeTXT.setText("");
//                           Toast.makeText(getApplicationContext(), R.string.wrong_otp, Toast.LENGTH_LONG).show();
//                       }
                   }
               }

               @Override
               public void afterTextChanged(Editable s) {

               }
           });

//        binding.wrongNumTXT.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        viewModel.wrongNumberClick();
//
//                    }
//                });

        binding.resenOTPTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewModel.resendOTPClick(binding.otpCodeTXT.getText().toString().trim(),utr);
            }
        });

        SMSTimer=new CountDownTimer(timeOut,1) {
            @Override
            public void onTick(long millisUntilFinished) {

                String text = String.format(Locale.getDefault(), "%02d: %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);

                binding.timerTXT.setText(text);
            }

            @Override
            public void onFinish() {

            }
        }.start();


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code =viewModel.parseCode(message);
                binding.otpCodeTXT.setText(code);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }

}
