package com.a2a.fans.views.Adapters;

import android.animation.ValueAnimator;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a2a.fans.utls.appConstants;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.a2a.fans.models.DashboardModel;
import com.a2a.fans.R;

import java.util.ArrayList;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MenuItemViewHolder> {

    Context context;
    MutableLiveData<ArrayList<DashboardModel>> arrayList;
    Animation anim,animPopup;


    public DashboardAdapter(Context context,MutableLiveData<ArrayList<DashboardModel>> ResourcelList)
    {
        this.context = context;
        arrayList = ResourcelList;
        anim = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
        animPopup = AnimationUtils.loadAnimation(context, R.anim.slide_in_left);
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_row_layout, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final MenuItemViewHolder holder, final int position) {

        float cardBalance=Float.parseFloat(arrayList.getValue().get(position).getCardBalance());
        ValueAnimator animator = ValueAnimator.ofFloat(0,(int) cardBalance);
        animator.setDuration(3000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                holder.cardBalanceTXT.setText(animation.getAnimatedValue().toString()+"00");
            }
        });
        animator.start();
        //holder.cardBalanceTXT.setText(String.valueOf(cardBalance));

        float availableBalance=Float.parseFloat(arrayList.getValue().get(position).getAvailableBalance());
        ValueAnimator animator2 = ValueAnimator.ofFloat(0,(int) availableBalance);
        animator2.setDuration(3000);
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                holder.availablBalncTXT.setText(animation.getAnimatedValue().toString()+"00");
            }
        });
        animator2.start();
       // holder.availablBalncTXT.setText(String.valueOf(availableBalance));


//        Glide.with(context).load(arrayList.get(position).getImage())
//                //.apply(RequestOptions.circleCropTransform())
//                .transition(GenericTransitionOptions.with(R.anim.slide_in_right))
//                .into(holder.logoIMG);

//        byte[] decodedString = Base64.decode(arrayList.getValue().get(position).getImage(), Base64.DEFAULT);
//        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//        holder.logoIMG.setImageBitmap(decodedByte);

        holder.logoIMG.setImageBitmap(appConstants.imageFrom64(arrayList.getValue().get(position).getImage()));


        if(position==0)
        {
            holder.containerLO.setBackgroundResource(R.drawable.orange_card_bg);
        }
        else  if(position==1)
        {
            holder.containerLO.setBackgroundResource(R.drawable.green_card_bg);
        }
        else  if(position==2)
        {
            holder.containerLO.setBackgroundResource(R.drawable.blue_card_bg);
        }

        holder.itemView.startAnimation(anim);
        holder.bankLogoLO.startAnimation(animPopup);


    }

    @Override
    public int getItemCount() {
        return arrayList.getValue().size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout mainLO,containerLO,bankLogoLO;
        TextView cardBalanceTXT,availablBalncTXT ;
        ImageView logoIMG;

        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            containerLO= itemView.findViewById(R.id.containerLO);
            cardBalanceTXT = itemView.findViewById(R.id.cardBalanceTXT);
            availablBalncTXT= itemView.findViewById(R.id.availablBalncTXT);
            logoIMG = itemView.findViewById(R.id.logoIMG);
            bankLogoLO= itemView.findViewById(R.id.bankLogoLO);


        }

    }






}
