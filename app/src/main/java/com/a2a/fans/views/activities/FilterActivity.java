package com.a2a.fans.views.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.a2a.fans.databinding.FilterDialogLayoutBinding;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.viewmodels.FilterVM;
import com.a2a.fans.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class FilterActivity extends AppCompatActivity {

    FilterDialogLayoutBinding binding;
    FilterVM viewModel;

    long bankID;
   String startDate="",endDate="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(FilterActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.filter_dialog_layout);
        binding.setLifecycleOwner(FilterActivity.this);
        viewModel = ViewModelProviders.of(this).get(FilterVM.class);
        binding.setViewModel(viewModel);

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        min.add(Calendar.MONTH, -6);

        binding.calendarView.setMinimumDate(min);
        binding.calendarView.setMaximumDate(max);

        bankID=getIntent().getLongExtra("bankID",0);

        viewModel.getAccountList(bankID);
       // viewModel.getCardList(bankID);

        viewModel.accList.observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings)
            {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, strings);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                binding.accountNumsSpin.setAdapter(spinnerArrayAdapter);
            }
        });

//        viewModel.cardsList.observe(this, new Observer<List<String>>() {
//            @Override
//            public void onChanged(@Nullable List<String> strings)
//            {
//                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, strings);
//                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            }
//        });


        binding.searchBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try
                {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    List<Calendar> selectedDatesCalender=binding.calendarView.getSelectedDates();
                    if(selectedDatesCalender.size()>0)
                    {
                        startDate = String.valueOf(format.format(selectedDatesCalender.get(0).getTime())).trim()+"  00:00:00" ;
                        endDate = String.valueOf(format.format(selectedDatesCalender.get(selectedDatesCalender.size() - 1).getTime())).trim()+" 00:00:00";
                    }
                    if(endDate.equalsIgnoreCase(""))
                    {
                        endDate=startDate;
                    }

                    String flagedMsgs="";
                    String accNum="";

                    if(binding.flaggedMsgCB.isChecked())
                    {
                       flagedMsgs="true";
                    }
                    else
                    {
                        flagedMsgs="false";
                    }

                    if(binding.accountNumsSpin.getCount()>0)
                    {
                        if(binding.accountNumsSpin.getSelectedItemPosition()>0)
                        {
                            accNum = binding.accountNumsSpin.getSelectedItem().toString();
                        }
                        else
                        {
                            accNum="";
                        }
                    }
                    //String cardNum=binding.cardNumsSpin.getSelectedItem().toString();

                    if(!binding.amountFromTXT.getText().toString().equalsIgnoreCase(""))
                    {
                        if(binding.amountToTXT.getText().toString().equalsIgnoreCase(""))
                        {
                            Toast.makeText(getApplicationContext(),getString( R.string.fill_to_amount),  Toast.LENGTH_LONG).show();
                            return;

                        }
                    }


                    Intent data = new Intent();
                    data.putExtra("accNum",accNum);
                   // data.putExtra("cardNum",cardNum);
                    data.putExtra("amntFrom",binding.amountFromTXT.getText().toString());
                    data.putExtra("amntTo",binding.amountToTXT.getText().toString());
                    data.putExtra("startDate",startDate);
                    data.putExtra("endDate",endDate);
                    data.putExtra("flagged",flagedMsgs);
                    setResult(RESULT_OK, data);
                    finish();

                }
                catch (Exception xx){}

            }
        });

        binding.cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }



}
