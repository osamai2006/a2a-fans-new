package com.a2a.fans.views.activities;

import android.app.FragmentManager;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.a2a.fans.databinding.MainActivityBinding;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.viewmodels.MainScreenVM;
import com.a2a.fans.viewmodels.OtpVM;
import com.a2a.fans.views.Fragments.BanksListFragment;
import com.a2a.fans.views.Fragments.ChartsFragment;
import com.a2a.fans.views.Fragments.DashboardFragment;
import com.a2a.fans.views.Fragments.TransactionFragment;
import com.a2a.fans.R;

import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    MainActivityBinding binding;
    MainScreenVM viewModel;
    private Fragment fragment;
    FragmentTransaction tx;
    int backPressCounter = 0;
    boolean doubleBackToExitPressedOnce = false;

    //private NonSwipeableViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(MainActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.main_activity);
        binding.setLifecycleOwner(MainActivity.this);
        viewModel = ViewModelProviders.of(this).get(MainScreenVM.class);
        binding.setViewModel(viewModel);

       //customType(MainActivity.this,getResources().getString(R.string.animation_bottom_to_up));

      filterBTN= findViewById(R.id.filterBTN);

        android.support.v7.widget.Toolbar toolbar=findViewById ( R.id.main_toolbar );
        //Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.more_icon);
        //toolbar.setOverflowIcon(drawable);
        setSupportActionBar ( toolbar );
        setScreenTitle(getString(R.string.app_name));
        hideFilterBTN(true);



//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, toolbar,
//                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        binding.drawerLayout.addDrawerListener(toggle);
//        toggle.syncState();
//        binding.navigationView.inflateMenu(R.menu.nav_drawer_menu);
//        assert binding.navigationView != null;
//        binding.navigationView.setNavigationItemSelectedListener(MainActivity.this);
//        binding.navigationView.inflateHeaderView(R.layout.navigation_drawer_header);
//        View header = binding.navigationView.getHeaderView(0);

        binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(R.layout.tab_notificationlist_layout).setTag("0"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(R.layout.tab_dashboard_layout).setTag("1"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(R.layout.tab_charts_layout).setTag("2"));
        //binding.tabLayout.addTab(binding.tabLayout.newTab().setCustomView(R.layout.tab_transaction_layout).setTag("3"));


        binding.tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green_light));
        binding.tabLayout.setTabTextColors(getResources().getColor(R.color.White), getResources().getColor(R.color.White));
        binding.tabLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.setupWithViewPager(viewPager);


        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                if (tab.getTag().equals("0"))
                {
                    hideFilterBTN(true);
                    fragment = new BanksListFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();

                }
                else if (tab.getTag().equals("1"))
                {
                    hideFilterBTN(true);
                    fragment = new DashboardFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }
                else if (tab.getTag().equals("2"))
                {
                    hideFilterBTN(true);
                    fragment = new ChartsFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }
                else if (tab.getTag().equals("3"))
                {
                    hideFilterBTN(true);
                    fragment = new TransactionFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (tab.getTag().equals("0"))
                {
                    hideFilterBTN(true);
                    fragment = new BanksListFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    //tx.addToBackStack(null);
                    tx.commit();

                }
                else if (tab.getTag().equals("1"))
                {
                    hideFilterBTN(true);
                    fragment = new DashboardFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }
                else if (tab.getTag().equals("2"))
                {
                    hideFilterBTN(true);
                    fragment = new ChartsFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }
                else if (tab.getTag().equals("3"))
                {
                    hideFilterBTN(false);
                    fragment = new TransactionFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                }
            }
        });

        binding.langBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Language")
                        .setMessage("Are you sure you want to change the language?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(appConstants.current_language.equalsIgnoreCase("en"))
                                {
                                    appConstants.setInitialLanguage("ar",MainActivity.this);
                                    binding.langBTN.setText("Ar");
                                }
                                else
                                {
                                    appConstants.setInitialLanguage("en",MainActivity.this);
                                    binding.langBTN.setText("Eng");
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
        });


        fragment = new BanksListFragment();
        tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, fragment,"BanksListFragment");
        //tx.addToBackStack(null);
        tx.commit();



    }





//    private void setupViewPager(ViewPager viewPager) {
//
////        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.tv2));
////        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.layers));
//
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//
//        adapter.addFragment(new BanksListFragment(), getString(R.string.notific_list));
//        adapter.addFragment(new DashboardFragment(), getString(R.string.dashboard));
//        adapter.addFragment(new ChartsFragment(), getString(R.string.chart_txt));
//        adapter.addFragment(new ChartsFragment(), getString(R.string.transaction_txt));
//
//
//        viewPager.setAdapter(adapter);
//
//    }



    @Override
    public void onBackPressed() {

        try {

            //MainActivity.this.getIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


            BanksListFragment myFragment = (BanksListFragment)getSupportFragmentManager().findFragmentByTag("BanksListFragment");
            if (myFragment != null && myFragment.isVisible())
            {
                finish();
            }
            else
            {
                binding.tabLayout.getTabAt(0).select();

                fragment = new BanksListFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment,"BanksListFragment");
                //tx.addToBackStack(null);
                tx.commit();

            }

//            if (binding.tabLayout.getSelectedTabPosition() == 0)
//            {
//                if(backPressCounter>=1)
//                {
//                    finish();
//                }
//                backPressCounter+=1;
//
//            }

        }
        catch (Exception xx)
        {
            xx.toString();
            finish();
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {

            case R.id.setting_dr:
                fragment = new ChartsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;
        }
       // binding.drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
