package com.a2a.fans.views.Fragments;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LayoutAnimationController;

import com.a2a.fans.databinding.NotificationsListFragmentBinding;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.models.BankListModel;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.viewmodels.BanksListVM;
import com.a2a.fans.views.Adapters.BanksListAdapter;
import com.a2a.fans.R;

import java.util.List;

public class BanksListFragment extends Fragment implements BanksListAdapter.AdapterCallback {

    NotificationsListFragmentBinding binding;
   BanksListVM viewModel;
    MutableLiveData<List<BankListModel>> arrayList=new MutableLiveData<>();
    BanksListAdapter adapter;

    Animation anim;
    LayoutAnimationController controller;
    BroadcastReceiver receiver;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String token=sharedPrefs.getInstance().getStringPreference(getActivity(),appConstants.mobileToken_KEY);
        binding= DataBindingUtil.inflate(inflater, R.layout.notifications_list_fragment, container, false);
        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this).get(BanksListVM.class);
        binding.setViewModel(viewModel);


        View view = binding.getRoot();

        receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {

                long bankID=intent.getLongExtra("bankID",0);

                viewModel.fillBanksList();
                viewModel.bankList.observe(BanksListFragment.this, new Observer<List<BankListModel>>() {
                    @Override
                    public void onChanged(@Nullable List<BankListModel> banksListModels) {
                        arrayList.setValue(banksListModels);
                        initRecycleView();

                    }
                });

                if(appConstants.offlineMode==false)
                    viewModel.getBankInfo(bankID);

            }
        };

        if(appConstants.submitOnlineIsCalled==false && appConstants.offlineMode==false)
        {
            registerObserver(true);
        }


        viewModel.syncResult.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean)
                {
                    SQLHelper dbhelper;
                    dbhelper=new SQLHelper(getActivity().getApplicationContext());
                    dbhelper.open();
                    String query="update ROOM set actionName='',syncFlag='0' ";
                    dbhelper.Update(query);
                    dbhelper.close();

                }
            }
        });

        return view;
    }


    @Override
    public void onResume()
    {
        super.onResume();
        registerObserver(false);
    }




    private void registerObserver(boolean submitOnOnline)
    {
        viewModel.fillBanksList();


        viewModel.bankList.observe(BanksListFragment.this, new Observer<List<BankListModel>>() {
            @Override
            public void onChanged(@Nullable List<BankListModel> banksListModels)
            {
                arrayList.setValue(banksListModels);
                initRecycleView();

                if(submitOnOnline)
                {
                    if (appConstants.offlineMode == false)
                        viewModel.submitIsOnline(arrayList);
                }

            }
        });


   }
    private void initRecycleView()
    {
            if(arrayList.getValue().size()>0)
            {
                binding.noMsgImg.setVisibility(View.GONE);
            }
            else
            {
                binding.noMsgImg.setVisibility(View.VISIBLE);
            }
            adapter = new BanksListAdapter(getActivity().getApplicationContext(), arrayList,BanksListFragment.this);
            binding.contactsLV.setAdapter(adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            binding.contactsLV.setLayoutManager(layoutManager);

    }


    @Override
    public void onRecyclItemClicked(int itemID, int position, BankListModel itemModel) {

        if(itemID==R.id.mainLO)
        {
            viewModel.goToBankNotifications(itemModel.getID(),itemModel.getEDesc());
        }
    }

    @Override
    public void onRecyclItemLongClicked(int itemID, int position, BankListModel itemModel) {

        if(itemID==R.id.mainLO)
        {
//            if (arrayList.getValue().get(position).getIsPinned().equalsIgnoreCase("false"))
//            {
//                viewModel.pinned_unPinned_bank(arrayList.getValue().get(position).getId(), "true");
//                arrayList.getValue().get(position).setIsPinned("1");
//
//            } else if (arrayList.getValue().get(position).getIsPinned().equalsIgnoreCase("true"))
//            {
//                viewModel.pinned_unPinned_bank(arrayList.getValue().get(position).getId(), "false");
//                arrayList.getValue().get(position).setIsPinned("0");
//            }

//            viewModel.deleteAllData();
//            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((receiver),
                new IntentFilter("receiveAction"));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }
}
