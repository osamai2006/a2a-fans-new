package com.a2a.fans.views.Adapters;

import android.animation.ObjectAnimator;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.a2a.fans.models.CustomChartModel;
import com.a2a.fans.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CustomChartsAdapter extends RecyclerView.Adapter<CustomChartsAdapter.MenuItemViewHolder> {

    Context context;
     MutableLiveData<List<CustomChartModel>> resourcelList;
     int maxValue=0;



    public CustomChartsAdapter(Context context, MutableLiveData<List<CustomChartModel>> ResourcelList,int maxValue) {
        this.context = context;
        resourcelList = ResourcelList;
        this.maxValue=maxValue;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.accounts_custom_chart_layout, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {


        holder. bankNameTXT.setText(resourcelList.getValue().get(position).getBankName());
        holder.accountBalanceTXT.setText(resourcelList.getValue().get(position).getAccBalance());
        holder.cardBalanceTXT.setText(resourcelList.getValue().get(position).getCardBalance());



        //holder. accountBalanceChart.setProgress(Integer.parseInt(resourcelList.get(position).getAccBalance()));
        //holder. cardBalanceChart.setProgress(Integer.parseInt(resourcelList.get(position).getCardBalance()));


        //ObjectAnimator.ofInt(holder. accountBalanceChart, "progress", 79).start();

//        double rangeMin=0.1,rangeMax=1;
//        Random r = new Random();
//        double n = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
//        double maxAcc=n*Double.parseDouble(resourcelList.getValue().get(position).getAccBalance());
//        double maxCard=n*Double.parseDouble(resourcelList.getValue().get(position).getCardBalance());

//        holder.accountBalanceChart.setMax(150000);
//        holder.cardBalanceChart.setMax(150000);
//        double max=1.3*Double.parseDouble(resourcelList.getValue().get(position).getAccBalance());
//        double maxCard=1.3*Double.parseDouble(resourcelList.getValue().get(position).getCardBalance());
//        if(max<maxCard)
//        {
//            max=maxCard;
//        }


        if(resourcelList.getValue().get(position).getType().equalsIgnoreCase("last"))
        {
            holder.accountBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_orange));
            holder.cardBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_blue));

            holder.accountBalanceChart.setMax(maxValue);
            holder.cardBalanceChart.setMax(maxValue);
        }
        else  if(resourcelList.getValue().get(position).getType().equalsIgnoreCase("current"))
        {
            holder.accountBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_orange));
            holder.cardBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_blue));

            holder.accountBalanceChart.setMax(maxValue);
            holder.cardBalanceChart.setMax(maxValue);
        }
        else if(resourcelList.getValue().get(position).getType().equalsIgnoreCase("breakdown"))//breakdown
        {
            holder.accountBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_orange));
            holder.cardBalanceChart.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_states_blue));

            holder.accountBalanceChart.setMax(2000);
            holder.cardBalanceChart.setMax(2000);

        }


        double accBalanceINT=Double.parseDouble(resourcelList.getValue().get(position).getAccBalance());
        double cardBalanceINT=Double.parseDouble(resourcelList.getValue().get(position).getCardBalance());
        ObjectAnimator.ofInt(holder. accountBalanceChart, "progress", 0,(int) accBalanceINT).setDuration(2000).start();
        ObjectAnimator.ofInt(holder. cardBalanceChart, "progress", 0,(int)cardBalanceINT).setDuration(2000).start();


    }

    @Override
    public int getItemCount() {
        return resourcelList.getValue().size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        ProgressBar accountBalanceChart,cardBalanceChart;
        TextView bankNameTXT,accountBalanceTXT,cardBalanceTXT;


        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            accountBalanceChart = itemView.findViewById(R.id.accountBalanceChart);
            cardBalanceChart = itemView.findViewById(R.id.cardBalanceChart);
            accountBalanceTXT= itemView.findViewById(R.id.accountBalanceTXT);
            cardBalanceTXT= itemView.findViewById(R.id.cardBalanceTXT);
            bankNameTXT= itemView.findViewById(R.id.bankNameTXT);

        }

    }








}
