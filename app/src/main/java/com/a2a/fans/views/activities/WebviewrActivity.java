package com.a2a.fans.views.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.a2a.fans.R;
import com.a2a.fans.helpers.loadingWheel;

public class WebviewrActivity extends AppCompatActivity {

    WebView webView;
    public static String url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webviewr_activity);
        webView=findViewById(R.id.webView);


        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);



        webView.setWebViewClient(new WebViewClient() {



            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loadingWheel.startSpinwheel(WebviewrActivity.this,false,true);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadingWheel.stopLoading();
            }
        });

        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+url);

    }
}
