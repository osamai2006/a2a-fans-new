package com.a2a.fans.views.Adapters;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a2a.fans.models.ChatRoomModel;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.a2a.fans.R;


import java.util.List;


public class ChattingRoomAdapter extends RecyclerView.Adapter<ChattingRoomAdapter.MenuItemViewHolder> {


    public interface AdapterCallback
    {
        void onRecyclItemClicked(int itemID,int position,ChatRoomModel itemModel);
        void onRecyclItemLongClicked(int itemID,int position,ChatRoomModel itemModel);
    }


    Activity activity;
    MutableLiveData<List<ChatRoomModel>> resourcelList;

   public static boolean multiSelectMode=false;
    long bankID;
    private AdapterCallback callback;


    double longtitude = 0.0, latitude = 0.0;
    Animation animFromRight, animFromLeft;


    public ChattingRoomAdapter(Activity activity, MutableLiveData<List<ChatRoomModel>> ResourcelList,long bankID,AdapterCallback callback)
    {
        this.activity = activity;
        resourcelList = ResourcelList;
        animFromRight = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.dialog_in);
        animFromLeft = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_in_left);
        this.bankID=bankID;
        this.callback=callback;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatting_list_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final MenuItemViewHolder holder, final int position) {

        holder.msgTXT.setText(resourcelList.getValue().get(position).getMsgBody());
        holder.dateTXT.setText(resourcelList.getValue().get(position).getMsgDate());

        if (resourcelList.getValue().get(position).getMessageCat().equalsIgnoreCase("4"))//otp
        {
            holder.shareBTN.setVisibility(View.GONE);
            holder.reportBTN.setVisibility(View.GONE);
            holder.addFlagBTN.setVisibility(View.GONE);
            holder.copyBTN.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.shareBTN.setVisibility(View.VISIBLE);
            holder.reportBTN.setVisibility(View.VISIBLE);
            holder.addFlagBTN.setVisibility(View.VISIBLE);
            holder.copyBTN.setVisibility(View.GONE);
        }

//        if (resourcelList.getValue().get(position).getSender().equalsIgnoreCase("bank"))
//        {
//            //holder.msgLO.setGravity(Gravity.LEFT);
//
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT
//            );
//            params.setMargins(5, 5, 50, 5);
//            holder.mainLO.setLayoutParams(params);
//            holder.mainLO.setBackgroundResource(R.drawable.msg_bg_receiver);
//            // holder.mainLO.startAnimation(animFromLeft);
//            //holder.statusIMG.setVisibility(View.GONE);
//        }

//        else
//            {
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT
//            );
//            params.setMargins(50, 5, 5, 5);
//            holder.mainLO.setLayoutParams(params);
//            holder.mainLO.setBackgroundResource(R.drawable.msg_bg_sender);
//            //holder.mainLO.startAnimation(animFromRight);
//            //holder.statusIMG.setVisibility(View.VISIBLE);
//        }


        if (resourcelList.getValue().get(position).getIsFlagged().equalsIgnoreCase("false"))//unflagged
        {
            holder.flaggedIMG.setVisibility(View.GONE);

        } else if (resourcelList.getValue().get(position).getIsFlagged().equalsIgnoreCase("true"))//flagged
        {
            holder.flaggedIMG.setVisibility(View.VISIBLE);
        }


        if (resourcelList.getValue().get(position).getReadFlag().equalsIgnoreCase("false"))//unRead
        {
            holder.statusLO.setBackgroundResource(R.color.red_dark);

        } else if (resourcelList.getValue().get(position).getReadFlag().equalsIgnoreCase("true"))//red
        {
            holder.statusLO.setBackgroundResource(R.color.transparent);
        }


        if (resourcelList.getValue().get(position).getIsReported().equalsIgnoreCase("false"))//not reported
        {
            holder.reportedStatusIMG.setVisibility(View.GONE);

        } else if (resourcelList.getValue().get(position).getIsReported().equalsIgnoreCase("true"))//reported
        {
            holder.reportedStatusIMG.setVisibility(View.VISIBLE);
        }


        if (resourcelList.getValue().get(position).getMessageType().equalsIgnoreCase("1"))//txt
        {
            holder.msgTXT.setVisibility(View.VISIBLE);
            holder.msgContentIMG.setVisibility(View.GONE);
            holder.msgTXT.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            //holder.mapView.setVisibility(View.GONE);

        } else if (resourcelList.getValue().get(position).getMessageType().equalsIgnoreCase("2"))//image
        {
            //holder.msgTXT.setVisibility(View.GONE);
            holder.msgContentIMG.setVisibility(View.VISIBLE);
            Glide.with(activity.getApplicationContext()).load(resourcelList.getValue().get(position).getBase64())
                    //.apply(RequestOptions.circleCropTransform())
                    .transition(GenericTransitionOptions.with(R.anim.fadein))
                    .into(holder.msgContentIMG);
            holder.msgTXT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach, 0, 0, 0);
            // holder.mapView.setVisibility(View.GONE);

//            byte[] decodedString = Base64.decode(resourcelList.get(position).getBase64(), Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            holder.msgContentIMG.setImageBitmap(decodedByte);

        } else if (resourcelList.getValue().get(position).getMessageType().equalsIgnoreCase("3"))//pdf
        {
            holder.msgTXT.setVisibility(View.VISIBLE);
            holder.msgTXT.setTypeface(null, Typeface.BOLD);
            holder.msgContentIMG.setVisibility(View.GONE);

            holder.msgTXT.setCompoundDrawablesWithIntrinsicBounds( R.drawable.pdf_new_ic, 0, 0, 0);
        }



        if(resourcelList.getValue().get(position).getChecked().equalsIgnoreCase("true"))
        {
            holder.mainContainerLO.setBackgroundResource(R.color.marker_color);

        }
        else
        {
            holder.mainContainerLO.setBackgroundResource(R.color.transparent);
        }


        holder.msgContentIMG.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                callback.onRecyclItemClicked(holder.msgContentIMG.getId(),position,resourcelList.getValue().get(position));

            }
        });




        holder.msgLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                callback.onRecyclItemClicked(holder.msgLO.getId(),position,resourcelList.getValue().get(position));

//                if(resourcelList.get(position).getChecked().equalsIgnoreCase("true"))
//                {
//                    holder.mainContainerLO.setBackgroundResource(R.color.transparent);
//                }
//                else
//                {
//                    holder.mainContainerLO.setBackgroundResource(R.color.marker_color);
//                }

//                if(multiSelectMode)
//                {
//
//                    if(resourcelList.getValue().get(position).getChecked().equalsIgnoreCase("true"))
//                    {
//                        holder.mainContainerLO.setBackgroundResource(R.color.transparent);
//                    }
//                    else
//                    {
//                        holder.mainContainerLO.setBackgroundResource(R.color.marker_color);
//                    }
//                }

            }
        });


        holder.msgLO.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                callback.onRecyclItemLongClicked(holder.msgLO.getId(),position,resourcelList.getValue().get(position));

                multiSelectMode=true;
                //resourcelList.get(position).setChecked("true");
                holder.mainContainerLO.setBackgroundResource(R.color.marker_color);

                return true;
            }
        });



        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
//                try {
//                    YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.mainSwipLO));
//                }
//                catch (Exception xx)
//                {
//                    xx.toString();
//                }

            }
        });

        holder.shareBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onRecyclItemClicked(holder.shareBTN.getId(),position,resourcelList.getValue().get(position));


                 //ChattingActivity.showShareDialog(position);
            }
        });

        holder.reportBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onRecyclItemClicked(holder.reportBTN.getId(),position,resourcelList.getValue().get(position));

            }
        });

        holder.copyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onRecyclItemClicked(holder.copyBTN.getId(),position,resourcelList.getValue().get(position));

            }
        });


        holder.addFlagBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callback.onRecyclItemClicked(holder.addFlagBTN.getId(),position,resourcelList.getValue().get(position));


            }
        });


    }


    public static class MenuItemViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLO, msgLO, mainContainerLO, statusLO, mainSwipLO;
        TextView msgTXT, dateTXT;
        ImageView msgContentIMG, shareBTN, reportBTN, addFlagBTN, flaggedIMG, reportedStatusIMG,copyBTN;
        SwipeLayout swipeLayout;

        //MapView mapView;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            msgLO = itemView.findViewById(R.id.msgLO);
            dateTXT = itemView.findViewById(R.id.dateTXT);
            msgTXT = itemView.findViewById(R.id.msgTXT);
            //statusIMG= itemView.findViewById(R.id.statusIMG);
            //deleteBTN= itemView.findViewById(R.id.deleteBTN);
            msgContentIMG = itemView.findViewById(R.id.msgContentIMG);
            shareBTN = itemView.findViewById(R.id.shareBTN);
            reportBTN = itemView.findViewById(R.id.reportBTN);
            addFlagBTN = itemView.findViewById(R.id.addFlagBTN);
            copyBTN = itemView.findViewById(R.id.copyBTN);
            swipeLayout = itemView.findViewById(R.id.swipeLayout);
            mainContainerLO = itemView.findViewById(R.id.mainContainerLO);
            flaggedIMG = itemView.findViewById(R.id.flaggedIMG);
            statusLO = itemView.findViewById(R.id.statusLO);
            reportedStatusIMG = itemView.findViewById(R.id.reportedStatusIMG);
            mainSwipLO = itemView.findViewById(R.id.mainSwipLO);

            //mapView= itemView.findViewById(R.id.view_mapview);


        }

    }

    @Override
    public int getItemCount() {
        return resourcelList.getValue().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}
