package com.a2a.fans.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.a2a.fans.helpers.loadingWheel;
import com.a2a.fans.R;

public abstract class BaseActivity extends AppCompatActivity {


    TextView titleTXT;
    ImageView filterBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public void setScreenTitle(String title){
        titleTXT= findViewById(R.id.titleTXT);
        titleTXT.setText(title);
    }


    public void hideFilterBTN(boolean hide){
        filterBTN= findViewById(R.id.filterBTN);

        if(hide)
            filterBTN.setVisibility(View.INVISIBLE);
        else
            filterBTN.setVisibility(View.VISIBLE);
    }

    public void startLoadingWheel(){

        loadingWheel.startSpinwheel(this,false,true);

    }


}
