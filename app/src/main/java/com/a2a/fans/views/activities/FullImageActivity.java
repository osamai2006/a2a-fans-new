package com.a2a.fans.views.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.a2a.fans.R;
import com.jsibbold.zoomage.ZoomageView;

public class FullImageActivity extends AppCompatActivity {


  public static String imageURL="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen_image_activity);

        //ZoomAbleImageView imageView=(ZoomAbleImageView)findViewById(R.id.fullimage) ;
        ZoomageView imageView=findViewById(R.id.fullimage) ;
//        byte[] decodedString = Base64.decode(imageStr, Base64.DEFAULT);
//        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//        imageView.setImageBitmap(decodedByte);

        Glide.with(getApplicationContext()).load(imageURL)
                //.apply(RequestOptions.circleCropTransform())
                //.transition(GenericTransitionOptions.with(R.anim.fade_in))
                .into(imageView);

       }


//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//
//        return true;
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        if (id == android.R.id.home) {
//
//
//            finish();
//            return (true);
//        }
//
//
//
//
//
//
//
//
//        return super.onOptionsItemSelected(item);
//    }


}
