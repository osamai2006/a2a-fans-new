package com.a2a.fans.views.Adapters;

import android.animation.ValueAnimator;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.a2a.fans.models.BankListModel;
import com.a2a.fans.utls.appConstants;
import com.airbnb.lottie.LottieAnimationView;
import com.a2a.fans.R;

import java.util.List;


public class BanksListAdapter extends RecyclerView.Adapter<BanksListAdapter.MenuItemViewHolder> {

    public interface AdapterCallback
    {
        void onRecyclItemClicked(int itemID, int position, BankListModel itemModel);
        void onRecyclItemLongClicked(int itemID, int position, BankListModel itemModel);
    }

    private AdapterCallback callback;

    Context context;
    MutableLiveData<List<BankListModel>> resourcelList;

    Animation anim,animPop;



    public BanksListAdapter(Context context, MutableLiveData<List<BankListModel>> ResourcelList, AdapterCallback callback) {
        this.context = context;
        resourcelList = ResourcelList;
        anim = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
        animPop = AnimationUtils.loadAnimation(context, R.anim.glowing_animation);
        this.callback=callback;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final MenuItemViewHolder holder, final int position)  {

        try {
            holder.bankNameTXT.setText(resourcelList.getValue().get(position).getEDesc());
            //holder.msgCountTXT.setText("1");

//            Glide.with(context).load(resourcelList.get(position).getThumbImage())
//                    //.apply(RequestOptions.circleCropTransform())
//                    .transition(GenericTransitionOptions.with(R.anim.dialog_in))
//                    .into(holder.logoIMG);


            holder.logoIMG.setImageBitmap(appConstants.imageFrom64(resourcelList.getValue().get(position).getIcon()));


            holder.mainLO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    callback.onRecyclItemClicked(holder.mainLO.getId(),position,resourcelList.getValue().get(position));
                }
            });


            //holder.itemView.startAnimation(anim);

            if (!resourcelList.getValue().get(position).getNewMsgsCount().equalsIgnoreCase("0"))
            {
                holder.msgCountLO.setVisibility(View.VISIBLE);

                int msgCount = Integer.parseInt(resourcelList.getValue().get(position).getNewMsgsCount());
                ValueAnimator animator2 = ValueAnimator.ofInt(0, msgCount);
                animator2.setDuration(2000);
                animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        holder.msgCountTXT.setText(animation.getAnimatedValue().toString());
                    }
                });
                animator2.start();
            } else
                {
                holder.msgCountLO.setVisibility(View.INVISIBLE);
            }

            holder.lottieAnimationView.playAnimation();


            if (resourcelList.getValue().get(position).getIsPinned().equalsIgnoreCase("true")) {
                holder.pinnedIMG.setVisibility(View.VISIBLE);
            } else {
                holder.pinnedIMG.setVisibility(View.GONE);
            }


            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    callback.onRecyclItemLongClicked(holder.itemView.getId(),position,resourcelList.getValue().get(position));

                    return false;
                }
            });
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return resourcelList.getValue().size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout mainLO;
        RelativeLayout msgCountLO;
        TextView bankNameTXT,msgCountTXT ;
        ImageView logoIMG,pinnedIMG;
        LottieAnimationView lottieAnimationView;

        public MenuItemViewHolder(View itemView)
        {
            super(itemView);
            mainLO = itemView.findViewById(R.id.mainLO);
            bankNameTXT = itemView.findViewById(R.id.bankNameTXT);
            msgCountTXT= itemView.findViewById(R.id.msgCountTXT);
            logoIMG = itemView.findViewById(R.id.logoIMG);
            msgCountLO= itemView.findViewById(R.id.msgCountLO);
            pinnedIMG= itemView.findViewById(R.id.pinnedIMG);
            lottieAnimationView = itemView.findViewById(R.id.lottie_animation_view);


        }

    }









}
