package com.a2a.fans.views.activities;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.R;
import com.a2a.fans.viewmodels.RegistrationVM;
import com.a2a.fans.databinding.RegisterActivityBinding;
import com.a2a.fans.viewmodels.SplashVM;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import static maes.tech.intentanim.CustomIntent.customType;


public class registerActivity extends AppCompatActivity {

    RegisterActivityBinding binding;
    RegistrationVM registrationVM;

    AnimationDrawable anim ;
    CountryPicker countryPicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(registerActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.register_activity);
        binding.setLifecycleOwner(registerActivity.this);
        registrationVM = ViewModelProviders.of(this).get(RegistrationVM.class);
        binding.setViewModel(registrationVM);
         customType(registerActivity.this,"fadein-to-fadeout");



        //binding.tokenTXT.setText(sharedPrefs.getInstance().getStringPreference(getApplicationContext(), appConstants.mobileToken_KEY));

        registrationVM.grantPermissions();
        binding.countryTXT.setText(sharedPrefs.getInstance().getStringPreference(getApplicationContext(), appConstants.countryCode_KEY));
        binding.countryTXT.setEnabled(false);


        if(getIntent().getStringExtra("mobile")!=null)
        {
            binding.mobileTXT.setText(getIntent().getStringExtra("mobile"));

        }



        binding.registerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

               // startActivity(new Intent(registerActivity.this,MainActivity.class));
                if (binding.mobileTXT.getText().toString().trim().equals(""))
                {
                    Toast.makeText(AppController.getInstance().getApplicationContext(), R.string.fill_mobile_num, Toast.LENGTH_SHORT).show();
                    return;
                }

                String mobileNUM=binding.mobileTXT.getText().toString().trim();
                if(mobileNUM.startsWith("0"))
                {
                    mobileNUM=mobileNUM.substring(1,mobileNUM.length());
                }
                String msg=getApplicationContext().getString(R.string.confirm_mobile_nunm).replace("$",
                        binding.countryTXT.getText().toString()+" "+mobileNUM);

                String finalMobileNUM = mobileNUM;
                new AlertDialog.Builder(registerActivity.this)
                        .setTitle("Hint")
                        .setCancelable(false)
                        .setMessage(msg)
                        .setPositiveButton(R.string.continue_to_regist, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                registrationVM.requestRegistration(finalMobileNUM,binding.countryTXT.getText().toString());

                            }
                        })
                        .setNegativeButton(R.string.edit_mobile, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                return;
                            }
                        })
                        .show();




            }
        });

    }

}
