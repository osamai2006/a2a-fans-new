package com.a2a.fans.views.Fragments;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a2a.fans.databinding.DashboardFragmentBinding;
import com.a2a.fans.viewmodels.DashBoardVM;
import com.a2a.fans.viewmodels.SplashVM;
import com.a2a.fans.views.Adapters.BanksListAdapter;
import com.a2a.fans.views.Adapters.DashboardAdapter;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.models.DashboardModel;
import com.a2a.fans.R;
import com.a2a.fans.views.activities.SplashActivity;

import java.util.ArrayList;

public class DashboardFragment extends Fragment  {

    DashboardFragmentBinding binding;
    DashBoardVM viewModel;
    MutableLiveData<ArrayList<DashboardModel>> arrayList=new MutableLiveData<>();
    DashboardAdapter adapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding= DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false);
        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this).get(DashBoardVM.class);
        binding.setViewModel(viewModel);

        View view = binding.getRoot();

        viewModel.fillDashBoardList().observe(this, new Observer<ArrayList<DashboardModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<DashboardModel> dashboardModels) {

                arrayList.setValue(dashboardModels);
                initRecycleView();
            }
        });

        return view;
    }

    private void initRecycleView()
    {
        if(arrayList.getValue().size()>0)
        {
            adapter = new DashboardAdapter(getActivity().getApplicationContext(), arrayList);
            binding.dashBoardLV.setAdapter(adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            binding.dashBoardLV.setLayoutManager(layoutManager);
        }

    }

}
