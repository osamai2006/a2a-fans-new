package com.a2a.fans.views.activities;

import android.app.NotificationManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Toast;

import com.a2a.fans.databinding.SplashActivityBinding;
import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.viewmodels.SplashVM;

public class SplashActivity extends AppCompatActivity {

    SplashActivityBinding binding;
    SplashVM viewModel;
    Animation anim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setCurrentActivity(SplashActivity.this);
        binding= DataBindingUtil.setContentView(this,R.layout.splash_activity);
        binding.setLifecycleOwner(SplashActivity.this);
        viewModel = ViewModelProviders.of(this).get(SplashVM.class);
        binding.setViewModel(viewModel);


        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        viewModel.initLanguage();
        viewModel.initSplash();


        boolean isLoggedIn= sharedPrefs.getInstance().getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn_KEY,false);
        if(isLoggedIn)
        {
            binding.registerLO.setVisibility(View.GONE);

        }
        else
        {

            binding.registerLO.setVisibility(View.VISIBLE);

        }


        binding.termCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
              viewModel.readTerms();
            }
        });

        binding.registerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(binding.termCB.isChecked())
                {
                    viewModel.registerClicked();
                }
                else
                {
                    Toast.makeText(AppController.getInstance().getApplicationContext(), R.string.plz_accept_terms, Toast.LENGTH_SHORT).show();

                }

            }
        });


    }
}
