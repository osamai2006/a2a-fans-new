package com.a2a.fans.views.Fragments;

import android.animation.ValueAnimator;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.a2a.fans.views.Adapters.CustomChartsAdapter;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.models.CustomChartModel;
import com.a2a.fans.R;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;

import java.util.ArrayList;

public class TransactionFragment extends Fragment  {

    View view;
    ScatterChart mChart;
    TextView totalDepitTXT,totalCreidtTXT;
    static RecyclerView brekdownChartLV;
    static SQLHelper dbhelper;
    static Cursor banksCursor;
   static ArrayList<CustomChartModel> trxBreakDownArrayList;
    static CustomChartModel customChartModel;
    static CustomChartsAdapter breakDownadapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.transaction_chart_fragment, container, false);



        init();
        initChart();
        fillBreakDownChartData(getActivity());


        return view;
    }


    private void init()
    {

        totalCreidtTXT= view.findViewById(R.id.totalCreidtTXT);
        totalDepitTXT= view.findViewById(R.id.totalDepitTXT);

        brekdownChartLV= view.findViewById(R.id.brekdownChartLV);
        mChart = view.findViewById(R.id.chart1);

        float ceditBalance=Float.parseFloat(totalCreidtTXT.getText().toString());
        ValueAnimator animator = ValueAnimator.ofFloat(0,ceditBalance);
        animator.setDuration(2000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                totalCreidtTXT.setText(animation.getAnimatedValue().toString()+"00");
            }
        });
        animator.start();

        float depitBalance=Math.abs(Float.parseFloat(totalDepitTXT.getText().toString()));
        ValueAnimator animator2 = ValueAnimator.ofFloat(0,depitBalance);
        animator2.setDuration(2000);
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                totalDepitTXT.setText(animation.getAnimatedValue().toString()+"00");
            }
        });
        animator2.start();



    }




    private void initChart()
    {
        mChart.getDescription().setEnabled(false);

        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        mChart.setMaxHighlightDistance(50f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        mChart.setMaxVisibleValueCount(200);
        mChart.setPinchZoom(true);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setTextColor(getActivity().getResources().getColor(R.color.White));
        l.setDrawInside(false);
        l.setXOffset(5f);

        YAxis yl = mChart.getAxisLeft();
        yl.setAxisMinimum(-5f); // this replaces setStartAtZero(true)
        //yl.setAxisMaximum(100f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xl = mChart.getXAxis();
        xl.setDrawGridLines(false);

        setBreackDownChartDat();

//        // no description text
//        mChart.getDescription().setEnabled(false);
//        // enable touch gestures
//        mChart.setTouchEnabled(true);
//        mChart.setDragDecelerationFrictionCoef(0.9f);
//        // enable scaling and dragging
//        mChart.setDragEnabled(false);
//        mChart.setScaleEnabled(false);
//        mChart.setDrawGridBackground(false);
//        mChart.setHighlightPerDragEnabled(true);
//
//        // if disabled, scaling can be done on x- and y-axis separately
//        mChart.setPinchZoom(false);
//        // set an alternative background color
//        mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
//        // add data
//        setFirstChartData(10, 20);
//        mChart.animateX(2500);
    }


    private void setBreackDownChartDat()
    {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<Entry> yVals2 = new ArrayList<Entry>();
        ArrayList<Entry> yVals3 = new ArrayList<Entry>();

        for (int i = 0; i < 5; i++) {
            float val = (float) (Math.random() * 5) + 3;
            yVals1.add(new Entry(i, val));
        }

        for (int i = 0; i < 8; i++) {
            float val = (float) (Math.random() * 2) + 3;
            yVals2.add(new Entry(i+0.33f, val));
        }

        for (int i = 0; i < 8; i++) {
            float val = (float) (Math.random() * 2) + 3;
            yVals3.add(new Entry(i+0.66f, val));
        }

        // create a dataset and give it a type
        ScatterDataSet set1 = new ScatterDataSet(yVals1, "Arab Bank");
        set1.setScatterShape(ScatterChart.ScatterShape.SQUARE);
        set1.setValueTextColor(getActivity().getResources().getColor(R.color.White));
        set1.setColor(getActivity().getResources().getColor(R.color.red_dark));

        ScatterDataSet set2 = new ScatterDataSet(yVals2, "JCB");
        set2.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        set2.setScatterShapeHoleColor(getActivity().getResources().getColor(R.color.White));
        set2.setValueTextColor(getActivity().getResources().getColor(R.color.White));
        set2.setScatterShapeHoleRadius(3f);
        set2.setColor(getActivity().getResources().getColor(R.color.orange_dark));

        ScatterDataSet set3 = new ScatterDataSet(yVals3, "Cairo Amman");
        set3.setValueTextColor(getActivity().getResources().getColor(R.color.White));
        //set3.setShapeRenderer(new CustomScatterShapeRenderer());
        set3.setColor(getActivity().getResources().getColor(R.color.yello_color));

        set1.setScatterShapeSize(10f);
        set2.setScatterShapeSize(10f);
        set3.setScatterShapeSize(10f);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<IScatterDataSet>();
        dataSets.add(set1); // add the datasets
        dataSets.add(set2);
        dataSets.add(set3);

        // create a data object with the datasets
        ScatterData data = new ScatterData(dataSets);

        mChart.getXAxis().setTextColor(getActivity().getResources().getColor(R.color.White));
        mChart.getAxisLeft().setTextColor(getActivity().getResources().getColor(R.color.White));
        mChart.getAxisRight().setTextColor(getActivity().getResources().getColor(R.color.White));
        mChart.animateXY(3000, 3000);
        mChart.setData(data);
        mChart.invalidate();
    }



//    private void setFirstChartData(int count, float range) {
//
//        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
//
//        for (int i = 0; i < count; i++) {
//            float mult = range / 2f;
//            float val = (float) (Math.random() * mult) + 50;
//            yVals1.add(new Entry(i, val));
//        }
//
//        ArrayList<Entry> yVals2 = new ArrayList<Entry>();
//
//        for (int i = 0; i < count-1; i++) {
//            float mult = range;
//            float val = (float) (Math.random() * mult) + 450;
//            yVals2.add(new Entry(i, val));
////            if(i == 10) {
////                yVals2.add(new Entry(i, val + 50));
////            }
//        }
//
////        ArrayList<Entry> yVals3 = new ArrayList<Entry>();
////
////        for (int i = 0; i < count; i++) {
////            float mult = range;
////            float val = (float) (Math.random() * mult) + 500;
////            yVals3.add(new Entry(i, val));
////        }
//
//        LineDataSet set1, set2;// set3;
//
//        if (mChart.getData() != null &&
//                mChart.getData().getDataSetCount() > 0) {
//            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
//            set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
//            //set3 = (LineDataSet) mChart.getData().getDataSetByIndex(2);
//            set1.setValues(yVals1);
//            set2.setValues(yVals2);
//            //set3.setValues(yVals3);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            // create a dataset and give it a type
//            set1 = new LineDataSet(yVals1, "Total Credit");
//
//            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//            set1.setColor(getActivity().getResources().getColor(R.color.blue_light));
//            set1.setCircleColor(Color.WHITE);
//            set1.setLineWidth(2f);
//            set1.setCircleRadius(2f);
//            set1.setFillAlpha(15);
//            set1.setFillColor(getActivity().getResources().getColor(R.color.blue_light));
//            set1.setHighLightColor(getActivity().getResources().getColor(R.color.blue_light));
//            set1.setDrawCircleHole(true);
//            //set1.setFillFormatter(new MyFillFormatter(0f));
//            //set1.setDrawHorizontalHighlightIndicator(false);
//            //set1.setVisible(false);
//            //set1.setCircleHoleColor(Color.WHITE);
//
//            // create a dataset and give it a type
//            set2 = new LineDataSet(yVals2, "Total Debit");
//            set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
//            set2.setColor(getActivity().getResources().getColor(R.color.orange_dark));
//            set2.setCircleColor(Color.WHITE);
//            set2.setLineWidth(2f);
//            set2.setCircleRadius(3f);
//            set2.setFillAlpha(65);
//            set2.setFillColor(getActivity().getResources().getColor(R.color.orange_dark));
//            set2.setDrawCircleHole(false);
//            set2.setHighLightColor(getActivity().getResources().getColor(R.color.orange_dark));
//            //set2.setFillFormatter(new MyFillFormatter(900f));
//
////            set3 = new LineDataSet(yVals3, "DataSet 3");
////            set3.setAxisDependency(YAxis.AxisDependency.RIGHT);
////            set3.setColor(Color.YELLOW);
////            set3.setCircleColor(Color.WHITE);
////            set3.setLineWidth(2f);
////            set3.setCircleRadius(3f);
////            set3.setFillAlpha(65);
////            set3.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
////            set3.setDrawCircleHole(false);
////            set3.setHighLightColor(Color.rgb(244, 117, 117));
//
//            // create a data object with the datasets
//            LineData data = new LineData(set1, set2);
//            data.setValueTextColor(Color.WHITE);
//            data.setValueTextSize(9f);
//
//            // set data
//            mChart.getXAxis().setTextColor(getActivity().getResources().getColor(R.color.White));
//            mChart.getAxisLeft().setTextColor(getActivity().getResources().getColor(R.color.White));
//            mChart.getAxisRight().setTextColor(getActivity().getResources().getColor(R.color.White));
//            mChart.setData(data);
//
//            Legend l = mChart.getLegend();
//
//            // modify the legend ...
//            l.setForm(Legend.LegendForm.LINE);
//            //l.setTypeface(mTfLight);
//            l.setTextSize(11f);
//            l.setTextColor(Color.WHITE);
//            l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//            l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//            l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//            l.setDrawInside(false);
////        l.setYOffset(11f);
//
////            XAxis xAxis = mChart.getXAxis();
////            //xAxis.setTypeface(mTfLight);
////            xAxis.setTextSize(11f);
////            xAxis.setTextColor(Color.WHITE);
////            xAxis.setDrawGridLines(false);
////            xAxis.setDrawAxisLine(false);
////
////            YAxis leftAxis = mChart.getAxisLeft();
////            //leftAxis.setTypeface(mTfLight);
////            leftAxis.setTextColor(Color.WHITE);
////            leftAxis.setAxisMaximum(2000);
////            leftAxis.setAxisMinimum(-1000);
////            leftAxis.setDrawGridLines(true);
////            leftAxis.setGranularityEnabled(true);
////
////            YAxis rightAxis = mChart.getAxisRight();
////            //rightAxis.setTypeface(mTfLight);
////            rightAxis.setTextColor(Color.WHITE);
////            rightAxis.setAxisMaximum(2000);
////            rightAxis.setAxisMinimum(-1000);
////            rightAxis.setDrawGridLines(false);
////            rightAxis.setDrawZeroLine(false);
////            rightAxis.setGranularityEnabled(false);
//        }
//    }

    public static void fillBreakDownChartData(final Context context)
    {
        try
        {
            final Animation animFromRight= AnimationUtils.loadAnimation(context, R.anim.slide_in_left);

            dbhelper=new SQLHelper(context);
            dbhelper.open();
            trxBreakDownArrayList=new ArrayList<>();

            String query="select id,name,image,status from BANKS ";

            banksCursor = dbhelper.Select(query, null);
            String trxCounter_Accounts="0";
            String trxCounter_Cards="0";
            String bankID="";
            String bankName="";


            if(banksCursor.moveToFirst())
            {


                do {
                    String bank_id=banksCursor.getString(0);

                    String queryTrxCard="select  b.id,b.name,b.image,r.account_balance ,r.card_balance,count(r.id) cardTrxCount " +
                            " from room r,banks b where  B.id=R.bank_id and r.msgCategory in ('1','2') and r.sender='bank' and b.id='"+bank_id+"' " +
                            " and r.message like '%card%' ";

                    String queryTrxAcc="select  b.id,b.name,b.image,r.account_balance ,r.card_balance,count(r.id) cardTrxCount " +
                            " from room r,banks b where  B.id=R.bank_id and r.msgCategory in ('1','2') and r.sender='bank' and b.id='"+bank_id+"' " +
                            " and r.message not like '%card%' ";

                    Cursor trxCardCursor=dbhelper.Select(queryTrxCard,null);
                    if(trxCardCursor.moveToFirst())
                    {
                        trxCounter_Cards=trxCardCursor.getString(5);
                         bankID=trxCardCursor.getString(0);
                         bankName=trxCardCursor.getString(1);

                    }

                    Cursor trxAccCursor=dbhelper.Select(queryTrxAcc,null);
                    if(trxAccCursor.moveToFirst())
                    {
                        trxCounter_Accounts=trxAccCursor.getString(5);

                    }


                    customChartModel = new CustomChartModel(bankID,bankName,trxCounter_Accounts,trxCounter_Cards,"breakdown");
                    trxBreakDownArrayList.add(customChartModel);

                }
                while (banksCursor.moveToNext());

                Handler handler = new Handler(Looper.getMainLooper()) {
                    @Override
                    public void handleMessage(Message msg) {

//                        breakDownadapter = new CustomChartsAdapter(context,trxBreakDownArrayList);
//                        brekdownChartLV.setAdapter(breakDownadapter);
//                        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//                        brekdownChartLV.setLayoutManager(layoutManager);
//                        brekdownChartLV.startAnimation(animFromRight);


                    }
                };
                handler.sendEmptyMessage(1);





            }
        }
        catch (Exception xx){}
    }



}
