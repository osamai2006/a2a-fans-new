package com.a2a.fans.views.Fragments;

import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.a2a.fans.databinding.ChartsFragmentBinding;
import com.a2a.fans.viewmodels.BanksListVM;
import com.a2a.fans.viewmodels.ChartsListVM;
import com.a2a.fans.views.Adapters.BanksListAdapter;
import com.a2a.fans.views.Adapters.CustomChartsAdapter;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.models.CustomChartModel;
import com.a2a.fans.R;

import java.util.ArrayList;
import java.util.List;

public class ChartsFragment extends Fragment {

    View view;
    ChartsFragmentBinding binding;
    ChartsListVM viewModel;

    MutableLiveData<List<CustomChartModel>> currentChartArrayList=new MediatorLiveData<>();
    MutableLiveData<List<CustomChartModel>> previousChartArrayList=new MediatorLiveData<>();
     //ArrayList<CustomChartModel> currentChartArrayList;
    // ArrayList<CustomChartModel> lastChartArrayList;
     CustomChartsAdapter adapter;
     Animation animFromRight;
     Animation animFromLeft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater, R.layout.charts_fragment, container, false);
        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this).get(ChartsListVM.class);
        binding.setViewModel(viewModel);

        View view = binding.getRoot();


        animFromRight = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
        animFromLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);

        //initRecycleViewCurrentMonth();
       // initRecycleViewPrevoiusMonth();
        //fillCurrentMonthData(getActivity());
       // fillLastMonthData(getActivity());

        viewModel.fillCurrentMonthData();
        viewModel.fillPreviosMonthData();

        viewModel.currentMonthList.observe(this, new Observer<ArrayList<CustomChartModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<CustomChartModel> customChartModels)
            {
                currentChartArrayList.setValue(customChartModels);
                initRecycleViewCurrentMonth();

            }
        });

        viewModel.previousMonthList.observe(this, new Observer<ArrayList<CustomChartModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<CustomChartModel> customChartModels)
            {
                previousChartArrayList.setValue(customChartModels);
                initRecycleViewPrevoiusMonth();

            }
        });

        return view;
    }



    private void initRecycleViewCurrentMonth()
    {
        int max=viewModel.getMax(currentChartArrayList)+100;

        adapter = new CustomChartsAdapter(getActivity().getApplicationContext(),currentChartArrayList,max);
        binding.currentMonthChartLV.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.currentMonthChartLV.setLayoutManager(layoutManager);
        binding.currentMonthChartLV.startAnimation(animFromRight);

    }

    private void initRecycleViewPrevoiusMonth()
    {
        int max=viewModel.getMax(previousChartArrayList)+100;

        adapter = new CustomChartsAdapter(getActivity().getApplicationContext(),previousChartArrayList,max);
        binding.lastMonthChartLV.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.lastMonthChartLV.setLayoutManager(layoutManager);
        binding.lastMonthChartLV.startAnimation(animFromRight);

    }


}
