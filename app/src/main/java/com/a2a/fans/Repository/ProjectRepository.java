package com.a2a.fans.Repository;

import android.arch.lifecycle.MutableLiveData;
import android.database.Cursor;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.GetResponseBody;
import com.a2a.fans.helpers.RequestBodyCreator;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.models.AccountsModel;
import com.a2a.fans.models.BanksModel;
import com.a2a.fans.models.ChatRoomModel;
import com.a2a.fans.models.IsOnlineModel;
import com.a2a.fans.models.RegistrationModel;
import com.a2a.fans.models.DashboardModel;
import com.a2a.fans.models.MessageModel;
import com.a2a.fans.models.VersionModel;
import com.a2a.fans.networking.RequestHandler;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.utls.appConstants;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements RequestHandler.ResponseHandler {

    private static SQLHelper dbhelper;
    private   Cursor dataCursor;
    //BanksListModel banksListModel;
    DashboardModel dashboardModel;



    private  static ProjectRepository.ResponseHandler responseHandler;

    private static ProjectRepository instance;
    public static ProjectRepository getInstance(ProjectRepository.ResponseHandler _responseHandler)
    {
        if(instance == null)
        {
            instance = new ProjectRepository();
        }
        responseHandler=_responseHandler;
        dbhelper=new SQLHelper(AppController.getInstance().getApplicationContext());
        dbhelper.open();
        return instance;
    }


    //public MutableLiveData<PublicKeyModel> publicKeyModel=new MutableLiveData<>();


//    public MutableLiveData<List<BanksListModel>> fillBanksList()
//    {
//        MutableLiveData<List<BanksListModel>> arrayList = new MutableLiveData<>();
//        List<BanksListModel> list=new ArrayList<>();
//        try
//        {
//            dataCursor=dbhelper.Select("select id,name,image,status,thumb_image,isPinned,promo_code,modified_date" +
//                           " from BANKS order by cast(isPinned as integer) desc",null);
//            if(dataCursor.moveToFirst())
//            {
//                String msgCounte="";
//
//
//                do {
//                    String id=dataCursor.getString(0);
//                    String name=dataCursor.getString(1);
//                    String image=dataCursor.getString(2);
//                    String status=dataCursor.getString(3);
//                    String thumb_image=dataCursor.getString(4);
//                    String isPinned=dataCursor.getString(5);
//                    String promoCode=dataCursor.getString(6);
//                    String modifidDate=dataCursor.getString(7);
//
//                    String query="select count(id) from room where bank_id='"+id+"' and status='false'";
//                    Cursor counterCursor=dbhelper.Select(query,null);
//                    if(counterCursor.moveToFirst())
//                    {
//                        msgCounte=counterCursor.getString(0);
//                    }
//
//                    banksListModel = new BanksListModel(id,name,image,status,thumb_image,isPinned,msgCounte,promoCode,modifidDate);
//                    list.add(banksListModel);
//
//                }
//                while (dataCursor.moveToNext());
//                arrayList.postValue(list);
//
//
//            }
//            return arrayList;
//        }
//        catch (Exception xx)
//        {
//            xx.getMessage();
//            return null;
//        }
//    }

    public  MutableLiveData<ArrayList<DashboardModel>>  fillDashboardData()
    {
        ArrayList<DashboardModel> arrayList = new ArrayList<>();
        MutableLiveData<ArrayList<DashboardModel>> list=new MutableLiveData<>();

        try
        {

            String query="select id,name,image,status from BANKS ";

            dataCursor = dbhelper.Select(query, null);

            if(dataCursor.moveToFirst())
            {
                arrayList=new ArrayList<>();

                do {
                    String bank_id=dataCursor.getString(0);
                    String accBalnce="0",cardBalnce="0",bankName="",bankImage="";

                    //and R.deleteFlag=''

                    Cursor accBalanceCurs=dbhelper.Select("select b.id,b.name,b.image,r.account_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('2') and r.sender='bank' and b.id='"+bank_id+"'" +
                            "  order by cast(r.rowid as integer) desc limit 1",null);

                    Cursor cardBalanceCurs=dbhelper.Select("select b.id,b.name,b.image,r.card_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('1') and r.sender='bank' and b.id='"+bank_id+"' " +
                            " order by cast(r.rowid as integer) desc limit 1",null);

                    if (accBalanceCurs.moveToFirst())
                    {
                        bankName=accBalanceCurs.getString(1);
                        bankImage=accBalanceCurs.getString(2);
                        accBalnce=accBalanceCurs.getString(3);
                    }

                    if (cardBalanceCurs.moveToFirst())
                    {
                        bankName=cardBalanceCurs.getString(1);
                        bankImage=cardBalanceCurs.getString(2);
                        cardBalnce=cardBalanceCurs.getString(3);
                    }

                    if(bankImage.equalsIgnoreCase(""))
                    {
                        Cursor bankImgCursor=dbhelper.Select("select b.id,b.name,b.image,r.card_balance  from room r,banks b where  B.id=R.bank_id" +
                                " and b.id='"+bank_id+"' order by cast(r.rowid as integer) desc limit 1",null);
                        if (bankImgCursor.moveToFirst())
                        {
                            bankImage=bankImgCursor.getString(2);
                        }

                    }

                    dashboardModel = new DashboardModel(bank_id,bankName,bankImage,cardBalnce,accBalnce);
                    arrayList.add(dashboardModel);

//                    Cursor balanceCursor=dbhelper.Select("select b.id,b.name,b.image,r.account_balance ,r.card_balance from room r,banks b where  B.id=R.bank_id" +
//                            " and r.msgType in('1','2') and r.msgCategory in ('1','2') " +
//                            "and r.sender='bank' and b.id='"+bank_id+"' and R.deleteFlag='' order by cast(r.rowid as integer) desc limit 1",null);

//                    if(balanceCursor.moveToFirst())
//                    {
//                        String bankID=balanceCursor.getString(0);
//                        String bankName=balanceCursor.getString(1);
//                        String bankImage=balanceCursor.getString(2);
//                        String account_balance=balanceCursor.getString(3);
//                        String card_balance=balanceCursor.getString(4);
//
//                        dashboardModel = new DashboardModel(bankID,bankName,bankImage,card_balance,account_balance);
//                        arrayList.add(dashboardModel);
//
//
//                    }


                }
                while (dataCursor.moveToNext());
                list.postValue(arrayList);

            }

            return list;
        }
        catch (Exception xx)
        {
            return list;
        }
    }

//    public  MutableLiveData<List<ChatRoomModel>>  fillMessageList(final String bankID, String groupBY)
//    {
//
//        MutableLiveData<List<ChatRoomModel>>  chatList=new MediatorLiveData<>();
//        ChatRoomModel chatRoomModel;
//        ArrayList<ChatRoomModel> arrayList=new ArrayList<>();
//
//        try {
//
//
////            swipRefresh.setRefreshing(false);
//
//            String query="select B.id,B.name,B.image,B.status,R.id,R.message,R.date,R.readFlag,R.sender,R.msgCategory,R.msgType," +
//                    "R.base64,R.locationXY,R.trxAmount,R.hasFlag,R.isReported,R.reportReason,R.actionName,R.deleteFlag " +
//                    " from BANKS B,ROOM R WHERE B.id=R.bank_id and R.bank_id='" + bankID +"' and R.deleteFlag='' ";
//
//            if(groupBY.equalsIgnoreCase("1"))//cards
//            {
//                query=query+" and R.sender='bank' and R.msgCategory='1' ";
//            }
//            else  if(groupBY.equalsIgnoreCase("2"))//accounts
//            {
//                query=query+" and R.sender='bank' and R.msgCategory='2' ";
//            }
//            else  if(groupBY.equalsIgnoreCase("3"))//ads
//            {
//                query=query+" and R.sender='bank' and R.msgCategory='3' ";
//            }
//            else  if(groupBY.equalsIgnoreCase("4"))//otp
//            {
//                query=query+" and R.sender='bank' and R.msgCategory='4' ";
//            }
//
//            //dbhelper.Update("update room set status='0' where id in (select id from room Where bank_id='2' order by cast(rowid as integer) desc limit 3)");//for test purpose
//
//            Cursor  dataCursor = dbhelper.Select(query, null);
//
//            if (dataCursor.moveToFirst())
//            {
//                arrayList = new ArrayList<>();
//                arrayList.clear();
//
//                do {
//                    String bankId = dataCursor.getString(0);
//                    String bankName = dataCursor.getString(1);
//                    String bankImage = dataCursor.getString(2);
//                    String bankStatus = dataCursor.getString(3);
//                    String msgId = dataCursor.getString(4);
//                    String msgBody = dataCursor.getString(5);
//                    String msgDate = dataCursor.getString(6);
//                    String readFlag = dataCursor.getString(7);
//                    String sender = dataCursor.getString(8);
//                    String msgCat = dataCursor.getString(9);
//                    String msgtype = dataCursor.getString(10);
//                    String base64 = dataCursor.getString(11);
//                    String locationXY = dataCursor.getString(12);
//                    String trxAmount = dataCursor.getString(13);
//                    String hasFlag = dataCursor.getString(14);
//                    String isReported = dataCursor.getString(15);
//                    String reportReason = dataCursor.getString(16);
//                    String actionName = dataCursor.getString(17);
//                    String deleteFlag = dataCursor.getString(18);
//
//                    chatRoomModel = new ChatRoomModel(msgId, msgBody, msgDate, readFlag, sender,msgCat,msgtype,
//                            base64,locationXY,"false",trxAmount,hasFlag,isReported,reportReason,actionName,deleteFlag);
//                    arrayList.add(chatRoomModel);
//
//                }
//                while (dataCursor.moveToNext());
//                chatList.postValue(arrayList);
//
//
//            }
//            else
//            {
//                arrayList.clear();
//                chatList.postValue(arrayList);
//            }
//
//            return chatList;
//
//        }
//        catch (Exception xx)
//        {
//            String xxx=xx.getMessage();
//            return chatList;
//        }
//
//    }

    public  MutableLiveData<List<ChatRoomModel>>  searchChatRoomData(final long bankID, String keywordTXT, String amountFromTXT, String amountToTXT, String startDate, String endDate, String accNum, String flaggedMesg)
    {

        ArrayList<ChatRoomModel> arrayList=new ArrayList<>();
        ChatRoomModel chatRoomModel;
        MutableLiveData<List<ChatRoomModel>> list=new MutableLiveData<>();

        try {


            String query="select B.id,B.name,B.image,B.status,R.id,R.message,R.date,R.readFlag,R.sender,R.msgCategory,R.msgType," +
                    "R.base64,R.locationXY,R.trxAmount,R.hasFlag,R.isReported,R.reportReason,R.actionName,R.deleteFlag " +
                    " from BANKS B,ROOM R WHERE B.id=R.bank_id and R.bank_id='" + bankID +"' and R.deleteFlag='' ";

            if(!keywordTXT.equalsIgnoreCase(""))
            {
                query=query+" AND R.message LIKE '%"+keywordTXT+"%' ";
            }

            if(!accNum.equalsIgnoreCase(""))
            {
                query=query+" AND R.accountNumber = '"+accNum+"' ";
            }

//            if(!cardNum.equalsIgnoreCase(""))
//            {
//                query=query+" OR R.cardNumber = '"+cardNum+"' ";
//            }

            if(!amountFromTXT.equalsIgnoreCase(""))
            {
                query=query+" AND R.trxAmount between cast('"+amountFromTXT+"' as integer) and cast('"+amountToTXT+"' as integer) ";
            }

            if(!flaggedMesg.equalsIgnoreCase(""))
            {
                query=query+" AND R.hasFlag='"+flaggedMesg+"' ";
            }

            if(!startDate.equalsIgnoreCase(""))
            {
                query=query+" AND date BETWEEN '"+startDate+"' AND '"+endDate+"'";
            }



            dataCursor = dbhelper.Select(query, null);

            if (dataCursor.moveToFirst()) {
                arrayList.clear();

                do {
                    String bankId = dataCursor.getString(0);
                    String bankName = dataCursor.getString(1);
                    String bankImage = dataCursor.getString(2);
                    String bankStatus = dataCursor.getString(3);
                    String msgId = dataCursor.getString(4);
                    String msgBody = dataCursor.getString(5);
                    String msgDate = dataCursor.getString(6);
                    String readFlag = dataCursor.getString(7);
                    String sender = dataCursor.getString(8);
                    String msgCat = dataCursor.getString(9);
                    String msgtype = dataCursor.getString(10);
                    String base64 = dataCursor.getString(11);
                    String locationXY = dataCursor.getString(12);
                    String trxAmount = dataCursor.getString(13);
                    String hasFlag = dataCursor.getString(14);
                    String isReported = dataCursor.getString(15);
                    String reportReason = dataCursor.getString(16);
                    String actionName = dataCursor.getString(17);
                    String deleteFlag = dataCursor.getString(18);

                    chatRoomModel = new ChatRoomModel(msgId, msgBody, msgDate, readFlag, sender,msgCat,msgtype,
                                  base64,locationXY,"false",trxAmount,hasFlag,isReported,reportReason,actionName,deleteFlag);
                    arrayList.add(chatRoomModel);

                }
                while (dataCursor.moveToNext());
                list.postValue(arrayList);


            }
            else
            {
                arrayList.clear();
                list.postValue(arrayList);

            }
        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
        }

        return list;

    }



    public void getPublicKEY()
    {
        try
        {
            JSONObject requestBody = new JSONObject();
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetCerPublicKey,false);

        }
        catch (Exception xx){}

    }

    public void getVersion()
    {
        try
        {
            JSONObject A2ABody = new JSONObject();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("ConutryName", appConstants.countryName.toUpperCase());
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetVersion,true);

        }
        catch (Exception xx)
        {}


    }

    public void registration(String token)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("Token", token);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.Registration,true);

        }
        catch (Exception xx){}

    }

    public void refreshToken(String token)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("Token", token);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.RefreshToken,true);

        }
        catch (Exception xx){}

    }

    public void getAccountsNum(long bankID)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(bankID);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetAccount,true);

        }
        catch (Exception xx){}

    }

//    public void getCardsNum(String bankID)
//    {
//        try
//        {
//            JSONObject A2ABody = new JSONObject();
//            A2ABody= RequestBodyCreator.getInstance().createRequestObject(bankID,A2ABody);
//            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetCard,true);
//
//        }
//        catch (Exception xx){}
//
//    }



    public void authntecation(String otp,String utr)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("OTP",otp);
            A2ABody.putOpt("UTR",utr);
            A2ABody.putOpt("Token", sharedPrefs.getInstance().getStringPreference(AppController.getInstance().getApplicationContext(),appConstants.mobileToken_KEY));
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.Authentication,true);

        }
        catch (Exception xx){}

    }

    public void submitIsOnline(JSONArray bankArray)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("Bank", bankArray );
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.IsOnline,true);

        }
        catch (Exception xx){}

    }
    public void deleteMessage(JSONArray idsArray)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("MsgIDs", idsArray );
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.DeleteMsg,true);

        }
        catch (Exception xx){}

    }

    public void updateMsgStatus(String msgID,String isMsgFlaged,String reportFlag,String reportReason)
    {
        try
        {
            if(reportReason.equalsIgnoreCase(""))
            { reportReason="0";}

            if(reportFlag.equalsIgnoreCase(""))
            { reportFlag="false";}

            JSONObject A2ABody = new JSONObject();
            JSONObject msgBody = new JSONObject();
            JSONArray msgArr=new JSONArray();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            msgBody.putOpt("MsgID", msgID );
            msgBody.putOpt("Flag", isMsgFlaged );
            msgBody.putOpt("ReportFlag", reportFlag );
            msgBody.putOpt("ReportReasonID", reportReason );
            msgArr.put(msgBody);
            A2ABody.putOpt("Alerts",msgArr);

            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.SetMsgStatus,true);

        }
        catch (Exception xx){}

    }

    public void updateMsgStatus(JSONArray msgArr)
    {
        try
        {

            JSONObject A2ABody = new JSONObject();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("Alerts",msgArr);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.SetMsgStatus,true);

        }
        catch (Exception xx){}

    }

    public void updateMsgAsRed(long bankID)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(bankID);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.SetUpdateMsgAsRead,true);

        }
        catch (Exception xx){}

    }

    public void getBankInfo(long bankID)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(bankID);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetBankInfo,true);

        }
        catch (Exception xx){}

    }

    public void getTrx(long bankID)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();
            A2ABody= RequestBodyCreator.getInstance().createRequestObject(bankID);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.GetTrx,true);

        }
        catch (Exception xx){}

    }

    public void resendSMS(String otp,String utr)
    {
        try
        {
            JSONObject A2ABody = new JSONObject();

            A2ABody= RequestBodyCreator.getInstance().createRequestObject(0);
            A2ABody.putOpt("OTP",otp);
            A2ABody.putOpt("UTR",utr);
            RequestHandler.getInstance(this).requestAPI(A2ABody, URLClass.Resend,true);

        }
        catch (Exception xx){}

    }

    @Override
    public void successRequest(JSONObject response, String apiName) {

        try
        {

            if (apiName.equalsIgnoreCase(URLClass.GetCerPublicKey))
            {
                responseHandler.successRequest(null,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.GetVersion))
            {
                Gson gson = new Gson();
                Object  classObj = gson.fromJson(response.toString(), VersionModel.class);
                responseHandler.successRequest((VersionModel)classObj,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.Registration))
            {
                Gson gson = new Gson();
                Object  classObj = gson.fromJson(response.toString(), RegistrationModel.class);
                responseHandler.successRequest((RegistrationModel)classObj,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.Authentication))
            {
//                Gson gson = new Gson();
//                Object  classObj = gson.fromJson(GetResponseBody.getResponseBody(response).toString(), OTPModel.class);
                responseHandler.successRequest(null,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.IsOnline))
            {
                Gson gson = new Gson();
                Object classObj = gson.fromJson(response.toString(), IsOnlineModel.class);
                responseHandler.successRequest((IsOnlineModel)classObj,apiName);

            }

            if (apiName.equalsIgnoreCase(URLClass.Resend))
            {
                Gson gson = new Gson();
                Object  classObj = gson.fromJson(response.toString(), RegistrationModel.class);
                responseHandler.successRequest(classObj,apiName);

            }

            if (apiName.equalsIgnoreCase(URLClass.GetBankInfo))
            {
                Gson gson = new Gson();
                Object classObj = gson.fromJson(response.toString(), BanksModel.class);
                responseHandler.successRequest((BanksModel)classObj,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.GetTrx))
            {
                Gson gson = new Gson();
                Object classObj = gson.fromJson(response.toString(), MessageModel.class);
                responseHandler.successRequest((MessageModel)classObj,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.DeleteMsg))
            {
                responseHandler.successRequest(null,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.SetUpdateMsgAsRead))
            {
                responseHandler.successRequest(null,apiName);
            }

            if (apiName.equalsIgnoreCase(URLClass.GetAccount))
            {
                Gson gson = new Gson();
                Object classObj = gson.fromJson(response.toString(), AccountsModel.class);
                responseHandler.successRequest((AccountsModel)classObj,apiName);
            }

//            if (apiName.equalsIgnoreCase(URLClass.GetCard))
//            {
//                Gson gson = new Gson();
//                Object classObj = gson.fromJson(GetResponseBody.getResponseBody(response).toString(), CardsBody.class);
//                responseHandler.successRequest((CardsBody)classObj,apiName);
//            }

          }
        catch (Exception xx)
    {
        xx.getMessage();
    }
    }

    @Override
    public void failedRequest(String msg, String apiName)
    {
        responseHandler.failedRequest(msg,apiName);
    }


    public interface ResponseHandler{

        void successRequest(Object classObject, String apiName);
        void failedRequest(String msg, String apiName);
    }



}
