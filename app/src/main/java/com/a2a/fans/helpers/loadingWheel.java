package com.a2a.fans.helpers;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.widget.ProgressBar;

import com.a2a.fans.R;


public class loadingWheel {

    public static Dialog spinWheelDialog = null;
    public static void startSpinwheel(Context context, boolean setDefaultLifetime, boolean isCancelable) {
        try {

            if (spinWheelDialog != null && spinWheelDialog.isShowing())
                return;

            spinWheelDialog = new Dialog(context, R.style.wait_spinner_style);
            ProgressBar progressBar = new ProgressBar(context);
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            spinWheelDialog.addContentView(progressBar, layoutParams);
            spinWheelDialog.setCancelable(isCancelable);
            spinWheelDialog.show();

            Handler spinWheelTimer = new Handler();
            spinWheelTimer.removeCallbacks(dismissSpinner);

            if (setDefaultLifetime) // If requested for default dismiss time.
                spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + 1000);

            spinWheelDialog.setCanceledOnTouchOutside(false);
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }
    }
    static Runnable dismissSpinner = new Runnable() {

        @Override
        public void run() {
            stopLoading();
        }

    };

    public static void stopLoading() {
        if (spinWheelDialog != null)
            try
            {
                spinWheelDialog.dismiss();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        spinWheelDialog = null;
    }
}
