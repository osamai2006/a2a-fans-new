package com.a2a.fans.helpers;

import android.app.Activity;
import android.app.Application;

public class AppController extends Application
{

    public static AppController instance;
    private Activity currentActivity;

    public static AppController getInstance() {
        if(instance == null)
        {
            instance = new AppController();
        }

        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        instance = this;


    }

    public Activity getCurrentActivity() {

        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity)
    {
        this.currentActivity = currentActivity;
    }

}
