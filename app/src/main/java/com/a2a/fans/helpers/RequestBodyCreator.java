package com.a2a.fans.helpers;

import android.content.Context;
import android.os.Build;

import com.a2a.fans.BuildConfig;
import com.a2a.fans.utls.appConstants;

import org.json.JSONObject;

public class RequestBodyCreator {

    Context context=AppController.getInstance().getApplicationContext();
    private static RequestBodyCreator instance;
    public static RequestBodyCreator getInstance()
    {
        if(instance == null)
        {
            instance = new RequestBodyCreator();
        }

        return instance;
    }

    public JSONObject createRequestObject(long bankID)
    {
        //String DeviceType= "Android :" + Build.MANUFACTURER + "," + Build.MODEL + "_Release_" + Build.VERSION.RELEASE;

        //JsonObject EnecryptedRequest=new JsonObject();
        //JSONObject finalRequest = new JSONObject();
        JSONObject dataObject = new JSONObject();
        JSONObject footerObject = new JSONObject();

        try
        {
            if(appConstants.mobileNumber.equalsIgnoreCase(""))
            {
                appConstants.mobileNumber=sharedPrefs.getInstance().getStringPreference(context, appConstants.mobileNum_KEY);
            }

            if(appConstants.countryCode.equalsIgnoreCase(""))
            {
                appConstants.countryCode=sharedPrefs.getInstance().getStringPreference(context, appConstants.countryCode_KEY);
            }




            dataObject.putOpt("RegionCode", appConstants.countryCode);
            dataObject.putOpt("MobileNo",appConstants.mobileNumber);
            dataObject.putOpt("MobileType","Android");
            dataObject.putOpt("MobileOS",Build.VERSION.RELEASE);
            dataObject.putOpt("FANSVer", BuildConfig.VERSION_NAME);
            dataObject.putOpt("Channel","FN");
            dataObject.putOpt("GuidID",appConstants.uuid);
            dataObject.putOpt("BankID",bankID);

            footerObject.putOpt("signature","null");
            dataObject.putOpt("footer",footerObject);

        }
        catch (Exception xx)
        {
            xx.toString();
        }
        return dataObject;
    }


    public JSONObject createRequestObject(String bankID, JSONObject bodyObj)
    {
        //String DeviceType= "Android :" + Build.MANUFACTURER + "," + Build.MODEL + "_Release_" + Build.VERSION.RELEASE;

        //JsonObject EnecryptedRequest=new JsonObject();
        JSONObject finalRequest=new JSONObject();
        JSONObject A2ARequest=new JSONObject();
        JSONObject headerObject=new JSONObject();
        JSONObject footerObject=new JSONObject();

        try
        {

            if(appConstants.mobileNumber.equalsIgnoreCase(""))
            {
                appConstants.mobileNumber=sharedPrefs.getInstance().getStringPreference(context, appConstants.mobileNum_KEY);
            }

            if(appConstants.countryCode.equalsIgnoreCase(""))
            {
                appConstants.countryCode=sharedPrefs.getInstance().getStringPreference(context, appConstants.countryCode_KEY);
            }

//            if(!appConstants.mobileNumber.equalsIgnoreCase(""))
//            {
//                if (!appConstants.mobileNumber.startsWith("9"))
//                {
//                    appConstants.mobileNumber = appConstants.countryCode + appConstants.mobileNumber.replace("+", "");
//                }
//            }

            headerObject.putOpt("RegionCode", appConstants.countryCode);
            headerObject.putOpt("MobileNO",appConstants.mobileNumber);
            //headerObject.putOpt("MethodName",serviceName);
            headerObject.putOpt("MobileType","Android");
            headerObject.putOpt("MobileOS",Build.VERSION.RELEASE);
            headerObject.putOpt("FANSVer", BuildConfig.VERSION_NAME);
            headerObject.putOpt("SrvID","");
            headerObject.putOpt("UserID","ibank");
            headerObject.putOpt("Password","4RldD6Eg7Dpetm+Kirflg==");
            headerObject.putOpt("Channel","FN");
            headerObject.putOpt("GuidID",appConstants.uuid);
            //headerObject.putOpt("DeviceName",Build.MANUFACTURER+"_"+Build.MODEL );
            headerObject.putOpt("BankID",bankID);

            footerObject.putOpt("signature","null");

            A2ARequest.putOpt("Header",headerObject);
            A2ARequest.putOpt("body",bodyObj);
            A2ARequest.putOpt("footer",footerObject);

            finalRequest.putOpt("A2ARequest",A2ARequest);

//            EnecryptedRequest.addProperty("METHOD",serviceName);
//            EnecryptedRequest.add("DATA",finalRequest);


        }
        catch (Exception xx)
        {
            xx.getMessage();
        }
        return finalRequest;

    }
}
