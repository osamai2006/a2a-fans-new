package com.a2a.fans.networking;

public class URLClass {

    //private final String BASE_URL = AppController.getInstance().getString(R.string.baseURL);

    public static String GetCerPublicKey="GetCerPublicKey";
    public static String GetVersion="GetVersion";
    public static String Registration="Registration";
    public static String Authentication="Authentication";
    public static String IsOnline="IsOnline";
    public static String DeleteMsg="DeleteMsg";
    public static String GetBankInfo="GetBankInfo";
    public static String GetTrx="GetTrx";
    public static String Resend="Resend";
    public static String SetMsgStatus="SetMsgStatus";
    public static String SetUpdateMsgAsRead="SetUpdateMsgAsRead";
    public static String RefreshToken="RefreshToken";
    public static String GetAccount="GetAccount";
    public static String GetCard="GetCard";



}
