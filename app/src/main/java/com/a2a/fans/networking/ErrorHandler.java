package com.a2a.fans.networking;

import android.widget.Toast;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.utls.appConstants;

import org.json.JSONObject;


public class ErrorHandler {


    public static ErrorHandler errorHandling;

    public static ErrorHandler getInstance()
    {
        if (errorHandling == null)
            return errorHandling = new ErrorHandler();
        return errorHandling;
    }



    public boolean checkError(JSONObject response)
    {
        boolean result=false;

        try
        {
            //JSONObject resultObj = response.getJSONObject("ErrorMsg");
            //String resultCode=response.getString("ErrorMsg");

            String errorCode = response.getString("ErrorCode");

            if(!errorCode.equalsIgnoreCase("0"))
            {
                String errorDesc = "";

                if (appConstants.current_language.equalsIgnoreCase("en")) {
                    errorDesc = response.getString("EDesc");

                } else {

                    errorDesc = response.getString("ADesc");

                }
                Toast.makeText(AppController.getInstance().getApplicationContext(), errorDesc, Toast.LENGTH_LONG).show();

                return false;
            }
        }
        catch (Exception xx)
        {
            Toast.makeText(AppController.getInstance().getApplicationContext(), xx.toString(), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

//    public boolean checkError(JSONObject response)
//    {
//        boolean result=false;
//
//        try
//        {
//            JSONObject resultObj = response.getJSONObject("ErrorMsg");
//
//            String errorCode = resultObj.getString("ErrorCode");
//
//            if(!errorCode.equalsIgnoreCase("0"))
//            {
//                String errorDesc = "";
//
//                if (appConstants.current_language.equalsIgnoreCase("en")) {
//                    errorDesc = resultObj.getString("EDesc");
//
//                } else {
//
//                    errorDesc = resultObj.getString("ADesc");
//
//                }
//                Toast.makeText(AppController.getInstance().getApplicationContext(), errorDesc, Toast.LENGTH_LONG).show();
//
//                return false;
//            }
//        }
//        catch (Exception xx)
//        {
//            Toast.makeText(AppController.getInstance().getApplicationContext(), xx.toString(), Toast.LENGTH_LONG).show();
//            return false;
//        }
//
//        return true;
//    }

}
