package com.a2a.fans.networking;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.widget.Toast;
import com.a2a.fans.R;
import com.a2a.fans.SecurityHelper.EncryptionDecryptionHelper;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.RequestBodyCreator;
import com.a2a.fans.helpers.loadingWheel;
import com.a2a.fans.utls.appConstants;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class RequestHandler
{

    private final String BASE_URL = AppController.getInstance().getString(R.string.baseURL);
    int requestTimeOut=50000;
    private   static  ResponseHandler responseHandler;


    private static RequestHandler instance;
    public static RequestHandler getInstance(ResponseHandler _requestHandler)
    {
        responseHandler=_requestHandler;
        if (instance == null)
        {
            return instance = new RequestHandler();
        }

        return instance;
    }


    public LiveData<JSONObject> requestAPI(final JSONObject requestBody, final String endPointName,boolean encryptionRequired)
    {
        MutableLiveData<JSONObject> requestResult=new MutableLiveData<>();
        JSONObject EnecryptedRequest = new JSONObject();
        Context context= AppController.getInstance().getApplicationContext();

        if(encryptionRequired)
        {
            try
            {
                //to do the AES Encryption
                String encryMethod = EncryptionDecryptionHelper.getInstance().encryptData(endPointName, appConstants.authKey);
                String encryData = EncryptionDecryptionHelper.getInstance().encryptData(requestBody.toString(), appConstants.authKey);

                EnecryptedRequest.putOpt("METHOD", encryMethod);
                EnecryptedRequest.putOpt("DATA", encryData);
            }
            catch (Exception xx)
            {
                xx.getMessage();
            }

        }
        else
        {     try
                {
                    EnecryptedRequest.putOpt("Method", endPointName);
                    EnecryptedRequest.putOpt("Data", requestBody);
                }
                catch (Exception xx)
                {
                    xx.getMessage();
                }
        }

        new Thread(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    JsonObjectRequest jsonObjReq;
                    jsonObjReq = new JsonObjectRequest(Request.Method.POST, BASE_URL, EnecryptedRequest, new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                loadingWheel.stopLoading();
                                JSONObject  responseDecrypted=new JSONObject();
                                //JSONArray responseDecrypted1=new JSONArray();

                                if(response !=null)
                                {
                                    if(encryptionRequired)
                                    {
                                            responseDecrypted = new JSONObject(EncryptionDecryptionHelper.getInstance().
                                                    decryptData(response.getString("DATA"), appConstants.authKey));

                                    }


                                    if(endPointName.equalsIgnoreCase(URLClass.GetCerPublicKey))
                                    {
                                        responseHandler.successRequest(response, endPointName);
                                        requestResult.setValue(response);

                                    }
                                    else
                                    {
                                        if (ErrorHandler.getInstance().checkError(responseDecrypted))
                                        {
                                            responseHandler.successRequest(responseDecrypted, endPointName);
                                            requestResult.setValue(responseDecrypted);
                                        }
//                                        else
//                                        {
//                                            requestResult.setValue(null);
//                                            responseHandler.failedRequest("",endPointName);
//                                        }
                                    }
                                }

                            }
                            catch (Exception error)
                            {
                                requestResult.setValue(null);
                                Toast.makeText(context,endPointName+" "+ error.toString(), Toast.LENGTH_SHORT).show();
                                loadingWheel.stopLoading();
                                responseHandler.failedRequest("Server Error",endPointName);
                            }
                        }
                    }, new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            loadingWheel.stopLoading();
                            requestResult.setValue(null);
                            appConstants.offlineMode=true;
                            Toast.makeText(context,endPointName+" "+ error.toString(), Toast.LENGTH_SHORT).show();
                            responseHandler.failedRequest("Server Error",endPointName);

                        }
                    })

                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headersSys = super.getHeaders();
                            Map<String, String> headers = new HashMap<String, String>();
                            headersSys.remove("Authorization");
                            headers.put("Authorization", appConstants.authKey);
                            headers.put("User-Agent", "Android");
                            headers.putAll(headersSys);
                            return headers;
                        }


                        @Override
                        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                                try {

                                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                                    JSONObject jsonResponse = new JSONObject(jsonString);
                                    if(endPointName.equalsIgnoreCase(URLClass.GetCerPublicKey )||
                                            endPointName.equalsIgnoreCase(URLClass.Registration )||
                                            endPointName.equalsIgnoreCase(URLClass.IsOnline))
                                    {
                                        if(response.headers.get("Authorization")!=null)
                                        {
                                            appConstants.authKey = response.headers.get("Authorization");
                                        }
                                    }
                                    jsonResponse.put("headers", new JSONObject(response.headers));
                                    return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));

                                } catch (UnsupportedEncodingException e) {
                                    return Response.error(new ParseError(e));
                                } catch (JSONException je) {
                                    return Response.error(new ParseError(je));
                                }


                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                            requestTimeOut,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjReq);

                }
                catch (Exception ex)
                {
                    loadingWheel.stopLoading();
                }

            }
        }).start();

         return requestResult;
    }



    public interface ResponseHandler{

       void successRequest(JSONObject response, String apiName);
       void failedRequest(String msg, String apiName);
    }

}
