
package com.a2a.fans.models;

public class BaseResponseModel {


    /**
     * METHOD : 7L4vdUQR/1wYszZJNNMHhQ==
     * DATA : U4J6ntT/aJjhBk8fwUA68g/aySIskzyrB7euB99SrEr8TN3UJLJOhM9IdxIM45vWPE/a/aelGiBBFu6ux4nXir/VdTpvQ658ec4gYhxKgFXlzZC1O0YZHLjBnfGh1CN6JTQ9sXLEX7QP4wYcZW/g2Ln08WbeSz/oRlR3CnTi6XnTp9Rv1bYklg6IIg7BF1WivO51Ya6UBd64+D5r5uSfwlrB2cGARGjaYBbAmXl9E5jpK8wG8/QpEy2ZbZmClL/OswzOK/V7aWAET6ppa6nY/0ifyDBuIjgLsIBtJb33wDobcEDazAp3XWsDVAaHIF1nihECjn0/1y6Unya8BJQi2mvLrXASOEU0lhSOIVzu0tRvjtYeVRlOnpMK50vs3WoL6KCG2OSVJ9uFzwqHR6/Do0eIGLC9chyfjZs2GEg1GXBFJet81xOJamOGjExixQTW6gvAMwbc7aX/yd0KFoMwRLjlW1w82vBYPfknovo8ARbPt0AeMhSHOPvwtKttvQFR6A+J647FPW6b6QIwun4sNouFGgjnkZgo7xYgovP5nz8YQMuSZwEe74rEG/Ozh5CQadBBsZXNP1bgJV/1Q/pPPEPWj2Yh9KFkTW++QUvOMeR9CpHLxqcxDQ6OW3e6KqNFekos3rDuArNusd31GOLH2t6Lus5nUZJXPW3dxtiWNGE=
     */

    private String METHOD;
    private String DATA;

    public String getMETHOD() {
        return METHOD;
    }

    public void setMETHOD(String METHOD) {
        this.METHOD = METHOD;
    }

    public String getDATA() {
        return DATA;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }
}
