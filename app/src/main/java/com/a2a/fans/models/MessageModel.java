
package com.a2a.fans.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class MessageModel {

    @SerializedName("Alerts")
    private List<AlertModel> mAlertModels;
    @SerializedName("TrxFlag")
    private Boolean mTrxFlag;

    public List<AlertModel> getAlerts() {
        return mAlertModels;
    }

    public void setAlerts(List<AlertModel> alertModels) {
        mAlertModels = alertModels;
    }

    public Boolean getTrxFlag() {
        return mTrxFlag;
    }

    public void setTrxFlag(Boolean trxFlag) {
        mTrxFlag = trxFlag;
    }

}
