
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;


public class IsOnlineListModel {

    @SerializedName("BankFlag")
    private Boolean mBankFlag;
    @SerializedName("BankID")
    private Long mBankID;
    @SerializedName("TrxFlag")
    private Boolean mTrxFlag;

    public Boolean getBankFlag() {
        return mBankFlag;
    }

    public void setBankFlag(Boolean bankFlag) {
        mBankFlag = bankFlag;
    }

    public Long getBankID() {
        return mBankID;
    }

    public void setBankID(Long bankID) {
        mBankID = bankID;
    }

    public Boolean getTrxFlag() {
        return mTrxFlag;
    }

    public void setTrxFlag(Boolean trxFlag) {
        mTrxFlag = trxFlag;
    }

}
