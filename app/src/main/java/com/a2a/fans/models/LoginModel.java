package com.a2a.fans.models;

public class LoginModel {



    private String message;
    private String otherMessage;
    private String status;

    public LoginModel(String message, String otherMessage, String status) {
        this.message = message;
        this.otherMessage = otherMessage;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOtherMessage() {
        return otherMessage;
    }

    public void setOtherMessage(String otherMessage) {
        this.otherMessage = otherMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
