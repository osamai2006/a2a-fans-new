
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;


public class CardsModel {

    @SerializedName("RegistrationModel")
    private CardsBody mBody;

    public CardsBody getCardsBody() {
        return mBody;
    }

    public void setCardsBody(CardsBody cardsBody) {
        mBody = cardsBody;
    }

}
