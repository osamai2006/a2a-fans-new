package com.a2a.fans.models;

import android.arch.lifecycle.MutableLiveData;

public class CheckOnlineModel extends MutableLiveData<CheckOnlineModel> {


    /**
     * A2AResponse : {"Header":{"RegionCode":"","SrvID":0,"GuidID":"00000000-0000-0000-0000-000000000000","TimeStamp":"2019-03-24T13:38:45.3229857+02:00","Result":{"ErrorCode":0,"EDesc":"","ADesc":""}},"IsOnlineBody":{"ModifedFlag":false,"BankFlag":false,"TrxFlag":false},"Footer":{"Signature":""}}
     * ErrorMsg : {"ErrorCode":0,"EDesc":"","ADesc":""}
     */

    private A2AResponseBean A2AResponse;
    private ErrorMsgBean ErrorMsg;

    public A2AResponseBean getA2AResponse() {
        return A2AResponse;
    }

    public void setA2AResponse(A2AResponseBean A2AResponse) {
        this.A2AResponse = A2AResponse;
    }

    public ErrorMsgBean getErrorMsg() {
        return ErrorMsg;
    }

    public void setErrorMsg(ErrorMsgBean ErrorMsg) {
        this.ErrorMsg = ErrorMsg;
    }

    public static class A2AResponseBean {
        /**
         * Header : {"RegionCode":"","SrvID":0,"GuidID":"00000000-0000-0000-0000-000000000000","TimeStamp":"2019-03-24T13:38:45.3229857+02:00","Result":{"ErrorCode":0,"EDesc":"","ADesc":""}}
         * IsOnlineBody : {"ModifedFlag":false,"BankFlag":false,"TrxFlag":false}
         * Footer : {"Signature":""}
         */

        private HeaderBean Header;
        private BodyBean Body;
        private FooterBean Footer;

        public HeaderBean getHeader() {
            return Header;
        }

        public void setHeader(HeaderBean Header) {
            this.Header = Header;
        }

        public BodyBean getBody() {
            return Body;
        }

        public void setBody(BodyBean Body) {
            this.Body = Body;
        }

        public FooterBean getFooter() {
            return Footer;
        }

        public void setFooter(FooterBean Footer) {
            this.Footer = Footer;
        }

        public static class HeaderBean {
            /**
             * RegionCode :
             * SrvID : 0
             * GuidID : 00000000-0000-0000-0000-000000000000
             * TimeStamp : 2019-03-24T13:38:45.3229857+02:00
             * Result : {"ErrorCode":0,"EDesc":"","ADesc":""}
             */

            private String RegionCode;
            private int SrvID;
            private String GuidID;
            private String TimeStamp;
            private ResultBean Result;

            public String getRegionCode() {
                return RegionCode;
            }

            public void setRegionCode(String RegionCode) {
                this.RegionCode = RegionCode;
            }

            public int getSrvID() {
                return SrvID;
            }

            public void setSrvID(int SrvID) {
                this.SrvID = SrvID;
            }

            public String getGuidID() {
                return GuidID;
            }

            public void setGuidID(String GuidID) {
                this.GuidID = GuidID;
            }

            public String getTimeStamp() {
                return TimeStamp;
            }

            public void setTimeStamp(String TimeStamp) {
                this.TimeStamp = TimeStamp;
            }

            public ResultBean getResult() {
                return Result;
            }

            public void setResult(ResultBean Result) {
                this.Result = Result;
            }

            public static class ResultBean {
                /**
                 * ErrorCode : 0
                 * EDesc :
                 * ADesc :
                 */

                private int ErrorCode;
                private String EDesc;
                private String ADesc;

                public int getErrorCode() {
                    return ErrorCode;
                }

                public void setErrorCode(int ErrorCode) {
                    this.ErrorCode = ErrorCode;
                }

                public String getEDesc() {
                    return EDesc;
                }

                public void setEDesc(String EDesc) {
                    this.EDesc = EDesc;
                }

                public String getADesc() {
                    return ADesc;
                }

                public void setADesc(String ADesc) {
                    this.ADesc = ADesc;
                }
            }
        }

        public static class BodyBean {
            /**
             * ModifedFlag : false
             * BankFlag : false
             * TrxFlag : false
             */

            private boolean ModifedFlag;
            private boolean BankFlag;
            private boolean TrxFlag;

            public boolean isModifedFlag() {
                return ModifedFlag;
            }

            public void setModifedFlag(boolean ModifedFlag) {
                this.ModifedFlag = ModifedFlag;
            }

            public boolean isBankFlag() {
                return BankFlag;
            }

            public void setBankFlag(boolean BankFlag) {
                this.BankFlag = BankFlag;
            }

            public boolean isTrxFlag() {
                return TrxFlag;
            }

            public void setTrxFlag(boolean TrxFlag) {
                this.TrxFlag = TrxFlag;
            }
        }

        public static class FooterBean {
            /**
             * Signature :
             */

            private String Signature;

            public String getSignature() {
                return Signature;
            }

            public void setSignature(String Signature) {
                this.Signature = Signature;
            }
        }
    }

    public static class ErrorMsgBean {
        /**
         * ErrorCode : 0
         * EDesc :
         * ADesc :
         */

        private int ErrorCode;
        private String EDesc;
        private String ADesc;

        public int getErrorCode() {
            return ErrorCode;
        }

        public void setErrorCode(int ErrorCode) {
            this.ErrorCode = ErrorCode;
        }

        public String getEDesc() {
            return EDesc;
        }

        public void setEDesc(String EDesc) {
            this.EDesc = EDesc;
        }

        public String getADesc() {
            return ADesc;
        }

        public void setADesc(String ADesc) {
            this.ADesc = ADesc;
        }
    }
}
