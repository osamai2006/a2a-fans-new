
package com.a2a.fans.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class AccountsModel {

    @SerializedName("Accounts")
    private List<String> mAccounts;

    public AccountsModel(List<String> mAccounts) {
        this.mAccounts = mAccounts;
    }

    public List<String> getAccounts() {
        return mAccounts;
    }

    public void setAccounts(List<String> accounts) {
        mAccounts = accounts;
    }

}
