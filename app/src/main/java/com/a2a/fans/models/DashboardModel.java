package com.a2a.fans.models;

public class DashboardModel {
    private String id;
    private String name;
    private String image;
    private String cardBalance;
    private String availableBalance;

    public DashboardModel(String id, String name, String image, String cardBalance, String availableBalance) {
        this.setId(id);
        this.setName(name);
        this.setImage(image);
        this.setCardBalance(cardBalance);
        this.setAvailableBalance(availableBalance);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCardBalance() {
        return cardBalance;
    }

    public void setCardBalance(String cardBalance) {
        this.cardBalance = cardBalance;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }
}
