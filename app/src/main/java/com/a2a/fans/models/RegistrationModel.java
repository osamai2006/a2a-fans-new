
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;

public class RegistrationModel {

    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("OTP")
    private OTPModel mOTPModel;
    @SerializedName("OTPExpiryTime")
    private String mOTPExpiryTime;

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public OTPModel getOTP() {
        return mOTPModel;
    }

    public void setOTP(OTPModel oTP) {
        mOTPModel = oTP;
    }

    public String getOTPExpiryTime() {
        return mOTPExpiryTime;
    }

    public void setOTPExpiryTime(String oTPExpiryTime) {
        mOTPExpiryTime = oTPExpiryTime;
    }

}
