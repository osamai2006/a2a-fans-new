
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;


public class BankListModel {

    @SerializedName("ADesc")
    private String mADesc;
    @SerializedName("AlertTimeOut")
    private Long mAlertTimeOut;
    @SerializedName("BankCode")
    private String mBankCode;
    @SerializedName("EDesc")
    private String mEDesc;
    @SerializedName("Enabled")
    private Boolean mEnabled;
    @SerializedName("ID")
    private Long mID;
    @SerializedName("Icon")
    private String mIcon;
    @SerializedName("ModifiedDate")
    private String mModifiedDate;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("PromotionCode")
    private Object mPromotionCode;
    @SerializedName("RegionCode")
    private String mRegionCode;
    @SerializedName("Thumbnail")
    private String mThumbnail;
    @SerializedName("URL")
    private String mURL;
    @SerializedName("UserID")
    private String mUserID;

    private String isPinned;
    private String newMsgsCount;

    public BankListModel(String mADesc, String mBankCode, String mEDesc, Boolean mEnabled, Long mID, String mIcon, String mModifiedDate, String mPassword, Object mPromotionCode, String mRegionCode, String mThumbnail, String mURL, String mUserID, String isPinned, String newMsgsCount) {
        this.mADesc = mADesc;
        this.mBankCode = mBankCode;
        this.mEDesc = mEDesc;
        this.mEnabled = mEnabled;
        this.mID = mID;
        this.mIcon = mIcon;
        this.mModifiedDate = mModifiedDate;
        this.mPassword = mPassword;
        this.mPromotionCode = mPromotionCode;
        this.mRegionCode = mRegionCode;
        this.mThumbnail = mThumbnail;
        this.mURL = mURL;
        this.mUserID = mUserID;
        this.isPinned = isPinned;
        this.newMsgsCount = newMsgsCount;
    }

    public String getADesc() {
        return mADesc;
    }

    public void setADesc(String aDesc) {
        mADesc = aDesc;
    }

    public Long getAlertTimeOut() {
        return mAlertTimeOut;
    }

    public void setAlertTimeOut(Long alertTimeOut) {
        mAlertTimeOut = alertTimeOut;
    }

    public String getBankCode() {
        return mBankCode;
    }

    public void setBankCode(String bankCode) {
        mBankCode = bankCode;
    }

    public String getEDesc() {
        return mEDesc;
    }

    public void setEDesc(String eDesc) {
        mEDesc = eDesc;
    }

    public Boolean getEnabled() {
        return mEnabled;
    }

    public void setEnabled(Boolean enabled) {
        mEnabled = enabled;
    }

    public Long getID() {
        return mID;
    }

    public void setID(Long iD) {
        mID = iD;
    }

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String icon) {
        mIcon = icon;
    }

    public String getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        mModifiedDate = modifiedDate;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public Object getPromotionCode() {
        return mPromotionCode;
    }

    public void setPromotionCode(Object promotionCode) {
        mPromotionCode = promotionCode;
    }

    public String getRegionCode() {
        return mRegionCode;
    }

    public void setRegionCode(String regionCode) {
        mRegionCode = regionCode;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String uRL) {
        mURL = uRL;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public String getIsPinned() {
        return isPinned;
    }

    public void setIsPinned(String isPinned) {
        this.isPinned = isPinned;
    }

    public String getNewMsgsCount() {
        return newMsgsCount;
    }

    public void setNewMsgsCount(String newMsgsCount) {
        this.newMsgsCount = newMsgsCount;
    }
}
