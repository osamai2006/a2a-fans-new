
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;


public class AlertModel {

    @SerializedName("AccNo")
    private String mAccNo;
    @SerializedName("AlertID")
    private Long mAlertID;
    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("Balance")
    private String mBalance;
    @SerializedName("BankID")
    private Long mBankID;
    @SerializedName("CardNo")
    private String mCardNo;
    @SerializedName("DeleteDateTime")
    private String mDeleteDateTime;
    @SerializedName("DeleteFlag")
    private Boolean mDeleteFlag;
    @SerializedName("Flag")
    private Boolean mFlag;
    @SerializedName("FlagDateTime")
    private String mFlagDateTime;
    @SerializedName("ID")
    private Long mID;
    @SerializedName("MagCat")
    private Long mMagCat;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("MsgBankID")
    private Long mMsgBankID;
    @SerializedName("MsgDateTime")
    private String mMsgDateTime;
    @SerializedName("MsgText")
    private String mMsgText;
    @SerializedName("MsgType")
    private Long mMsgType;
    @SerializedName("ReadDateTime")
    private String mReadDateTime;
    @SerializedName("ReadFlag")
    private Boolean mReadFlag;
    @SerializedName("ReportDateTime")
    private String mReportDateTime;
    @SerializedName("ReportFlag")
    private Boolean mReportFlag;
    @SerializedName("ReportReason")
    private Long mReportReason;
    @SerializedName("SendDateTime")
    private String mSendDateTime;
    @SerializedName("SendSMSDateTime")
    private String mSendSMSDateTime;
    @SerializedName("SendSMSFlag")
    private Boolean mSendSMSFlag;
    @SerializedName("SendTRXDateTime")
    private String mSendTRXDateTime;
    @SerializedName("SendTRXFlag")
    private Boolean mSendTRXFlag;
    @SerializedName("SrvID")
    private String mSrvID;

    public String getAccNo() {
        return mAccNo;
    }

    public void setAccNo(String accNo) {
        mAccNo = accNo;
    }

    public Long getAlertID() {
        return mAlertID;
    }

    public void setAlertID(Long alertID) {
        mAlertID = alertID;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getBalance() {
        return mBalance;
    }

    public void setBalance(String balance) {
        mBalance = balance;
    }

    public Long getBankID() {
        return mBankID;
    }

    public void setBankID(Long bankID) {
        mBankID = bankID;
    }

    public String getCardNo() {
        return mCardNo;
    }

    public void setCardNo(String cardNo) {
        mCardNo = cardNo;
    }

    public String getDeleteDateTime() {
        return mDeleteDateTime;
    }

    public void setDeleteDateTime(String deleteDateTime) {
        mDeleteDateTime = deleteDateTime;
    }

    public Boolean getDeleteFlag() {
        return mDeleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        mDeleteFlag = deleteFlag;
    }

    public Boolean getFlag() {
        return mFlag;
    }

    public void setFlag(Boolean flag) {
        mFlag = flag;
    }

    public String getFlagDateTime() {
        return mFlagDateTime;
    }

    public void setFlagDateTime(String flagDateTime) {
        mFlagDateTime = flagDateTime;
    }

    public Long getID() {
        return mID;
    }

    public void setID(Long iD) {
        mID = iD;
    }

    public Long getMagCat() {
        return mMagCat;
    }

    public void setMagCat(Long magCat) {
        mMagCat = magCat;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public Long getMsgBankID() {
        return mMsgBankID;
    }

    public void setMsgBankID(Long msgBankID) {
        mMsgBankID = msgBankID;
    }

    public String getMsgDateTime() {
        return mMsgDateTime;
    }

    public void setMsgDateTime(String msgDateTime) {
        mMsgDateTime = msgDateTime;
    }

    public String getMsgText() {
        return mMsgText;
    }

    public void setMsgText(String msgText) {
        mMsgText = msgText;
    }

    public Long getMsgType() {
        return mMsgType;
    }

    public void setMsgType(Long msgType) {
        mMsgType = msgType;
    }

    public String getReadDateTime() {
        return mReadDateTime;
    }

    public void setReadDateTime(String readDateTime) {
        mReadDateTime = readDateTime;
    }

    public Boolean getReadFlag() {
        return mReadFlag;
    }

    public void setReadFlag(Boolean readFlag) {
        mReadFlag = readFlag;
    }

    public String getReportDateTime() {
        return mReportDateTime;
    }

    public void setReportDateTime(String reportDateTime) {
        mReportDateTime = reportDateTime;
    }

    public Boolean getReportFlag() {
        return mReportFlag;
    }

    public void setReportFlag(Boolean reportFlag) {
        mReportFlag = reportFlag;
    }

    public Long getReportReason() {
        return mReportReason;
    }

    public void setReportReason(Long reportReason) {
        mReportReason = reportReason;
    }

    public String getSendDateTime() {
        return mSendDateTime;
    }

    public void setSendDateTime(String sendDateTime) {
        mSendDateTime = sendDateTime;
    }

    public String getSendSMSDateTime() {
        return mSendSMSDateTime;
    }

    public void setSendSMSDateTime(String sendSMSDateTime) {
        mSendSMSDateTime = sendSMSDateTime;
    }

    public Boolean getSendSMSFlag() {
        return mSendSMSFlag;
    }

    public void setSendSMSFlag(Boolean sendSMSFlag) {
        mSendSMSFlag = sendSMSFlag;
    }

    public String getSendTRXDateTime() {
        return mSendTRXDateTime;
    }

    public void setSendTRXDateTime(String sendTRXDateTime) {
        mSendTRXDateTime = sendTRXDateTime;
    }

    public Boolean getSendTRXFlag() {
        return mSendTRXFlag;
    }

    public void setSendTRXFlag(Boolean sendTRXFlag) {
        mSendTRXFlag = sendTRXFlag;
    }

    public String getSrvID() {
        return mSrvID;
    }

    public void setSrvID(String srvID) {
        mSrvID = srvID;
    }

}
