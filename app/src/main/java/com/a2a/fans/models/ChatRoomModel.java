package com.a2a.fans.models;

public class ChatRoomModel {
    private String msgId;
    private String msgBody;
    private String msgDate;
    private String readFlag;
    private String sender;
    private String messageCat;
    private String messageType;
    private String base64;
    private String locationXY;
    private String checked;
    private String trxAmount;
    private String isFlagged;
    private String isReported;
    private String reportReason;
    private String actionName;
    private String deleteFlag;

    public ChatRoomModel(String msgId, String msgBody, String msgDate, String readFlag, String sender, String messageCat, String messageType, String base64, String locationXY, String checked, String trxAmount, String isFlagged, String isReported, String reportReason, String actionName, String deleteFlag) {
        this.setMsgId(msgId);
        this.setMsgBody(msgBody);
        this.setMsgDate(msgDate);
        this.setReadFlag(readFlag);
        this.setSender(sender);
        this.setMessageCat(messageCat);
        this.setMessageType(messageType);
        this.setBase64(base64);
        this.setLocationXY(locationXY);
        this.setChecked(checked);
        this.setTrxAmount(trxAmount);
        this.setIsFlagged(isFlagged);
        this.setIsReported(isReported);
        this.setReportReason(reportReason);
        this.setActionName(actionName);
        this.setDeleteFlag(deleteFlag);
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public String getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(String readFlag) {
        this.readFlag = readFlag;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessageCat() {
        return messageCat;
    }

    public void setMessageCat(String messageCat) {
        this.messageCat = messageCat;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getLocationXY() {
        return locationXY;
    }

    public void setLocationXY(String locationXY) {
        this.locationXY = locationXY;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getTrxAmount() {
        return trxAmount;
    }

    public void setTrxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
    }

    public String getIsFlagged() {
        return isFlagged;
    }

    public void setIsFlagged(String isFlagged) {
        this.isFlagged = isFlagged;
    }

    public String getIsReported() {
        return isReported;
    }

    public void setIsReported(String isReported) {
        this.isReported = isReported;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
