
package com.a2a.fans.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CardsBody {

    @SerializedName("Cards")
    private List<String> mCards;

    public List<String> getCards() {
        return mCards;
    }

    public void setCards(List<String> cards) {
        mCards = cards;
    }

}
