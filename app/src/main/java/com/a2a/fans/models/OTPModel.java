
package com.a2a.fans.models;

import com.google.gson.annotations.SerializedName;


public class OTPModel {

    @SerializedName("Desc")
    private String mDesc;
    @SerializedName("OTP")
    private String mOTP;
    @SerializedName("ResCode")
    private Long mResCode;
    @SerializedName("UTR")
    private String mUTR;

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getOTP() {
        return mOTP;
    }

    public void setOTP(String oTP) {
        mOTP = oTP;
    }

    public Long getResCode() {
        return mResCode;
    }

    public void setResCode(Long resCode) {
        mResCode = resCode;
    }

    public String getUTR() {
        return mUTR;
    }

    public void setUTR(String uTR) {
        mUTR = uTR;
    }

}
