package com.a2a.fans.models;

public class CustomChartModel {
    private String bankID;
    private String bankName;
    private String accBalance;
    private String cardBalance;
    private String type;

    public CustomChartModel(String bankID, String bankName, String accBalance, String cardBalance, String type) {
        this.bankID = bankID;
        this.bankName = bankName;
        this.accBalance = accBalance;
        this.cardBalance = cardBalance;
        this.type = type;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccBalance() {
        return accBalance;
    }

    public void setAccBalance(String accBalance) {
        this.accBalance = accBalance;
    }

    public String getCardBalance() {
        return cardBalance;
    }

    public void setCardBalance(String cardBalance) {
        this.cardBalance = cardBalance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
