package com.a2a.fans.FirbaseServices;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;


import com.a2a.fans.helpers.AppController;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.views.activities.ChattingActivity;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.R;
import com.a2a.fans.views.activities.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService implements ProjectRepository.ResponseHandler {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    Context context= AppController.getInstance().getApplicationContext();
    SQLHelper dbhelper;


    long bankID;
    String bankName,bankStatus,sender, body, title,msgCat="",bankIsPinned="false"
            ,msgType="",accountBalance="",cardtBalance="",trxAmount="",hasFlag="",
            accNumber="",cardNumber="",isReported="false",reportReason="",readFlag="false",deleteFlag="",
            base64Content="",msgID="",msgDateTime="",actionName="",bankPromoCode="",bankModifiedDate="",syncdFlag="0";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

            try {
                msgID="";bankID=0;msgCat="";msgType="";hasFlag="";
                bankName="";bankModifiedDate="";base64Content="";
                isReported="false";accountBalance="";accNumber="";accountBalance="";
                cardNumber="";cardtBalance="";readFlag="false";syncdFlag="0";

                JSONObject obj=new JSONObject(remoteMessage.getData());

                msgID=obj.getString("MsgID");
                body = obj.getString("message");
                title = obj.getString("EbankName");
                bankID = obj.getLong("bankID");
                bankIsPinned="";//obj.getString("bankID");
                bankName =obj.getString("EbankName");
                bankStatus ="1";
                sender = "bank";
                msgCat =  obj.getString("MagCat");
                msgType=  obj.getString("MsgType");
                cardtBalance=obj.getString("cardBalance");
                accountBalance=obj.getString("accountBalance");
                trxAmount=  obj.getString("trxAmount");
                hasFlag=obj.getString("Flag");
                accNumber=obj.getString("accNumber");
                cardNumber="";//obj.getString("accNumber");
                base64Content=obj.getString("base64Content");
                msgDateTime=obj.getString("MsgNotifDateTime");
                readFlag=obj.getString("ReadFlag");
                bankPromoCode=obj.getString("PromotionCode");
                bankModifiedDate="";//obj.getString("ModifiedDate");



                PendingIntent msgPendingIntent;
                Intent msgsIntent = new Intent(context, SplashActivity.class);
                msgsIntent.putExtra("bankID", bankID);
                msgsIntent.putExtra("msgID",msgID );
                msgsIntent.putExtra("bankName",bankName );
                msgsIntent.setAction("OPEN_ACTION");
                msgPendingIntent = PendingIntent.getActivity(context, 0, msgsIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                PendingIntent deletePendingIntent;
                Intent deleteIntent = new Intent(context, NotificationReceiver.class);
                deleteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                deleteIntent.putExtra("bankID", bankID);
                deleteIntent.putExtra("bankName", bankName);
                deleteIntent.putExtra("msgID",msgID );
                deleteIntent.putExtra("bankID",bankID );
                deleteIntent.setAction("DELETE_ACTION");
                deletePendingIntent = PendingIntent.getBroadcast(context, 0, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                PendingIntent copyPendingIntent;
                Intent copyIntent = new Intent(context, NotificationReceiver.class);
                copyIntent.putExtra("otp_text", body);
                copyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                copyIntent.setAction("COPY_ACTION");
                copyPendingIntent = PendingIntent.getBroadcast(context, 0, copyIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                PendingIntent reportPendingIntent;
                Intent reportIntent = new Intent(context, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                reportIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                reportIntent.putExtra("bankName", bankName);
                reportIntent.putExtra("bankID", bankID);
                reportIntent.putExtra("msgID",msgID );
                reportIntent.setAction("REPORT_ACTION");
                reportPendingIntent = PendingIntent.getActivity(context, 0, reportIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                dbhelper=new SQLHelper(context);
                dbhelper.open();

                msgDateTime=appConstants.formatDate(msgDateTime);

                Cursor dataCursor=dbhelper.Select("select id from BANKS where id='"+bankID+"'",null);
                if(dataCursor.moveToFirst())
                {
                    dbhelper.Update("update BANKS set name='"+bankName+"',status='"+bankStatus+"'," +
                            "promo_code='"+bankPromoCode+"' where id='"+bankID+"'");

                    String stat=  dbhelper.Insert("insert into ROOM values('"+msgID+"'," +
                            "'"+bankID+"','"+body+"','"+title+"'," +
                            "'"+msgDateTime+"'," +
                            "'"+readFlag+"','"+sender+"','"+msgCat+"','"+msgType+"','"+base64Content+"','no_location'," +
                            "'"+accountBalance+"','"+cardtBalance+"','"+trxAmount+"','"+hasFlag+"'," +
                            "'"+accNumber+"','"+isReported+"','"+reportReason+"','"+actionName+"','"+deleteFlag+"','"+cardNumber+"','"+syncdFlag+"')");
                }
                else
                {
                    String result=   dbhelper.Insert("insert into BANKS values('"+bankID+"','"+bankName+"','','"+bankStatus+"'," +
                            " '','"+bankIsPinned+"','"+bankPromoCode+"','"+bankModifiedDate+"')");

                    String stat=  dbhelper.Insert("insert into ROOM values('"+msgID+"'," +
                            "'"+bankID+"','"+body+"','"+title+"'," +
                            "'"+msgDateTime+"'," +
                            "'"+readFlag+"','"+sender+"','"+msgCat+"','"+msgType+"','"+base64Content+"'," +
                            "'no_location','"+accountBalance+"','"+cardtBalance+"','"+trxAmount+"','"+hasFlag+"'," +
                            " '"+accNumber+"','"+isReported+"','"+reportReason+"','"+actionName+"','"+deleteFlag+"','"+cardNumber+"','"+syncdFlag+"')");


                }

                appConstants.BackupDatabase();


                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("DELETE_ACTION");
                intentFilter.setPriority(100);
                NotificationReceiver notificationReceiver = new NotificationReceiver();
                context.registerReceiver(notificationReceiver, intentFilter);

                Bitmap notifyImage = BitmapFactory.decodeResource(context.getResources(), R.mipmap.icon);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder;

                if(msgCat.equalsIgnoreCase("4"))//OTPModel Notification
                {
                     notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.mipmap.icon)
                            .setLargeIcon(notifyImage)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                            .addAction(R.drawable.ic_left, "Delete",deletePendingIntent)
                            .addAction(R.drawable.ic_left_blue, "Copy OTP", copyPendingIntent)
                            //.setColor(Color.parseColor("#FFE74C3C"))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationManager.IMPORTANCE_HIGH)
                            .setContentIntent(msgPendingIntent);
                }
                else
                {
                     notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.mipmap.icon)
                            .setLargeIcon(notifyImage)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                            .addAction(R.drawable.ic_left, "Delete",deletePendingIntent)
                            .addAction(R.drawable.ic_left_blue, "Report Transaction", reportPendingIntent)
                            //.setColor(Color.parseColor("#FFE74C3C"))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationManager.IMPORTANCE_HIGH)
                            .setContentIntent(msgPendingIntent);
                }






                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
                {

                    NotificationChannel channel = new NotificationChannel("default",context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                    channel.setDescription(body);
                    notificationManager.createNotificationChannel(channel);
                    notificationBuilder.setChannelId("default");

                }

                notificationManager.notify(0, notificationBuilder.build());

               // ProjectRepository.getInstance(this).fillBanksList();
               // ProjectRepository.getInstance(this).fillChatRoomData(bankID,"");

                LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());

                Intent intent = new Intent("receiveAction");
                intent.putExtra("bankID",bankID);
                intent.putExtra("msgID",msgID);
                broadcaster.sendBroadcast(intent);

            }
            catch (Exception xx)
            {
                xx.getMessage();
            }



    }

    @Override
    public void successRequest(Object classObject, String apiName) {

    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }
}