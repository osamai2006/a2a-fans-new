package com.a2a.fans.FirbaseServices;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.a2a.fans.utls.ClipboardManager;
import com.a2a.fans.utls.SQLHelper;

public class NotificationReceiver extends BroadcastReceiver {

    String msgID="", otpTXT="";
    long bankID;



    @Override
    public void onReceive(Context context, Intent intent)
    {


        String action = intent.getAction();
        if (action.equalsIgnoreCase("DELETE_ACTION"))
        {
            msgID = intent.getStringExtra("msgID");
            bankID = intent.getLongExtra("bankID",0);

            try {

                SQLHelper dbhelper = new SQLHelper(context);
                dbhelper.open();
                //String query = "delete from ROOM where id in ('" + msgID_Delete + "')";
                String query="update Room set actionName='delete',deleteFlag='true',syncFlag='1' where id in ("+msgID+")";
                dbhelper.Update(query);
                dbhelper.close();

                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancelAll();
            }
            catch (Exception xx)
            {
                xx.getMessage();
            }

        }
        else if (action.equalsIgnoreCase("COPY_ACTION"))
        {
            otpTXT=intent.getStringExtra("otp_text");
            String [] otpArr=otpTXT.split(":");
            ClipboardManager.getInstance().setClipboard(context,otpArr[1]);

            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancelAll();

        }

    }

}
