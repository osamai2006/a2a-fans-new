package com.a2a.fans.FirbaseServices;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.helpers.sharedPrefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Osama Ibrahim on 3/20/2016.
 */



public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements ProjectRepository.ResponseHandler {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh()
    {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sharedPrefs.getInstance().setStringPreference(getApplicationContext(), appConstants.mobileToken_KEY,refreshedToken);

        try
        {
            String mobileNumber=sharedPrefs.getInstance().getStringPreference(AppController.getInstance().getApplicationContext(),appConstants.mobileNum_KEY);

            if(!mobileNumber.equalsIgnoreCase(""))
            {
                ProjectRepository.getInstance(this).refreshToken(refreshedToken);
            }


        }
        catch (Exception xx){}


        Intent registrationComplete = new Intent(ConfigFCM.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }


    @Override
    public void successRequest(Object classObject, String apiName)
    {
        if(apiName.equalsIgnoreCase(URLClass.RefreshToken))
        {

        }

    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }
}