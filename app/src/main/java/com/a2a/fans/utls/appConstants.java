package com.a2a.fans.utls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;

import com.a2a.fans.helpers.sharedPrefs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Osama on 12/11/2018.
 */

public class appConstants
{

    public  static String mobileToken_KEY="token_key";
    public  static String isLoggedIn_KEY="isloggedIn";
    public  static String countryCode_KEY="countryCode";
    public  static String mobileNum_KEY="mobileNum";
    public  static String language_KEY="language";

    public  static String uuid="";
    public  static String mobileNumber="";
    public  static String countryCode="";
    public  static String authKey="";
    public  static String countryName="";
    public  static String  current_language="en";
    public  static boolean offlineMode=false;
    public  static boolean submitOnlineIsCalled=false;



    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception xx) {
            xx.toString();
        }
    }



    public static Bitmap imageFrom64(String imageStrings)
    {
        Bitmap decodedByte = null;
        try
        {
            byte[] decodedString = Base64.decode(imageStrings, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            //return decodedByte;
        }
        catch (Exception xx)
        {
            xx.toString();
        }
        return decodedByte;
    }

    public static String currentDate(Boolean Time,Boolean Date,Boolean DateTime) {

        String FulDate = "";
        String am_pm_string="";
        final Calendar c = Calendar.getInstance();
        int hour=c.get(Calendar.HOUR);
        int mint=c.get(Calendar.MINUTE);
        int sec=c.get(Calendar.SECOND);
        int am_pm=c.get(Calendar.AM_PM);
        if(am_pm==Calendar.AM)
        {
            am_pm_string="AM";
        }
        else
        {
            am_pm_string="PM";
        }

        int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
        String DateString=String.valueOf(todaysDate);
        String Year=DateString.substring(0, 4);
        String Month=DateString.substring(4, 6);
        String Day=DateString.substring(6, 8);

        if(Date==true)
        {
            FulDate=Year+"-"+Month+"-"+Day;
        }
        else if(Time==true)
        {
            FulDate=hour+":"+mint+":"+sec;
        }
        else if(DateTime==true)
        {
            FulDate=Day+"/"+Month+"/"+Year+" "+hour+":"+mint+":"+sec+" "+am_pm_string ;
        }
        return(String.valueOf(FulDate));

    }

    public static String getDate(long timeStamp){

        try{

            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timeStamp * 1000L);
            String date="";

                date = DateFormat.format("dd-MM-yyyy/hh:mm a", cal).toString();

            return  date;
        }
        catch(Exception ex)
        {
            return "";
        }
    }

        public static void BackupDatabase() throws IOException
    {
        boolean success =false;


        File file = null;
        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"FANsBackup");
        //file = new File("/sdcard"+File.separator+"CeaserPlusBackup/");

        if(file.exists())
        {
            success =true;
        }
        else
        {
            success = file.mkdir();
        }


        if(success)
        {
            String inFileName = "/data/data/com.a2a.fans/databases/ChattingDB.db";
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory()+"/FANsBackup/ChattingDB.db";
            //Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);
            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0){
                output.write(buffer, 0, length);
            }

            output.flush();
            output.close();
            fis.close();
        }
    }


    public static String convertDate(String strDate) {
        if (strDate == null || strDate.trim().length() == 0) {
            return "";
        }
        try {
            SimpleDateFormat spf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy",Locale.ENGLISH);
            Date newDate = spf.parse(strDate);
            spf = new SimpleDateFormat("dd-MM-yyyy",Locale.ENGLISH);
            String finalDate = spf.format(newDate);
            return finalDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getCurrentDate(Boolean Time,Boolean Date,Boolean DateTime) {

        String FulDate = null;
        final Calendar c = Calendar.getInstance();
        int hour=c.get(Calendar.HOUR);
        int mint=c.get(Calendar.MINUTE);
        int sec=c.get(Calendar.SECOND);

        int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
        String DateString=String.valueOf(todaysDate);
        String Year=DateString.substring(0, 4);
        String Month=DateString.substring(4, 6);
        String Day=DateString.substring(6, 8);

        if(Date==true)
        {
            FulDate=Year+"-"+Month+"-"+Day;
        }
        else if(Time==true)
        {
            FulDate=hour+":"+mint+":"+sec;
        }
        else if(DateTime==true)
        {
            FulDate=Day+"-"+Month+"-"+Year+" "+hour+":"+mint+":"+sec;
        }
        return(String.valueOf(FulDate));

    }

    public static String genarateID()
    {
        String Final_ID;
        UUID uniqID=UUID.randomUUID();
        String [] MyID=UUID.randomUUID().toString().split("-");
        Final_ID=MyID[0].toString();
        return Final_ID;

    }

    public static String encodeImgTo64(Bitmap bm)
    {
        String encode64="";
        try
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 70, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            encode64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        catch (Exception xx){}

        return encode64;

    }

    public static String formatDate(String date)
    {
        String formatedDate=date;
        try
        {
            String [] dateArr=date.split("T");
            String dateString=dateArr[0].toString();
            String [] timeArr=dateArr[1].split("\\.");
            String timeString=timeArr[0];
            formatedDate=dateString+"  "+timeString;


        }
        catch (Exception cc)
        {
            cc.toString();
        }
        return formatedDate;
    }

    public static void openShareIntent(Context context,String text)
    {
        try
        {
            String shareBody = text;
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "fans App");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            context.startActivity(Intent.createChooser(sharingIntent, "Share Using : "));
        }
        catch (Exception xx){}



    }

    public static void setInitialLanguage(String language,Activity activity) {
        Locale locale = new Locale(language);
        Resources res = activity.getApplicationContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        activity.onConfigurationChanged(conf);

        if(language.equalsIgnoreCase("ar"))
        {
            appConstants.current_language="ar";
        }
        else
        {
            appConstants.current_language="en";
        }

        sharedPrefs.getInstance().setStringPreference(activity.getApplicationContext(),appConstants.language_KEY,language);

    }




}
