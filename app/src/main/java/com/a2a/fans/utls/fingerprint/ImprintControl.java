package com.a2a.fans.utls.fingerprint;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.widget.LinearLayout;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;



public class ImprintControl {

    private LinearLayout fpLinear;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private static Activity activity;
    public static ImprintControl imprintControl;
    private Cipher cipher;
    private KeyStore keyStore;
    private String KEY_NAME = "Burgan Table";
    private FingerprintHandler helper;
    private boolean isCreation = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public ImprintControl(Activity activity) {
        this.activity = activity;
        keyguardManager = (KeyguardManager) activity.getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) activity.getSystemService(FINGERPRINT_SERVICE);
    }

    public static ImprintControl getInstance(Activity activity) {
        if (imprintControl == null || ImprintControl.activity != activity)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imprintControl = new ImprintControl(activity);
            }
        return imprintControl;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean isSupportHardware() {
        if (fingerprintManager == null)
            fingerprintManager = (FingerprintManager) activity.getSystemService(FINGERPRINT_SERVICE);
        return fingerprintManager.isHardwareDetected();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean checkFingerprintSupportHardware(Activity activity) {
        try {
            return ((FingerprintManager) activity.getSystemService(FINGERPRINT_SERVICE)).isHardwareDetected();
        } catch (NoClassDefFoundError error) {
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasEnrrolledFingerPrints() {
        return fingerprintManager.hasEnrolledFingerprints();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void startListen(String activityName, String activityMethod) {

        if (isCreation == false && isSupportHardware() && hasEnrrolledFingerPrints()) {
            generateKey();


            if (cipherInit()) {
                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                helper = new FingerprintHandler(activity, activityName, activityMethod);
                helper.startAuth(fingerprintManager, cryptoObject);
                isCreation = true;


            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void stopListen() {
        if (helper != null && isSupportHardware()) {
            isCreation = false;
            helper.stopAuth();

        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }


        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }


        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


}
