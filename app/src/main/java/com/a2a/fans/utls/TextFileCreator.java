package com.a2a.fans.utls;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextFileCreator {

    public static void writeToFileAndShare(String body, Context context)
    {

        FileOutputStream fos = null;

        try {
            final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/fans_files/" );

            if (!dir.exists())
            {
                if(!dir.mkdirs())
                {
                    //no permission
                }
            }

            final File myFile = new File(dir, "Transaction_log.txt");

            if (!myFile.exists())
            {
                myFile.createNewFile();
            }

            fos = new FileOutputStream(myFile);
            fos.write(body.getBytes());
            fos.close();

            sharePDFFile(context,"Transaction_log.txt");

        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

//    public static void writeToFileAndShare(String data, Context context)
//    {
//        try {
//            String directory_path= Environment.getExternalStorageDirectory().getAbsolutePath() + "/fans_files/";
//            File file = new File(directory_path);
//            if (!file.exists()) {
//                file.mkdirs();
//            }
//            String targetTextFile= Environment.getExternalStorageDirectory()+"/fans_files/Transaction_log.txt/";
//            File filePath = new File(targetTextFile);
//
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filePath.toString(), Context.MODE_PRIVATE));
//            outputStreamWriter.write(data);
//            outputStreamWriter.close();
//
//            sharePDFFile(context,"Transaction_log.txt");
//
//
//
//        }
//        catch (IOException e)
//        {
//        }
//    }

    private static void sharePDFFile(Context context, String textFileName) {

        try {

            File imagePath = new File(Environment.getExternalStorageDirectory(), "fans_files");
            File newFile = new File(imagePath, textFileName);
            Uri contentUri = FileProvider.getUriForFile(context, "com.a2a.fans", newFile);

            if (contentUri != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                context.startActivity(Intent.createChooser(shareIntent, "Choose an app").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }

    }
}
