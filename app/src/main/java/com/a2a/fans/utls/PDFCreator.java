package com.a2a.fans.utls;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.loadingWheel;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDDocumentCatalog;
import com.tom_roush.pdfbox.pdmodel.interactive.form.PDAcroForm;
import com.tom_roush.pdfbox.pdmodel.interactive.form.PDTextField;

import java.io.File;
import java.io.IOException;

public class PDFCreator {

    public static void fillPdfForm(final Context context, final String name, final String message, final String dateTime, final long bankID, final String accNum) {

        loadingWheel.startSpinwheel(context,false,true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String [] datetimeArr=dateTime.split("  ");
                    String date=datetimeArr[0];
                    String time=datetimeArr[1];

                    PDDocument document;
                    String fileName=bankID+"_form.pdf";
                    //fileName="Transaction Details new.pdf";

                    AssetManager assetManager =context.getAssets();
                    try
                    {
                        document = PDDocument.load(assetManager.open(fileName));
                    }
                    catch (IOException xx)
                    {
                        Toast.makeText(AppController.getInstance().getApplicationContext(), "No Form For The Selected Bank", Toast.LENGTH_LONG).show();
                        return;
                    }

                    PDDocumentCatalog docCatalog = document.getDocumentCatalog();
                    PDAcroForm acroForm = docCatalog.getAcroForm();


                    // Fill the text field
//                    PDTextField nameTXT = (PDTextField) acroForm.getField("bankName");
//                    nameTXT.setValue("Customer name : "+name);
//                    nameTXT.setReadOnly(true);

                    PDTextField dateTXT = (PDTextField) acroForm.getField("date");
                    dateTXT.setValue("Date : "+date);
                    // Optional: don't allow this field to be edited
                    dateTXT.setReadOnly(true);

                    PDTextField timeTXT = (PDTextField) acroForm.getField("time");
                    timeTXT.setValue("Time : "+time);
                    // Optional: don't allow this field to be edited
                    timeTXT.setReadOnly(true);

                    PDTextField accTXT = (PDTextField) acroForm.getField("acc_num");
                    accTXT.setValue("Transaction ID : "+accNum);
                    accTXT.setReadOnly(true);

//                    if(bankID==1000)
//                    {  PDTextField accTXT = (PDTextField) acroForm.getField("acc_num");
//                        accTXT.setValue("");
//                        accTXT.setReadOnly(true);}
//                    else
//                    {
//
//                    }




                    //String text= URLEncoder.encode(message,"utf-8");
                    String text=new String(message.getBytes("ISO-8859-1"), "UTF-8");

                    PDTextField msgTXT = (PDTextField) acroForm.getField("msg");
                    msgTXT.setValue(text);
                    // Optional: don't allow this field to be edited
                    msgTXT.setReadOnly(true);




                    //String path = root.getAbsolutePath() + "/Download/FilledForm.pdf";
                    String directory_path= Environment.getExternalStorageDirectory().getAbsolutePath() + "/fans_files/";
                    File file = new File(directory_path);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    String targetPdf= Environment.getExternalStorageDirectory()+"/fans_files/FANSTransaction.pdf";
                    File filePath = new File(targetPdf);
                    document.save(filePath);
                    document.close();

                    loadingWheel.stopLoading();

                    sharePDFFile(context,"FANSTransaction.pdf");

                } catch (final IOException e)
                {

                    loadingWheel.stopLoading();
                    Toast.makeText(context, "No Form For The Selected Bank",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();


                }
            }
        }).start();

    }

    private static void sharePDFFile(Context context,String pdfFileName) {

        try {

            File imagePath = new File(Environment.getExternalStorageDirectory(), "fans_files");
            File newFile = new File(imagePath, pdfFileName);
            Uri contentUri = FileProvider.getUriForFile(context, "com.a2a.fans", newFile);

            if (contentUri != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                context.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
            }
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }

    }
}
