package com.a2a.fans.utls;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;

import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.networking.RequestHandler;

import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ClipboardManager
{

    private static ClipboardManager instance;
    public static ClipboardManager getInstance()
    {
        if (instance == null)
        {
            return instance = new ClipboardManager();
        }
        return instance;
    }

    public void setClipboard(Context context,String text) {
        try
        {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)
            {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            }
            else
                {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText(text+"Copied", text);
                clipboard.setPrimaryClip(clip);
            }
        }
        catch (Exception xx)
        {
            xx.toString();
        }
    }


}
