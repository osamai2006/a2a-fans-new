package com.a2a.fans.utls;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import java.io.File;
import java.io.FileOutputStream;

public class TextToImageCreator {

    public static void convertToImage(Context context, String text)
    {
        try
        {

            final Rect bounds = new Rect();
            TextPaint textPaint = new TextPaint() {
                {
                    setColor(Color.BLACK);
                    //setTextAlign(Paint.Align.LEFT);
                    setTextSize(25f);
                    setAntiAlias(true);
                }
            };
            textPaint.getTextBounds(text, 0, text.length(), bounds);
            StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                    bounds.width(), Layout.Alignment.ALIGN_NORMAL, 2.0f, 1.0f, false);
            int maxWidth = -1;
            for (int i = 0; i < mTextLayout.getLineCount(); i++)
            {
                if (maxWidth < mTextLayout.getLineWidth(i)) {
                    maxWidth = (int) mTextLayout.getLineWidth(i);
                }
            }
            final Bitmap bmp = Bitmap.createBitmap(maxWidth , mTextLayout.getHeight(),
                    Bitmap.Config.ARGB_8888);
            bmp.eraseColor(Color.WHITE);// just adding black background
            final Canvas canvas = new Canvas(bmp);
            mTextLayout.draw(canvas);

            String directory_path= Environment.getExternalStorageDirectory().getAbsolutePath() + "/fans_files/";
            File file = new File(directory_path);
            if (!file.exists()) {
                file.mkdirs();
            }
            String targetPdf= Environment.getExternalStorageDirectory()+"/fans_files/FANSTransaction.png";
            File filePath = new File(targetPdf);

            FileOutputStream stream = new FileOutputStream(filePath); //create your FileOutputStream here
            bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
            bmp.recycle();
            stream.close();

            sharePDFFile(context,"FANSTransaction.png");
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }
    }

    private static void sharePDFFile(Context context, String imgFileName) {

        try {

            File imagePath = new File(Environment.getExternalStorageDirectory(), "fans_files");
            File newFile = new File(imagePath, imgFileName);
            Uri contentUri = FileProvider.getUriForFile(context, "com.a2a.fans", newFile);

            if (contentUri != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                context.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
            }
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }

    }
}
