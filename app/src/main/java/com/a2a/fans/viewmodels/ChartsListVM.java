package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.CustomChartModel;
import com.a2a.fans.models.DashboardModel;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.views.Adapters.CustomChartsAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChartsListVM extends AndroidViewModel {

    Activity activity;
    SQLHelper dbhelper;
     public MutableLiveData<ArrayList<CustomChartModel>> currentMonthList;
     public MutableLiveData<ArrayList<CustomChartModel>> previousMonthList;
     CustomChartModel customChartModel;


     ArrayList<CustomChartModel> lastChartArrayList;


    public ChartsListVM(Application application) {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();
        dbhelper=new SQLHelper(activity.getApplicationContext());
        dbhelper.open();
    }


    public MutableLiveData<ArrayList<CustomChartModel>> fillCurrentMonthData()
    {
        ArrayList<CustomChartModel> arrayList = new ArrayList<>();
        currentMonthList=new MediatorLiveData<>();
        Cursor dataCursor;

        try
        {

            String query="select id,name,image,status from BANKS ";

            dataCursor = dbhelper.Select(query, null);
            String startDate="",endDate="";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = Calendar.getInstance().getTime();
            Calendar c = Calendar.getInstance();

            int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
            String DateString=String.valueOf(todaysDate);
            String Year=DateString.substring(0, 4);
            String Month=DateString.substring(4, 6);
            String Day=DateString.substring(6, 8);

            startDate = String.valueOf(Year)+"-"+ String.valueOf(Month) +"-01  00:00:00" ;
            endDate = String.valueOf(sdf.format(currentDate))+" 00:00:00" ;

            if(dataCursor.moveToFirst())
            {
                arrayList=new ArrayList<>();

                do {
                    String bank_id=dataCursor.getString(0);
                    String accBalnce="0",cardBalnce="0",bankName="",bankImage="";

                    Cursor accBalanceCurs=dbhelper.Select("select b.id,b.name,b.image,r.account_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('2') and r.sender='bank' and b.id='"+bank_id+"'" +
                            " and R.deleteFlag='' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"'  order by cast(r.rowid as integer) desc limit 1",null);

                    Cursor cardBalanceCurs=dbhelper.Select("select b.id,b.name,b.image,r.card_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('1') and r.sender='bank' and b.id='"+bank_id+"' " +
                            " and R.deleteFlag='' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"'  order by cast(r.rowid as integer) desc limit 1",null);

                    if (accBalanceCurs.moveToFirst())
                    {
                        bankName=accBalanceCurs.getString(1);
                        accBalnce=accBalanceCurs.getString(3);
                    }

                    if (cardBalanceCurs.moveToFirst())
                    {
                        bankName=cardBalanceCurs.getString(1);
                        cardBalnce=cardBalanceCurs.getString(3);
                    }

                    customChartModel = new CustomChartModel(bank_id,bankName,accBalnce,cardBalnce,"current");
                    arrayList.add(customChartModel);

                }
                while (dataCursor.moveToNext());
                currentMonthList.postValue(arrayList);

            }

            return currentMonthList;
        }
        catch (Exception xx)
        {
            return currentMonthList;
        }
    }

    public int getMax( MutableLiveData<List<CustomChartModel>> list)
    {
        int max = Integer.MIN_VALUE;
        int accMax=Integer.MIN_VALUE;
        int cardMax=Integer.MIN_VALUE;
        for(int i=0; i<list.getValue().size(); i++)
        {
            if(Integer.parseInt(list.getValue().get(i).getAccBalance()) > accMax)
            {
                accMax = Integer.parseInt(list.getValue().get(i).getAccBalance());
            }

            if(Integer.parseInt(list.getValue().get(i).getCardBalance()) > cardMax)
            {
                cardMax = Integer.parseInt(list.getValue().get(i).getCardBalance());
            }
        }

        if(accMax>cardMax)
        {
            max=accMax;
        }
        else
        {
            max=cardMax;
        }
        return max;
    }

    public MutableLiveData<ArrayList<CustomChartModel>> fillPreviosMonthData()
    {
        ArrayList<CustomChartModel> arrayList = new ArrayList<>();
        previousMonthList=new MediatorLiveData<>();
        Cursor dataCursor;

        try
        {

            String query="select id,name,image,status from BANKS ";

            dataCursor = dbhelper.Select(query, null);
            String startDate="",endDate="";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = Calendar.getInstance().getTime();
            Calendar c = Calendar.getInstance();

            int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) ) * 100) + (c.get(Calendar.DAY_OF_MONTH));
            String DateString=String.valueOf(todaysDate);
            String Year=DateString.substring(0, 4);
            String Month=DateString.substring(4, 6);
            String Day=DateString.substring(6, 8);

            startDate = String.valueOf(Year)+"-"+ String.valueOf(Month) +"-01  00:00:00" ;
            endDate = String.valueOf(Year)+"-"+ String.valueOf(Month) +"-30 00:00:00" ;

            if(dataCursor.moveToFirst())
            {
                arrayList=new ArrayList<>();

                do {
                    String bank_id=dataCursor.getString(0);
                    String accBalnce="0",cardBalnce="0",bankName="",bankImage="";

                    String sqlAcc="select b.id,b.name,b.image,r.account_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('2') and r.sender='bank' and b.id='"+bank_id+"'" +
                            " and R.deleteFlag='' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"'  order by cast(r.rowid as integer) desc limit 1";

                    String sqlCard="select b.id,b.name,b.image,r.card_balance  from room r,banks b where  B.id=R.bank_id" +
                            " and r.msgType in('1','2') and r.msgCategory in ('1') and r.sender='bank' and b.id='"+bank_id+"' " +
                            " and R.deleteFlag='' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"'  order by cast(r.rowid as integer) desc limit 1";

                    Cursor accBalanceCurs=dbhelper.Select(sqlAcc,null);

                    Cursor cardBalanceCurs=dbhelper.Select(sqlCard,null);

                    if (accBalanceCurs.moveToFirst())
                    {
                        bankName=accBalanceCurs.getString(1);
                        accBalnce=accBalanceCurs.getString(3);
                    }

                    if (cardBalanceCurs.moveToFirst())
                    {
                        bankName=cardBalanceCurs.getString(1);
                        cardBalnce=cardBalanceCurs.getString(3);
                    }

                    customChartModel = new CustomChartModel(bank_id,bankName,accBalnce,cardBalnce,"last");
                    arrayList.add(customChartModel);

                }
                while (dataCursor.moveToNext());
                previousMonthList.postValue(arrayList);

            }

            return previousMonthList;
        }
        catch (Exception xx)
        {
            return previousMonthList;
        }
    }

//    public  ArrayList<CustomChartModel>  fillCurrentMonthData()
//    {
//        try
//        {
//            currentChartArrayList=new ArrayList<>();
//            String query="select id,name,image,status from BANKS ";
//           Cursor banksCursor = dbhelper.Select(query, null);
//           String startDate="",endDate="";
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date currentDate = Calendar.getInstance().getTime();
//            Calendar c = Calendar.getInstance();
//
//            int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
//            String DateString=String.valueOf(todaysDate);
//            String Year=DateString.substring(0, 4);
//            String Month=DateString.substring(4, 6);
//            String Day=DateString.substring(6, 8);
//
//            startDate = String.valueOf(Year)+"-"+ String.valueOf(Month) +"-01  00:00:00" ;
//            endDate = String.valueOf(sdf.format(currentDate))+" 00:00:00" ;
//
//            if(banksCursor.moveToFirst())
//            {
//                do {
//                     String bank_id=banksCursor.getString(0);
//
//                     String sql="select b.id,b.name,b.image,r.account_balance ,r.card_balance" +
//                            " from room r,banks b where  B.id=R.bank_id" +
//                            " and r.msgType in ('1','2') and r.msgCategory in ('1','2') and r.sender='bank' " +
//                            " and b.id='"+bank_id+"' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"' order by cast(r.rowid as integer) desc limit 1";
//
//                    Cursor balanceCursor=dbhelper.Select(sql,null);
//
//                    if(balanceCursor.moveToFirst())
//                    {
//                        String bankID=balanceCursor.getString(0);
//                        String bankName=balanceCursor.getString(1);
//                        //String bankImage=balanceCursor.getString(2);
//                        String account_balance=balanceCursor.getString(3);
//                        String card_balance=balanceCursor.getString(4);
//
//                        customChartModel = new CustomChartModel(bankID,bankName,account_balance,card_balance,"current");
//                        currentChartArrayList.add(customChartModel);
//
//                    }
//
//                }
//                while (banksCursor.moveToNext());
//
//            }
//            return currentChartArrayList;
//        }
//        catch (Exception xx)
//        {
//            return currentChartArrayList;
//        }
//    }



//    public   ArrayList<CustomChartModel> fillLastMonthData()
//    {
//        try
//        {
//            lastChartArrayList=new ArrayList<>();
//
//            String query="select id,name,image,status from BANKS ";
//
//           Cursor banksCursor = dbhelper.Select(query, null);
//            String startDate="",endDate="";
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date currentDate = Calendar.getInstance().getTime();
//
//            Calendar c = Calendar.getInstance();
//            int year = c.get(Calendar.YEAR);
//            int month = c.get(Calendar.MONTH);
//            int day = 1;
//
//
//            int curentMonthDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH)+1 ) * 100) + (c.get(Calendar.DAY_OF_MONTH));
//            String DateString=String.valueOf(curentMonthDate);
//
//            int lastMonthDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) ) * 100) + (c.get(Calendar.DAY_OF_MONTH));
//            String lastDateString=String.valueOf(lastMonthDate);
//
//            String Year=DateString.substring(0, 4);
//            String currentMonth=DateString.substring(4, 6);
//            String lasttMonth=lastDateString.substring(4, 6);
//
//            //String Day=DateString.substring(6, 8);
//
//            startDate = String.valueOf(Year)+"-"+ String.valueOf(lasttMonth) +"-01  00:00:00" ;
//            endDate = String.valueOf(Year)+"-"+ String.valueOf(currentMonth) +"-01 00:00:00";
//
//            if(banksCursor.moveToFirst())
//            {
//                do {
//                    String bank_id=banksCursor.getString(0);
//
//                    String sql="select b.id,b.name,b.image,r.account_balance ,r.card_balance " +
//                            " from room r,banks b where  B.id=R.bank_id" +
//                            " and r.msgType='1' and r.msgCategory in ('1','2')" +
//                            " and r.sender='bank' and b.id='"+bank_id+"' AND date BETWEEN '"+startDate.trim()+"' AND '"+endDate+"' order by cast(r.rowid as integer) desc limit 1";
//
//                    Cursor balanceCursor=dbhelper.Select(sql,null);
//
//                    if(balanceCursor.moveToFirst())
//                    {
//                        String bankID=balanceCursor.getString(0);
//                        String bankName=balanceCursor.getString(1);
//                        //String bankImage=balanceCursor.getString(2);
//                        String account_balance=balanceCursor.getString(3);
//                        String card_balance=balanceCursor.getString(4);
//
//                        double accBalanceDouble=Double.parseDouble(account_balance)/2.2;
//                        double cardBalanceDouble=Double.parseDouble(card_balance)/0.3;
//
//                        customChartModel = new CustomChartModel(bankID,bankName,String.valueOf(accBalanceDouble),String.valueOf(cardBalanceDouble),"last");
//                        lastChartArrayList.add(customChartModel);
//
//                    }
//
//                }
//                while (banksCursor.moveToNext());
//
//            }
//
//            return lastChartArrayList;
//        }
//        catch (Exception xx)
//        {
//            return lastChartArrayList;
//        }
//    }
}
