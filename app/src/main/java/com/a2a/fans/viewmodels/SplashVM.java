package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.widget.Toast;
import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.models.VersionModel;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.views.activities.ChattingActivity;
import com.a2a.fans.views.activities.MainActivity;
import com.a2a.fans.views.activities.registerActivity;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class SplashVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {

    Activity activity;
    private static int SPLASH_TIME_OUT = 2500;
    boolean isLoggedIn = false;
    CountDownTimer timer;
    SQLHelper dbhelper;
    //LiveData<List<BanksListModel>> banksArrayList=new MediatorLiveData<>();
   public MutableLiveData<Boolean> isInitiated=new MediatorLiveData<>();

    public SplashVM(Application application)
    {
        super(application);
        this.activity=AppController.getInstance().getCurrentActivity();
        isInitiated.setValue(false);

    }



    public void initLanguage()
    {
        String loc = "en";
        String currentLoc = sharedPrefs.getInstance().getStringPreference(activity.getApplicationContext(), appConstants.language_KEY);
        if (currentLoc != "")
        {
            loc = currentLoc;
        }

        if (loc.equalsIgnoreCase("en"))
        {
            appConstants.current_language = "en";
            Locale locale = new Locale("en");
            Resources res = activity.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            res.updateConfiguration(conf, dm);
            activity.onConfigurationChanged(conf);

        } else {
            appConstants.current_language = "ar";
            Locale locale = new Locale("ar");
            Resources res = activity.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            res.updateConfiguration(conf, dm);
            activity.onConfigurationChanged(conf);

        }
    }

    public void initSplash() {
        try {

            NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancelAll();

            if(checkSimCard()==false)
            {
                return;
            }

            isLoggedIn = sharedPrefs.getInstance().getBooleanPreference(activity.getApplicationContext(), appConstants.isLoggedIn_KEY, false);
            appConstants.uuid = UUID.randomUUID().toString();
            getCountryName();

            if(isNetworkConnected())
            {
                appConstants.offlineMode=false;
                //loadingWheel.startSpinwheel(activity, false, true);
                ProjectRepository.getInstance(this).getPublicKEY();
            }
            else
            {
                appConstants.offlineMode=true;
                if(isLoggedIn)
                {
                    activity.startActivity(new Intent(activity.getApplicationContext(),MainActivity.class));
                    activity.finish();
                }
                else //register mode
                {
                    new AlertDialog.Builder(activity)
                            .setTitle("Hint")
                            .setCancelable(false)
                            .setMessage(R.string.no_connection)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                  initSplash();
                                }
                            })
                            //.setNegativeButton(android.R.string.no, null)
                            .show();
                }

            }


        } catch (Exception xx) {
        }
    }



    public void openPlayStore() {

        final String appPackageName = activity.getPackageName();
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }


    public boolean checkSimCard()
    {
        try
        {
            TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            boolean simAvailablity=  !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);
            if(!simAvailablity)
            {

                new AlertDialog.Builder(activity)
                        .setTitle("Hint")
                        .setIcon(R.drawable.hint_icon)
                        .setCancelable(false)
                        .setMessage(activity.getString(R.string.no_sim))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                              activity.finish();
                            }
                        })
                        .show();


                return false ;
            }
        }
        catch (Exception xx)
        {
            xx.toString();
            return false ;
        }
        return true;
    }

    public String getCountryName()
    {
        try
        {
            TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getSimCountryIso();
            appConstants.countryName=countryCodeValue;
            return countryCodeValue;
        }
        catch (Exception xx)
        {
            return xx.getMessage();
        }
    }

    public void registerClicked()
    {

           Intent i = new Intent(activity.getApplicationContext(), registerActivity.class);
            activity.startActivity(i);
            activity.finish();


    }

    public boolean checkPendingIntentAction()
    {
        if(activity.getIntent().getAction().equalsIgnoreCase("OPEN_ACTION"))
        {
            String msgID=activity.getIntent().getStringExtra("msgID");
            long bankID=activity.getIntent().getLongExtra("bankID",0);
            String bankName=activity.getIntent().getStringExtra("bankName");

            activity.startActivity(new Intent(activity.getApplicationContext(), ChattingActivity.class)
                    .putExtra("msgID",msgID)
                    .putExtra("bankID",bankID)
                    .putExtra("bankName",bankName));

            activity.finish();

            return true;
        }
        else if(activity.getIntent().getAction().equalsIgnoreCase("REPORT_ACTION"))
        {
            String msgID=activity.getIntent().getStringExtra("msgID");
            long bankID=activity.getIntent().getLongExtra("bankID",0);
            String bankName=activity.getIntent().getStringExtra("bankName");

            activity.startActivity(new Intent(activity.getApplicationContext(), ChattingActivity.class)
                    .putExtra("msgID",msgID)
                    .putExtra("bankID",bankID)
                    .putExtra("bankName",bankName)
                    .setAction("REPORT_ACTION"));

            activity.finish();

            return true;
        }
        else
        {
            return false;
        }

    }

    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    @Override
    public void successRequest(Object classObject, String apiName)
    {
        if (apiName.equalsIgnoreCase(URLClass.GetCerPublicKey))
        {
            //loadingWheel.startSpinwheel(activity, false, true);
            ProjectRepository.getInstance(this).getVersion();
        }

        if(apiName.equalsIgnoreCase(URLClass.GetVersion))
        {
            VersionModel versionModel = (VersionModel) classObject;

            sharedPrefs.getInstance().setStringPreference(activity.getApplicationContext(), appConstants.countryCode_KEY, versionModel.getRegionCode());

            if (versionModel.getIsConutryValid())
            {
                if (versionModel.getIsUpdate())
                {
                    isInitiated.postValue(true);
                    if(isLoggedIn)
                    {
                        if(checkPendingIntentAction()==false)
                        {

                            activity.startActivity(new Intent(activity.getApplicationContext(), MainActivity.class));
                            activity.finish();
                        }


                    }
                }
                else
                {
                    new AlertDialog.Builder(activity)
                            .setTitle("Hint")
                            .setIcon(R.drawable.hint_icon)
                            .setCancelable(false)
                            .setMessage(activity.getString(R.string.not_updated))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                   openPlayStore();
                                }
                            })
                            .show();
                }

            }
            else
            {
                new AlertDialog.Builder(activity)
                        .setTitle("Hint")
                        .setIcon(R.drawable.hint_icon)
                        .setCancelable(false)
                        .setMessage(activity.getString(R.string.country_not_supported))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        })
                        .show();
            }




        }



    }

    public void readTerms()
    {
        new AlertDialog.Builder(activity)
                .setTitle("Term And Conditions")
                .setIcon(R.drawable.hint_icon)
                .setCancelable(false)
                .setMessage(activity.getString(R.string.terms_conditions))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        return;
                    }
                })
                .show();
    }

    @Override
    public void failedRequest(String msg, String apiName)
    {

            Toast.makeText(activity.getApplicationContext(),apiName+" "+msg, Toast.LENGTH_SHORT).show();
            appConstants.offlineMode=true;

            if(isLoggedIn)
            {
                activity.startActivity(new Intent(activity.getApplicationContext(),MainActivity.class));
                activity.finish();
            }
            else //register mode
            {
                new AlertDialog.Builder(activity)
                        .setTitle("Hint")
                        .setCancelable(false)
                        .setMessage(R.string.no_connection)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                initSplash();
                            }
                        })
                        //.setNegativeButton(android.R.string.no, null)
                        .show();
            }



    }
}
