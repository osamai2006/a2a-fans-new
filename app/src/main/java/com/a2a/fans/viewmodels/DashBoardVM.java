package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.DashboardModel;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.SQLHelper;

import java.util.ArrayList;

public class DashBoardVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {

    Activity activity;
    SQLHelper dbhelper;
    ArrayList<DashboardModel> arrayList;

    public DashBoardVM(Application application) {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();
        dbhelper=new SQLHelper(activity.getApplicationContext());
        dbhelper.open();
    }

    public MutableLiveData<ArrayList<DashboardModel>> fillDashBoardList()
    {
        return ProjectRepository.getInstance(this).fillDashboardData();
    }


    @Override
    public void successRequest(Object classObject, String apiName) {

    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }
}
