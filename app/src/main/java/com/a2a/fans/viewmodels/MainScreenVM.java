package com.a2a.fans.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.support.annotation.NonNull;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.SQLHelper;

public class MainScreenVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {

    Context context;
    boolean isLoggedIn = false;
    SQLHelper dbhelper;


    public MainScreenVM(@NonNull Application application)
    {
        super(application);
        this.context= AppController.getInstance().getApplicationContext();
        dbhelper=new SQLHelper(AppController.getInstance().getApplicationContext());
        dbhelper.open();

    }


//    public MutableLiveData<Fragment>  setInitialFragment(Fragment fragment)
//    {
//        MutableLiveData<Fragment> mFragment=new MutableLiveData<>();
//        mFragment.setValue(fragment);
//        return mFragment;
//    }



    @Override
    public void successRequest(Object classObject, String apiName) {



    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }
}
