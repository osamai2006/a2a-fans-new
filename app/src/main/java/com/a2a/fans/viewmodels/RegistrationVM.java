package com.a2a.fans.viewmodels;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.a2a.fans.helpers.loadingWheel;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.RegistrationModel;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.views.activities.otpActivity;

public class RegistrationVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {
    Activity activity;

    public RegistrationVM(Application application)
    {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();
    }



    public  boolean requestRegistration(String mobile,String countryCode)
    {

        try
        {
                appConstants.mobileNumber=mobile;
                appConstants.countryCode=countryCode;
                String token=sharedPrefs.getInstance().getStringPreference(AppController.getInstance().getApplicationContext(),appConstants.mobileToken_KEY);

                loadingWheel.startSpinwheel(activity,false,true);
                ProjectRepository.getInstance(this).registration(token);


        }
            catch (Exception xx)
        {
            xx.getMessage();
        }

        return  true;
    }

    public void grantPermissions() {

        try
        {
            if (ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(), Manifest.permission.READ_SMS)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(), Manifest.permission.READ_SMS)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AppController.getInstance().getApplicationContext(), Manifest.permission.RECEIVE_SMS)

                    == PackageManager.PERMISSION_GRANTED)

            {

            }
            else
            {

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS}, 101);


            }

        }
        catch (Exception xx)
        {
            xx.getMessage();
        }

    }

    @Override
    public void successRequest(Object classObject, String apiName)
    {
        try
        {
            RegistrationModel registrationModel = (RegistrationModel) classObject;

            activity.startActivity(new Intent(activity, otpActivity.class)
                    .putExtra("timeout", registrationModel.getOTPExpiryTime())
                    .putExtra("MobileNo", registrationModel.getMobileNo())
                    .putExtra("utr", registrationModel.getOTP().getUTR())
                    .putExtra("otp", registrationModel.getOTP().getOTP()));

            appConstants.mobileNumber=registrationModel.getMobileNo();

            activity.finish();
        }
         catch (Exception xx)
         {
             xx.toString();
         }

    }

    @Override
    public void failedRequest(String msg, String apiName)
    {
        appConstants.mobileNumber="";
    }


}
