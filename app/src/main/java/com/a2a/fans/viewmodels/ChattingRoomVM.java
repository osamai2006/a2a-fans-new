package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.ChatRoomModel;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.PDFCreator;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.utls.TextFileCreator;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.views.activities.FullImageActivity;
import com.a2a.fans.views.activities.WebviewrActivity;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ChattingRoomVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {

    Activity activity;
    SQLHelper dbhelper;
   public long bankID;
    Cursor dataCursor;
    ChatRoomModel chatRoomModel;
   public MutableLiveData<List<ChatRoomModel>> chatList;

    public ChattingRoomVM(Application application) {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();
        dbhelper=new SQLHelper(activity.getApplicationContext());
        dbhelper.open();

    }

//    public LiveData<List<ChatRoomModel>> fillMessageList(final String bankID, String groupBY)
//    {
//         return ProjectRepository.getInstance(this).fillMessageList(bankID,groupBY);
//    }

    public  MutableLiveData<List<ChatRoomModel>>  fillMessageList(final long bankID, String groupBY)
    {
        /*
        groupBy case : 0 no grouping and show all
                       1 group by cards
                       2 by accounts
                       3 by ads
                       4 by otp
        */
        chatList=new MediatorLiveData<>();
        ArrayList<ChatRoomModel> arrayList=new ArrayList<>();

        try {


//            swipRefresh.setRefreshing(false);

            String query="select B.id,B.name,B.image,B.status,R.id,R.message,R.date,R.readFlag,R.sender,R.msgCategory,R.msgType," +
                    "R.base64,R.locationXY,R.trxAmount,R.hasFlag,R.isReported,R.reportReason,R.actionName,R.deleteFlag " +
                    " from BANKS B,ROOM R WHERE B.id=R.bank_id and R.bank_id='" + bankID +"' and R.deleteFlag='' ";

            if(groupBY.equalsIgnoreCase("1"))//cards
            {
                query=query+" and R.sender='bank' and R.msgCategory='1' ";
            }
            else  if(groupBY.equalsIgnoreCase("2"))//accounts
            {
                query=query+" and R.sender='bank' and R.msgCategory='2' ";
            }
            else  if(groupBY.equalsIgnoreCase("3"))//ads
            {
                query=query+" and R.sender='bank' and R.msgCategory='3' ";
            }
            else  if(groupBY.equalsIgnoreCase("4"))//otp
            {
                query=query+" and R.sender='bank' and R.msgCategory='4' ";
            }

            //dbhelper.Update("update room set status='0' where id in (select id from room Where bank_id='2' order by cast(rowid as integer) desc limit 3)");//for test purpose

            Cursor  dataCursor = dbhelper.Select(query, null);

            if (dataCursor.moveToFirst())
            {
                arrayList = new ArrayList<>();
                arrayList.clear();

                do {
                    String bankId = dataCursor.getString(0);
                    String bankName = dataCursor.getString(1);
                    String bankImage = dataCursor.getString(2);
                    String bankStatus = dataCursor.getString(3);
                    String msgId = dataCursor.getString(4);
                    String msgBody = dataCursor.getString(5);
                    String msgDate = dataCursor.getString(6);
                    String readFlag = dataCursor.getString(7);
                    String sender = dataCursor.getString(8);
                    String msgCat = dataCursor.getString(9);
                    String msgtype = dataCursor.getString(10);
                    String base64 = dataCursor.getString(11);
                    String locationXY = dataCursor.getString(12);
                    String trxAmount = dataCursor.getString(13);
                    String hasFlag = dataCursor.getString(14);
                    String isReported = dataCursor.getString(15);
                    String reportReason = dataCursor.getString(16);
                    String actionName = dataCursor.getString(17);
                    String deleteFlag = dataCursor.getString(18);

                    chatRoomModel = new ChatRoomModel(msgId, msgBody, msgDate, readFlag, sender,msgCat,msgtype,
                            base64,locationXY,"false",trxAmount,hasFlag,isReported,reportReason,actionName,deleteFlag);
                    arrayList.add(chatRoomModel);

                }
                while (dataCursor.moveToNext());
                chatList.postValue(arrayList);


            }
            else
            {
                arrayList.clear();
                chatList.postValue(arrayList);
            }

            return chatList;

        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
            return chatList;
        }

    }
    public LiveData<List<ChatRoomModel>> searchMessageList(final long bankID, String keywordTXT, String amountFromTXT, String amountToTXT, String startDate, String endDate, String accNum, String flaggedMesg)
    {
        return ProjectRepository.getInstance(this).searchChatRoomData(bankID,keywordTXT,amountFromTXT,amountToTXT,startDate,endDate,accNum,flaggedMesg);
    }



    public boolean deleteMsgs(MutableLiveData<List<ChatRoomModel>> arrayList,long bankID)
    {
        JSONArray idsArr=new JSONArray();

        try
        {
            String selectedIDs="";
            for(int i=0;i<arrayList.getValue().size();i++)
            {
                if(arrayList.getValue().get(i).getChecked().equalsIgnoreCase("true"))
                {
                    String id="'"+arrayList.getValue().get(i).getMsgId()+"'";
                    selectedIDs=selectedIDs+id+",";

                    JsonObject idObject=new JsonObject();
                    idsArr.put(id.trim().replace("'",""));
                }

            }
            if(selectedIDs.endsWith(","))
            {
                selectedIDs=selectedIDs.substring(0, selectedIDs.length()-1) + "";

            }
            //String query="delete from ROOM where id in ("+selectedIDs+")";
            String query="update Room set actionName='delete',deleteFlag='true',syncFlag='1' where id in ("+selectedIDs+")";
            dbhelper.Update(query);

            //loadingWheel.startSpinwheel(activity,false,true);
            ProjectRepository.getInstance(this).deleteMessage(idsArr);

            return true;

        }
        catch (Exception xx)
        {
            xx.getMessage();
            return  false;
        }

    }


    public MutableLiveData<Boolean> reportMessage(final long bankID, final String msgID)
    {
        MutableLiveData<Boolean> result=new MediatorLiveData<>();
         Dialog dialog;
        final String[] reportReason = {"1"};

        dialog = new Dialog(activity, android.R.style.Theme_Holo_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.report_msg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Button submitBTN= dialog.findViewById(R.id.submitBTN);
        RadioGroup reporReasontRG= dialog.findViewById(R.id.reporReasontRG);

        reporReasontRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId==R.id.notMineRB)
                {
                    reportReason[0] ="1";
                }
                else if(checkedId==R.id.repeatedRB)
                {
                    reportReason[0] ="2";
                }
                else
                {
                    reportReason[0] ="";
                }
            }
        });


        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String query="update ROOM set isReported='true', reportReason='"+reportReason[0]+"'," +
                        " actionName='report',syncFlag='1' where id ='"+msgID+"'";
                dbhelper.Update(query);

                updateMsgStatus(bankID,msgID,"false","true", reportReason[0]);

              //  fillMessageList(bankID,"");

                dialog.dismiss();
                result.postValue(true);


            }
        });


        dialog.show();
        return result;


    }

    public void updateMsgStatus(long bankID,String msgID,String isMsgFlaged,String reportFlag,String reportReason)
    {
        ProjectRepository.getInstance(this).updateMsgStatus(msgID,isMsgFlaged,reportFlag, reportReason);
    }

    public  void openPDFMsg(String url)
    {
        try
        {
            WebviewrActivity.url=url;
            activity.startActivity(new Intent(activity.getApplicationContext(),WebviewrActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        catch (Exception xx)
        {

        }

    }

    public   void openImageZoom(String imageBase64)
    {
        try
        {
            FullImageActivity.imageURL=imageBase64;
            activity.startActivity(new Intent(activity.getApplicationContext(),FullImageActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        catch (Exception xx)
        {
            xx.getMessage();
        }

    }

    public  MutableLiveData<Boolean> flag_unFlagMsg(String msgID,long bankID,String isFlagged)
    {
        MutableLiveData<Boolean> result=new MediatorLiveData<>();
        try
        {
            String query="update ROOM set hasFlag='"+isFlagged+"',syncFlag='1' where id ='"+msgID+"'";
            dbhelper.Update(query);
            updateMsgStatus(bankID,msgID,isFlagged,"false","");
            result.postValue(true);

        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
            result.postValue(false);
        }
        return result;

    }

    public boolean exportMessages(MutableLiveData<List<ChatRoomModel>> arrayList)
    {
        boolean result=false;
        try
        {
            String messages="";
            for(int i=0;i<arrayList.getValue().size();i++)
            {
                if(arrayList.getValue().get(i).getChecked().equalsIgnoreCase("true"))
                {

                    String messageBody=""+arrayList.getValue().get(i).getMsgBody().trim()+"\r\n";
                    messages=messages+messageBody;

                }

            }

            TextFileCreator.writeToFileAndShare(messages,activity.getApplicationContext());

            return true;

        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
            return false;
        }
    }

    public  void updateMsgsAsRead(long bankID)
    {
        String query="update ROOM set readFlag='true' where bank_id ='"+bankID+"'";
        dbhelper.Update(query);
    }

    public  void updateMsgsAsReadToServer(long bankID)
    {
        ProjectRepository.getInstance(this).updateMsgAsRed(bankID);
    }

    public  void openShareDialog( String msgID,long bankID,String date,String body,String name ) {

        Dialog dialog;

        dialog = new Dialog(activity, android.R.style.Theme_Holo_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_type_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button shareTextBTN= dialog.findViewById(R.id.shareTextBTN);
        Button sharePDFBTN= dialog.findViewById(R.id.sharePDFBTN);
        //Button shareAsImageBTN= dialog.findViewById(R.id.shareAsImageBTN);



        shareTextBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appConstants.openShareIntent(activity,date+"\n\n"+body);
                dialog.dismiss();

            }
        });

        sharePDFBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                try {
                    PDFCreator.fillPdfForm(activity, name, body, date, bankID, msgID);
                    dialog.dismiss();
                }
                catch (Exception xx)
                {
                    Toast.makeText(AppController.getInstance().getApplicationContext(), "No Form For The Selected Bank", Toast.LENGTH_LONG).show();
                }
            }
        });

//        shareAsImageBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                TextToImageCreator.convertToImage(mThis, arrayList.get(pos).getMsgBody());
//                dialog.dismiss();
//            }
//        });


        dialog.show();

    }


    @Override
    public void successRequest(Object classObject, String apiName)
    {
        try
        {
            if (apiName.equalsIgnoreCase(URLClass.DeleteMsg))
            {
                String query="update Room set actionName='',syncFlag='0' where deleteFlag='true' and actionName='delete' and bank_id='"+bankID+"'";
                dbhelper.Update(query);
            }
            else  if(apiName.equalsIgnoreCase(URLClass.SetMsgStatus))
            {
                String query="update Room set actionName='',syncFlag='0' where isReported='true' and actionName='report' and bank_id='"+bankID+"'";
                dbhelper.Update(query);
            }
        }
        catch (Exception xx)
        {

        }
    }

    @Override
    public void failedRequest(String msg, String apiName)
    {


    }
}
