package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.widget.Toast;

import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.loadingWheel;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.models.RegistrationModel;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.views.activities.MainActivity;
import com.a2a.fans.views.activities.registerActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OtpVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {
    Activity activity;
   public String mobileNumber="";
   public String countryCode="";
   public int otpRequestCounter=0;

    public OtpVM(Application application)
    {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();

    }

    public  boolean requestValidation(String otpCode,String utr)
    {
        if(otpRequestCounter<=3)
        {
            loadingWheel.startSpinwheel(activity, false, true);
            ProjectRepository.getInstance(this).authntecation(otpCode, utr);
            otpRequestCounter += 1;
        }
        else
        {
            return false;


        }
        return true;
    }

    public String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find())
        {
            code = m.group(0);
        }
        return code;
    }

//    public void wrongNumberClick()
//    {
//
//        String mobileNum=sharedPrefs.getInstance().getStringPreference(activity.getApplicationContext(),appConstants.mobileNum_KEY);
//        activity.startActivity(new Intent(activity, registerActivity.class).putExtra("mobile",mobileNum ));
//        activity.finish();
//
//    }

    public void resendOTPClick(String otpCode,String utr)
    {
        loadingWheel.startSpinwheel(activity,false,true);
        ProjectRepository.getInstance(this).resendSMS(otpCode,utr);

    }

    @Override
    public void successRequest(Object classObject, String apiName) {

        try
        {
        if(apiName.equalsIgnoreCase(URLClass.Authentication))
        {
             sharedPrefs.getInstance().setBooleanPreference(activity.getApplicationContext(), appConstants.isLoggedIn_KEY, true);
             sharedPrefs.getInstance().setStringPreference(activity.getApplicationContext(),appConstants.mobileNum_KEY,mobileNumber);
             sharedPrefs.getInstance().setStringPreference(activity.getApplicationContext(),appConstants.countryCode_KEY,countryCode);

             activity.startActivity(new Intent(activity, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
             activity.finish();
        }
       else if(apiName.equalsIgnoreCase(URLClass.Resend))
        {
             RegistrationModel registrationModel = (RegistrationModel) classObject;
             appConstants.mobileNumber=registrationModel.getMobileNo();
             activity.finish();

             activity.startActivity( activity.getIntent()
                     .putExtra("timeout", registrationModel.getOTPExpiryTime())
                     .putExtra("MobileNo", registrationModel.getMobileNo())
                     .putExtra("utr", registrationModel.getOTP().getUTR())
                     .putExtra("otp", registrationModel.getOTP().getOTP()));



        }
        }
        catch (Exception xx)
        {
            xx.toString();
        }

    }

    @Override
    public void failedRequest(String msg, String apiName)
    {
        sharedPrefs.getInstance().setStringPreference(AppController.getInstance().getApplicationContext(),appConstants.mobileNum_KEY,"");
        sharedPrefs.getInstance().setStringPreference(AppController.getInstance().getApplicationContext(),appConstants.countryCode_KEY,"");
    }




}
