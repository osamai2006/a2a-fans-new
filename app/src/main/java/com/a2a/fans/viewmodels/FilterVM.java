package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.a2a.fans.R;
import com.a2a.fans.helpers.AppController;
import com.a2a.fans.models.AccountsModel;
import com.a2a.fans.models.ChatRoomModel;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.SQLHelper;

import java.util.ArrayList;
import java.util.List;

public class FilterVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {
    Activity activity;
    String bankID="";
    SQLHelper dbhelper;

  public   MutableLiveData<List<String>> accList=new MediatorLiveData<>();
   public MutableLiveData<List<String>> cardsList=new MediatorLiveData<>();

    public FilterVM(@NonNull Application application)
    {
        super(application);
        activity= AppController.getInstance().getCurrentActivity();
        dbhelper=new SQLHelper(AppController.getInstance().getApplicationContext());
        dbhelper.open();
    }

    public void  getAccountList(long bankID)
    {
        ProjectRepository.getInstance(this).getAccountsNum(bankID);
    }

//    public void  getCardList(String bankID)
//    {
//        ProjectRepository.getInstance(this).getCardsNum(bankID);
//    }

    public LiveData<List<ChatRoomModel>> searchMessageList(final long bankID, String keywordTXT, String amountFromTXT, String amountToTXT, String startDate, String endDate, String accNum, String cardNum, String flaggedMesg)
    {
        return ProjectRepository.getInstance(this).searchChatRoomData(bankID,keywordTXT,amountFromTXT,amountToTXT,startDate,endDate,accNum,flaggedMesg);
    }

    @Override
    public void successRequest(Object classObject, String apiName) {

        try
        {
            if(apiName.equalsIgnoreCase(URLClass.GetAccount))
            {

                AccountsModel accountsBody=(AccountsModel) classObject;
                List<String> list = new ArrayList<>();
                list.add(0,AppController.getInstance().getApplicationContext().getString(R.string.all_accounts));

                for (int i=1;i<accountsBody.getAccounts().size();i++)
                {
                    String accNum=accountsBody.getAccounts().get(i);
                    list.add(accNum);
                }
                accList.postValue(list);

            }

//            if(apiName.equalsIgnoreCase(URLClass.GetCard))
//            {
//                CardsBody cardsBody=(CardsBody) classObject;
//                List<String> list = new ArrayList<>();
//
//                for (int i=0;i<cardsBody.getCards().size();i++)
//                {
//                    String cardsNum=cardsBody.getCards().get(i);
//                    list.add(cardsNum);
//                }
//                cardsList.postValue(list);
//
//            }
        }
        catch (Exception xx)
        {

        }

    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }
}
