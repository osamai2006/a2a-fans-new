package com.a2a.fans.viewmodels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;

import com.a2a.fans.helpers.AppController;
import com.a2a.fans.helpers.sharedPrefs;
import com.a2a.fans.models.BankListModel;
import com.a2a.fans.models.BanksModel;
import com.a2a.fans.models.IsOnlineModel;
import com.a2a.fans.models.MessageModel;
import com.a2a.fans.networking.URLClass;
import com.a2a.fans.Repository.ProjectRepository;
import com.a2a.fans.utls.SQLHelper;
import com.a2a.fans.utls.appConstants;
import com.a2a.fans.views.activities.ChattingActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BanksListVM extends AndroidViewModel implements ProjectRepository.ResponseHandler {

     Activity activity;
     SQLHelper dbhelper;
     boolean isLoggedIn = false;
     private   Cursor dataCursor;
     //BanksListModel banksListModel;
     BankListModel bankListModelModel;
     public MutableLiveData<Boolean> syncResult=new MediatorLiveData<>();

  //public MutableLiveData<List<BanksListModel>> bankList=new MediatorLiveData<>();
    public MutableLiveData<List<BankListModel>> bankList=new MediatorLiveData<>();

    public BanksListVM(Application application) {
        super(application);
        this.activity = AppController.getInstance().getCurrentActivity();
        isLoggedIn = sharedPrefs.getInstance().getBooleanPreference(application.getApplicationContext(), appConstants.isLoggedIn_KEY, false);
        dbhelper=new SQLHelper(activity.getApplicationContext());
        dbhelper.open();
    }


    public  MutableLiveData<Boolean>  syncMessagesActions()
    {
        syncResult=new MediatorLiveData<>();

        try {

            JSONArray deletedMsgArr=new JSONArray();
            JSONArray reportedMsgArr=new JSONArray();
            JSONArray flaggedMsgArr=new JSONArray();
            JSONObject msgBody = new JSONObject();

            String query="select id,actionName,hasFlag,isReported,reportReason from ROOM where syncFlag='1' ";

            dataCursor = dbhelper.Select(query, null);

            if (dataCursor.moveToFirst())
            {

                do
                {
                    String msgID = dataCursor.getString(0);
                    String action = dataCursor.getString(1);
                    String hasFlag = dataCursor.getString(2);
                    String reportFlag = dataCursor.getString(3);
                    String reportReason = dataCursor.getString(4);

                    if(action.equalsIgnoreCase("delete"))
                    {
                        deletedMsgArr.put(msgID);
                    }
                    else if(action.equalsIgnoreCase("report"))
                    {
                        msgBody.putOpt("MsgID", msgID );
                        msgBody.putOpt("Flag", hasFlag );
                        msgBody.putOpt("ReportFlag", reportFlag );
                        msgBody.putOpt("ReportReasonID", reportReason );
                        reportedMsgArr.put(msgBody);
                    }
                    else if(action.equalsIgnoreCase("flag"))
                    {

                    }

                }
                while (dataCursor.moveToNext());

                if(deletedMsgArr.length()>0)
                {
                   ProjectRepository.getInstance(this).deleteMessage(deletedMsgArr);
                }

                if(reportedMsgArr.length()>0)
                {
                    ProjectRepository.getInstance(this).updateMsgStatus(reportedMsgArr);
                }

            }

        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
            syncResult.postValue(false);
        }

        return syncResult;

    }

    public MutableLiveData<List<BankListModel>> fillBanksList()
    {
        List<BankListModel> list=new ArrayList<>();
        try
        {
            dataCursor=dbhelper.Select("select id,name,thumb_image,status,thumb_image,isPinned,promo_code,modified_date" +
                    " from BANKS order by cast(isPinned as integer) desc",null);
            if(dataCursor.moveToFirst())
            {
                String msgCounte="";


                do {
                    long id=Long.parseLong(dataCursor.getString(0));
                    String name=dataCursor.getString(1);
                    String image=dataCursor.getString(2);
                    String status=dataCursor.getString(3);
                    String thumb_image=dataCursor.getString(4);
                    String isPinned=dataCursor.getString(5);
                    String promoCode=dataCursor.getString(6);
                    String modifidDate=dataCursor.getString(7);

                    String query="select count(id) from room where bank_id='"+id+"' and readFlag='false' and deleteFlag=''";
                    Cursor counterCursor=dbhelper.Select(query,null);
                    if(counterCursor.moveToFirst())
                    {
                        msgCounte=counterCursor.getString(0);
                    }

                    bankListModelModel =new BankListModel("","",name,true,id,image,modifidDate,"",promoCode,"",thumb_image,"","",isPinned,msgCounte);
                   // banksListModel = new BanksListModel(id,name,image,status,thumb_image,isPinned,msgCounte,promoCode,modifidDate);
                    list.add(bankListModelModel);

                }
                while (dataCursor.moveToNext());
                //arrayList.postValue(list);
                bankList.postValue(list);
            }
            return bankList;
        }
        catch (Exception xx)
        {
            xx.getMessage();
            return null;
        }
    }

//    public MutableLiveData<List<BanksListModel>> fillBanksList()
//    {
//        List<BanksListModel> list=new ArrayList<>();
//        try
//        {
//            dataCursor=dbhelper.Select("select id,name,thumb_image,status,thumb_image,isPinned,promo_code,modified_date" +
//                    " from BANKS order by cast(isPinned as integer) desc",null);
//            if(dataCursor.moveToFirst())
//            {
//                String msgCounte="";
//
//
//                do {
//                    String id=dataCursor.getString(0);
//                    String name=dataCursor.getString(1);
//                    String image=dataCursor.getString(2);
//                    String status=dataCursor.getString(3);
//                    String thumb_image=dataCursor.getString(4);
//                    String isPinned=dataCursor.getString(5);
//                    String promoCode=dataCursor.getString(6);
//                    String modifidDate=dataCursor.getString(7);
//
//                    String query="select count(id) from room where bank_id='"+id+"' and readFlag='false' and deleteFlag=''";
//                    Cursor counterCursor=dbhelper.Select(query,null);
//                    if(counterCursor.moveToFirst())
//                    {
//                        msgCounte=counterCursor.getString(0);
//                    }
//
//                    banksListModel = new BanksListModel(id,name,image,status,thumb_image,isPinned,msgCounte,promoCode,modifidDate);
//                    list.add(banksListModel);
//
//                }
//                while (dataCursor.moveToNext());
//                //arrayList.postValue(list);
//                bankList.postValue(list);
//            }
//            return bankList;
//        }
//        catch (Exception xx)
//        {
//            xx.getMessage();
//            return null;
//        }
//    }

    public void getBankInfo(long bankID)
    {
        ProjectRepository.getInstance(this).getBankInfo(bankID);
    }

    public   void pinned_unPinned_bank(String bankID ,String isPinned)
    {
        try
        {
            String query="update banks set isPinned='"+isPinned+"' where id ='"+bankID+"'";
            dbhelper.Update(query);
        }
        catch (Exception xx)
        {
            String xxx=xx.getMessage();
        }

    }

    public  void goToBankNotifications(long bankID,String bankName)
    {
        activity.startActivity(new Intent(activity.getApplicationContext(), ChattingActivity.class)
                .putExtra("bankID",bankID)
                .putExtra("bankName",bankName));
    }


    public void deleteAllData()
    {
        dbhelper.Delete("delete from banks");
        dbhelper.Delete("delete from ROOM");
    }


    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getApplication().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void submitIsOnline(MutableLiveData<List<BankListModel>> arrayList)
    {
        if(isLoggedIn && isNetworkConnected())
        {
            try {

                //arrayList= ProjectRepository.getInstance(this).fillBanksList();
                if (arrayList.getValue().size() > 0)
                {
                    JSONArray bankArr = new JSONArray();


                    for (int i = 0; i < arrayList.getValue().size(); i++) {

                        long bankID = arrayList.getValue().get(i).getID();
                        String modifiedDate = arrayList.getValue().get(i).getModifiedDate();

                        JSONObject bankObj = new JSONObject();
                        bankObj.putOpt("ID", bankID);
                        //modifiedDate="2018-01-12T11:12:29.57";
                        bankObj.putOpt("ModifiedDate", modifiedDate);
                        bankArr.put(bankObj);

                    }
                    // loadingWheel.startSpinwheel(activity, false, true);
                    ProjectRepository.getInstance(this).submitIsOnline(bankArr);
                }
            }
            catch (Exception xx){}
        }
    }




    @Override
    public void successRequest(Object classObject, String apiName)
    {
        try
        {
            if(apiName.equalsIgnoreCase(URLClass.IsOnline))
            {
                appConstants.submitOnlineIsCalled=true;
                syncMessagesActions();
                IsOnlineModel isOnlineModel=(IsOnlineModel) classObject;

                for(int i=0;i<isOnlineModel.getIsOnlineListModel().size();i++)
                {
                    if(isOnlineModel.getIsOnlineListModel().get(i).getBankFlag()==true)
                    {
                        long bankID=isOnlineModel.getIsOnlineListModel().get(i).getBankID();
                        ProjectRepository.getInstance(this).getBankInfo(bankID);
                    }
                    if(isOnlineModel.getIsOnlineListModel().get(i).getTrxFlag()==true)
                    {
                        long bankID=isOnlineModel.getIsOnlineListModel().get(i).getBankID();
                        ProjectRepository.getInstance(this).getTrx(bankID);
                    }

                }

            }
            if(apiName.equalsIgnoreCase(URLClass.GetTrx))
            {
                MessageModel messageModel=(MessageModel) classObject;
                for (int x=0;x<messageModel.getAlerts().size();x++)
                {
                    String msgID=String.valueOf(messageModel.getAlerts().get(x).getID());
                    String actionName="",deleteFlag="",syncdFlag="0";

                    String result=  dbhelper.Insert("insert into ROOM values('"+msgID+"'," +
                            "'"+messageModel.getAlerts().get(x).getBankID()+"','"+messageModel.getAlerts().get(x).getMsgText()+"'," +
                            "'"+messageModel.getAlerts().get(x).getMsgText()+"','"+messageModel.getAlerts().get(x).getMsgDateTime()+"'," +
                            "'"+messageModel.getAlerts().get(x).getReadFlag()+"','bank','"+messageModel.getAlerts().get(x).getMagCat()+"'," +
                            "'"+messageModel.getAlerts().get(x).getMsgType()+"','','no_location','"+messageModel.getAlerts().get(x).getBalance()+"'," +
                            "'"+messageModel.getAlerts().get(x).getBalance()+"','"+messageModel.getAlerts().get(x).getAmount()+"'," +
                            "'"+messageModel.getAlerts().get(x).getFlag()+"','"+messageModel.getAlerts().get(x).getAccNo()+"'," +
                            "'"+messageModel.getAlerts().get(x).getReportFlag()+"','"+messageModel.getAlerts().get(x).getReportReason()+"'," +
                            "'"+actionName+"','"+deleteFlag+"','"+messageModel.getAlerts().get(x).getCardNo()+"','"+syncdFlag+"')");

                }

            }
            if(apiName.equalsIgnoreCase(URLClass.GetBankInfo))
            {
                BanksModel banksModel = (BanksModel) classObject;

                long bankID=banksModel.getBank().getID();
                String image=banksModel.getBank().getIcon();
                String thumImg=banksModel.getBank().getThumbnail();

                Cursor banksCursor=dbhelper.Select("select id from BANKS where id='"+bankID+"'",null);
                if(banksCursor.moveToFirst())// already exist
                {
                    String query="update BANKS set name='"+banksModel.getBank().getEDesc()+"'," +
                            "image='"+image+"',thumb_image='"+thumImg+"',status='"+banksModel.getBank().getEnabled()+"'," +
                            " modified_date='"+banksModel.getBank().getModifiedDate()+"',promo_code='"+banksModel.getBank().getPromotionCode()+"'" +
                            " where id='"+banksModel.getBank().getID()+"'";

                    dbhelper.Update(query);

                }
                else
                {
                    String query="insert into BANKS values('"+bankID+"','"+banksModel.getBank().getEDesc()+"'," +
                            "'"+image+"','"+banksModel.getBank().getEnabled()+"','"+thumImg+"','false'," +
                            "'"+banksModel.getBank().getPromotionCode()+"','"+banksModel.getBank().getModifiedDate()+"')";

                    dbhelper.Insert(query);
                }
              fillBanksList();

            }
            if(apiName.equalsIgnoreCase(URLClass.DeleteMsg) || apiName.equalsIgnoreCase(URLClass.SetMsgStatus) )
            {
                syncResult.postValue(true);
            }
        }
        catch (Exception xx)
        {
            xx.toString();
        }

    }

    @Override
    public void failedRequest(String msg, String apiName) {

    }


    @Override
    protected void onCleared() {
        super.onCleared();

    }
}
